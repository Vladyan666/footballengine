var mapAPIData, schemeData, schemeRenderer, ticketParsers, cartTickets = [],
    zoomSizeState = 1, maxZoomStep = 10, zoomSize = 10,
    debugDeveloper = false; siteTypeDoorway = true;
var isMob = window.matchMedia("only screen and (max-width: 760px)").matches;
    mapAPIData = window.map_api_data;

if (!siteTypeDoorway && isMob) $('.map__scheme').css('height', window.innerHeight - 270);

var startCanvas = function(){
    // $.ajax({
    // 	type: 'GET',
    //     //url: '/api/seances/' + event_id +'/canvas/'
    //     //url: '/static/js/libs/canvas/crocus.json'
    //     url: '/api/seances/291564/canvas/'
    // }).then(function (jsonData) {


        // if (jsonData.error) {
        //     console.log(jsonData.error);
        //     // Закрываем прелоадер
        //     $('.map__switch a[data-id="1"]').remove();
        //     $('.map__switch a[data-id="2"]').click();
        //     $('.cute_loader').remove();
        //     $('body').removeClass('noscroll');
        //     return;
        // }

        var getMaximum = function(obj, c) {
            return Math.max.apply(Math, schemeData.tickets.map(function(o) { return c !== 'y' ? o.x : o.y; })) - Math.min.apply(Math, schemeData.tickets.map(function(o) { return c !== 'y' ? o.x : o.y; }))
        }

        // Определяем размер области с местами
        // Нужно определить максимальный размер по x и y
        // map_scale передается через админку 
        var sx = getMaximum(schemeData.tickets, 'x'),
            sy = getMaximum(schemeData.tickets, 'y'),
            canvas_wrapper = $('.canvas-holder').width(),
            map_scale = 1;
            map_scale = !isNaN(map_scale) && map_scale > 0 && map_scale <= 100 
                        ? parseInt(map_scale, 10)/100 : 1;

        if (sy > sx) {
            var scale = ((window.innerWidth/100)*95/sy)*map_scale;
          } else {
            var scale = ((window.innerWidth/100)*95/sx)*map_scale;
          }
        if (sy > canvas_wrapper) sy = canvas_wrapper;
        
        var height = sy * scale;

        console.log(sx + ' ' + sy + ' scale: ' + scale + ' map_scale: ' + map_scale);

        if (!isMob) {
            if (!siteTypeDoorway) $('.map__scheme').css('height', height + 50);
        } else {
            // height = $('.popScheme').height() - 5;
            $('.map__scheme').css('height', height);
        }

        // Добавляем легенду
        // addLegend();

    	// чтобы можно было отменять показ тултипа
    	var hideTooltipTimeout = null;

        // Сохраняем парсеры
        // if (userAuth && isStaff) {
        //     ticketParsers = [];
        //     schemeData.parsers.forEach(function(el, index) {
        //         ticketParsers[el.i] = el.n;
        //     });
        // }

    	schemeRenderer = new SchemeRenderer({
        canvasElement: document.getElementById('map'),
        tooltipElement: document.getElementById('tooltip'),

        // функция показа тултипа
        showTooltipFunction: function (tooltipElement, schemeObject, event) {
            if (hideTooltipTimeout) {
                clearTimeout(hideTooltipTimeout);
            }

            var params = schemeObject.getParams(),
                currency = params.currency === 'eur' ? '€' : 'руб.',
                sector = params.sectorName ? params.sectorName : 'Сектор';

            if (params.multipleTickets) {
                tooltipElement.innerHTML = 'Это сектор свобондной рассадки ' + sector;
            } else {
                tooltipElement.innerHTML = '<div>' + params.price + ' ' + currency + '</div>' +
                    '<div><div>Ряд: <span>' + params.row + ' </span></div><div>Место: <span>' + params.seat + '</span></div></div>' +
                    '<div>' + sector + '</div>';
                // if (userAuth && isStaff) {
                //     tooltipElement.innerHTML += '<span class="parser_info" style="color: ' + params.color + '">' + ticketParsers[params.parser] + '</span>';
                //     tooltipElement.innerHTML += '<span class="parser_info" style="color: ' + params.color + '">Наценка: ' + (params.price - params.nominalPrice) + '</span>';
                //     if (params.parser == 7) tooltipElement.innerHTML += '<pre>' + params.info + '</pre>';
                // }
            }

            tooltipElement.classList.add('active');

            var tooltipWidth = tooltipElement.offsetWidth,
                tooltipHeight = tooltipElement.offsetHeight;
                
            tooltipElement.style.top = (event.clientY - tooltipHeight - 25) + 'px';
            tooltipElement.style.left = (event.clientX - (tooltipWidth / 2)) +  'px';
        },

        // функция скрытия тултипа
        hideTooltipFunction: function (tooltipElement) {
            hideTooltipTimeout = setTimeout(function() {
                tooltipElement.classList.remove('active');
            }, 100);
        },

        // функция, возвращающая контент тултипа
        tooltipContentFunction: function(schemeObject) {
            var params = schemeObject.getParams(),
            	currency = params.currency == 'eur' ? '€' : 'руб.',
            	sector = params.sectorName ? params.sectorName : 'Сектор';
            
            return '<div>' + params.price + ' ' + currency + '</div>' +
    		       '<div><div>Ряд: <span>' + params.row + '</span></div><div>Место: <span>' + params.seat + '</span></div></div>' +
    		       '<div>' + sector + '</div>';
        },
        // функция отрабатывает, когда место выбрано
        onPlaceSelect: function(schemeObject) {
           	var params = schemeObject.getParams(),
                item = {c:params.currency,
                    is_bintranet: false,
                    is_official: false,
                    np: params.nominalPrice,
                    p: params.price,
                    id: params.ticketId,
                    row: params.row,
                    seat: params.seat,
                    sn: params.sectorName
                };

            console.log(params);
            $.ajax({
                url: '/ajax/',
                method: "POST",
                data: { type : 'put_carryall_ticket', json : item, match_id : data_id, csrfmiddlewaretoken: getCookie('csrftoken')},
                success: function (data, textStatus){
                    addToCart(data, id, item);
                }
            })
        },

        // функция отрабатывает, когда место снято
        onPlaceUnselect: function(schemeObject) {
            var params = schemeObject.getParams();
                item = {c:params.currency,
                    is_bintranet: false,
                    is_official: false,
                    np: params.nominalPrice,
                    p: params.price,
                    id: params.ticketId,
                    row: params.row,
                    seat: params.seat,
                    sn: params.sectorName
                };
            $.ajax({
                url: '/ajax/',
                method: "POST",
                data: { type : 'remove_carryall_ticket', json : item, match_id : data_id, csrfmiddlewaretoken: getCookie('csrftoken')},
                success: function (data, textStatus){
                    deleteFromCart(id, item["p"]);
                }
            })
        },

        onFreeSeatingSectorClick: function (schemeObject) {
            console.log('free seating sector', schemeObject);
            $('.ticket_modal__wrapper').addClass('visible');
        },

        placeRadiusDeltaOnSelect: 2,

        // конфигурация объекта SchemeDesigner
        schemeOptions: {
            options: {
                background: '#F3F3F3',
                cacheSchemeRatio: 2
            },
            scroll: {
                maxHiddenPart: 0.9
            },
            zoom: {
                padding: 0.15,
                maxScale: 8,
                zoomCoefficient: 1.04,
                clickZoomDelta: 30
            },
            storage: {
                treeDepth: 8
            },
            map: {
                mapCanvas: document.getElementById('canvas_guide')
            }
        }
       	});

    	// устанавливаем данные схемы
        schemeRenderer.setSchemeData(schemeData);

        // отрисовываем
        schemeRenderer.render();

        // Добавляем билеты из корзины
        // var tickets = parseTickets(storage.getItem('cart_tickets'));
        // tickets.forEach(function (e) {
        //     schemeRenderer.selectPlaceById(e.id);
        // });

        if (schemeData.multiple_tickets && schemeData.multiple_tickets.length) {
            multipleTickets(schemeData.multiple_tickets);
        }

        // if (isMob) schemeRenderer.zoomToCenter(15);
        if (devicePixelRatio) console.log('devicePixelRatio: ', devicePixelRatio);

        // Закрываем прелоадер
        $('.cute_loader').remove();
        $('body').removeClass('noscroll');
          
        // Модальник, когда нет билетов
        if (!schemeData.tickets.length && (!schemeData.multiple_tickets && !schemeData.multiple_tickets.length)) {
            $('#feedback').fadeIn();
        } else {
            // Вызываем блоки "Кто еще выбирает сейчас билеты" и "Подарок" 
            // showBlocks();
        }

      	// Отслеживаем изменение размера окна
      	window.addEventListener("resize", function(e) {
            schemeRenderer.toCenter();
        })
    // });
}

function multipleTickets(ticketsArray) {
    console.log(ticketsArray);
    return;
    $(ticketsArray).each(function(key, ticket) {
        var $sector = schemeData.sectors[ticket.sector];
        var h = $('#map').height() / devicePixelRatio;
        var t = $('#map').offset().top / devicePixelRatio;

        if (!ticket.hasOwnProperty('tickets')) return;

        ticketObj = ticket.tickets[0];

        // Если место уже выбрано
        var active = 0;
        var seatInCart = null;

        if (cartTickets[ticketObj.id]) {
            seatInCart = cartTickets[ticketObj.id];
            active = 1;
        }

        var minus_disabled = _amount == 0 ? ' disabled' : '';
        var plus_disabled = _amount == ticketObj.quantity ? ' disabled' : '';

    var _amount = active ? parseInt(seatInCart.count, 10) : 0;

    // Схема окошко выбора +/-
    var rowTemplate = '<div data-count="' + ticketObj.quantity + '" data-id="' + ticketObj.id + '" class="row__wrp rowMultiple">' +
        '<div class="row__label row__label--noseats">' +
        '<div class="inline-top row__count">' + ticketObj.quantity + '</div>' +
        '<div class="inline-top">' + ticketObj.price + ' руб</div>' +
        '<div class="inline-top cart-control">' +
        '<button class="cart-control__btn cart-control__btn--minus multipleRemove">-</button>' +
        '<span class="cart-control__qty">' + _amount + '</span>' +
        '<button class="cart-control__btn cart-control__btn--plus multipleAdd">+</button>' +
        '</div>' +
        '</div>' +
        '</div>';

    if (!$('.ticket_modal[data-id="' + $sector.slug +'"]').length) {
        $('body').append( // ticket_modal map__row
          '<div class="ticket_modal__wrapper" style="">' +
            '<div class="ticket_modal active" data-id="' + $sector.id + '" data-slug="' + $sector.slug + '">' +
              '<div class="ticket_modal__heading">' + $sector.name + '</div>' +
              '<div class="ticket_modal_grey">Выберите количество билетов</div>' +
              '<div class="ticket_modal__close"></div>' +
              '<div class="map__ditails_names">' +
                '<div>В наличии</div>' +
                '<div>Цена:</div>' +
                '<div>Вам нужно:</div>' +
              '</div>' +
              '<div class="map__ditails">' + rowTemplate + '</div>' +
            '</div>' +
          '</div>'
        );
      }
      $('.ticket_modal__wrapper').css('height', h).css('top', t);

      $modal = $('.ticket_modal__wrapper[data-id="' + $sector.slug + '"]');
      if (!$modal.length) {
            $('#wrapper').append('\
            <div class="ticket_modal__wrapper" data-id="'+ $sector.slug + '">\
                <div class="ticket_modal">\
                    <div class="ticket_modal__heading">'+ ticket.sn + '</div>\
                    <div class="ticket_modal__close"></div>\
                    <div class="ticket_modal__content"></div>\
                </div>\
            </div>\
            ')
        }

      $('.ticket_modal__wrapper').find('.ticket_modal__close').off('click').on('click', function (e) {
            $('.ticket_modal__wrapper').removeClass('visible');
        });

    });
}

// Мультибилеты на списке
$('body').on('click', '.multipleAdd', function() {
    var $row = $(this).closest('.rowMultiple'),
        ticketID = $row.data('id'),
        sectorSlug = $row.data('sector-slug'),
        sector = sectorSlug ? _.find(schemeData.sectors, function(sector) {return sector.slug == sectorSlug}) : '',
        sectorID = sectorSlug ? sector.id : $(this).closest('.ticket_modal').data('id');
    
    upsertMultiTicket( $(this).closest('.rowMultiple'), 1 );
});

$('body').on('click', '.multipleRemove', function() {
    var $row = $(this).closest('.rowMultiple'),
        ticketID = $row.data('id'),
        sectorSlug = $row.data('sector-slug'),
        sector = sectorSlug ? _.find(schemeData.sectors, function(sector) {return sector.slug == sectorSlug}) : '',
        sectorID = sectorSlug ? sector.id : $(this).closest('.ticket_modal').data('id');

    upsertMultiTicket( $(this).closest('.rowMultiple'), -1 );
});

$('body').on('click', '.zoom-control .active', function() {
    console.log(zoomSizeState);
    if ($(this).hasClass('minus')) {
        if (zoomSize > 0) zoomSize *= -1;
        if (zoomSizeState > 1) {
            zoomSizeState -= 1
        } else {
            $(this).removeClass('active');
        }
        $('.zoom-control .plus').addClass('active');
    } else {
        if (zoomSize < 0) zoomSize *= -1;
        if (zoomSizeState < maxZoomStep) {
            zoomSizeState += 1;
        } else {
            $(this).removeClass('active');
        }
        $('.zoom-control .minus').addClass('active');
    }
    console.log(zoomSizeState);
    schemeRenderer.zoomToCenter(zoomSize);
});

// Легенда
function addLegend() {
    var curr = $('.map__title').data('currency');

    if (show_legend && !$('.map_legend').length) {
        $('.map__scheme').prepend('<div class="map_legend"><div class="map_legend__wrp"></div></div>');
    }

    map_api_data.hall.sectors.forEach(function(sector){
        var min = Math.round(parseInt(sector.min_price)),
            max = Math.round(parseInt(sector.max_price)),
            color = '#555',
            order = 0;

        $('.map_legend__wrp').append(
            `<div style="order: ${order};" class="map_legend__element" data-slug="${sector.slug}" data-min="${min}" data-max="${max}">
                <div class="map_legend__title">${sector.name}</div>
                <div class="map_legend__price">${min} ${curr} - ${max} ${curr}</div>
                <div style="background: ${color};" class="map_legend__row">/div>
            </div>`
        );
    });
}

var upsertMultiTicket = function( $row, direction ) {
    var qty = parseInt($row.find('.cart-control__qty').first().text());
    var limit = parseInt($row.find('.row__count').text());

    if (qty==0 && direction == -1) {
        return;
    }
    if (qty==limit && direction == 1) {
        return;
    }

    var seatData = $row.data('seat');

    console.log(seatData, (qty + direction));
};

function tooltipData() {
    var self = event.target;
    var str, $el, mprice, row_title__name,
        currency = $('.map__title').data('currency');

    var row = 1,
        seat = 7,
        ticket_count = 0,
        price_extra = true,
        price_currency = 'rur',
        price = 100,
        n_price = 100,
        price_margin = 0,
        data_pre = '',
        data_profit = '',
        sector = 'sector_slug',
        sector_name = 'Сектор',
        source = '',
        official = false;
    
    if (seat) {
        var second_price = '',
            second_currency = '';

        if (price_extra) {
            if (price_currency == 'rur') {
                second_currency = '<span class="rur">руб</span>';
            }

            second_price = '<span class="sec_cur"> / ' + price_extra + ' ' + second_currency + '</span>';
        }

        if (sector == 'benuar_levaja_storona'   ||
            sector == 'benuar_pravaja_storona'  ||
            sector == 'beletazh_pravaja_storona' ||
            sector == 'beletazh_levaja_storona' ||
            sector == '1_jarus_levaja_storona'  ||
            sector == '1_jarus_pravaja_storona' || 
            sector == '2_jarus_pravaja_storona' ||
            sector == '2_jarus_levavja_storona' || 
            sector == '3_jarus_pravaja_storona' || 
            sector == '3_jarus_levaja_storona') {
            row_title__name = row_title__lozha;
        } else {
            row_title__name = row_title;
        }

        var row_info = row != '-1' ? '<div class="window_map__location"><div>' + row_title__name + ': <span>' + row + '</span></div><div>{% trans "Место" %}: <span>' + seat + '</span></div></div>' : '<div><div>'+ ticket_count + ' ' + declOfNum(ticket_count, ['билет', 'билета', 'билетов'])  +'</div></div>',
            row_price = '';

            // Спецпредложение/спецтариф
            if (data_pre) {
                row_price += '<div class="window-map_spec">' + row_spec + '</div>';
            }
    
            if (data_profit == 'true') {
                row_price += '<div class="window_map__price window_map__price-profit"><div>{% trans "Выгодная цена" %}</div><div>' + price + ' ' + currency + second_price + '</div></div>' +
                            '<div class="window_map__price window_map__price-profit window_map__price-profit_old"><div>{% trans "Старая цена" %}</div><div>' + parseInt(price * 1.3) + ' ' + currency + second_price + '</div></div>';
            } else if (official == 'true') {
                row_price += '<div class="window_map__price window_map__price-profit window_map__price-normal"><div>{% trans "Официальные билеты от организатора. Билеты БЕЗ наценки!" %}</div></div>' +
                            '<div class="window_map__price">' + price + ' ' + currency + second_price + '</div>';
            } else if (event_id == 368248) {
                row_price += '<div class="window_map__price window_map__price-foreign"><div>{% trans "Билеты от партнеров с наценкой" %}</div></div>' + 
                            '<div class="window_map__price">' + price + ' ' + currency + second_price + '</div>';
            } else {
                row_price += '<div class="window_map__price">' + price + ' ' + currency + second_price + '</div>';
            }

            str = '<div class="hide_this">С учетом сервисного сбора и бесплатной доставки</div>' +
                    row_price +
                    row_info +
                    '<div class="window_map__sector">' + sector_name + '</div>';

            if (source) {
                var source_row = source.toString();

                // Если со Зрителей
                if (source_row.search("zriteli.api") >= 0) {
                    source_row = source_row.split('\n')
                    source_row = source_row[0];
                }

                str += '<div class="window-map_source">' + source_row + '</div>';
                str += '<div class="window-map_source window-map_source-m">Наценка: ' + price_margin + '</div>';
            }

            str += '<div class="hide_this">Номинальная цена:' + n_price + ' ' + currency + '</div>';

    }
    else { 
        $el = $(self).parent();
        mprice = ($el.attr("data-min_price") ? $el.attr("data-min_price") : ($el.attr("data-max_price") ? $el.attr("data-max_price") : false ) );
        mprice = mprice ? '<div>от ' + mprice + ' <span class="rur">руб</span></div>' : '';
        $tooltip.html('\<div class="sector__header">' + $el.attr("data-name") + '</div>\
            <div class="sector__tickets">' + $el.attr("data-count") + ' ' + declOfNum($el.attr("data-count"), ['билет', 'билета', 'билетов']) + '</div>' +
            mprice);
    }
    $tooltip.html(str);
}