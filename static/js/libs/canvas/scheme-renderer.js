/**
 * Класс-обертка над SchemeDesigner
 * @param {Object} options
 */
var SchemeRenderer = function (options) {
    /**
     * Контекст
     * @type {SchemeRenderer}
     */
    var self = this;

    /**
     * Элемент канваса
     * {DOMElement}
     */
    var canvasElement = options.canvasElement;

    /**
     * Элемент тултипа
     * {DOMElement}
     */
    var tooltipElement = options.tooltipElement;

    /**
     * Функция показа тултипа
     * @type {Function}
     */
    var showTooltipFunction = options.showTooltipFunction || function() {};

    /**
     * Функция скрытия тултипа
     * @type {Function}
     */
    var hideTooltipFunction = options.hideTooltipFunction || function() {};

    /**
     * При выделения места
     * @type {Function}
     */
    var onPlaceSelect = options.onPlaceSelect;

    /**
     * При снятия выделения места
     * @type {Function}
     */
    var onPlaceUnselect = options.onPlaceUnselect;

    /**
     * При клике на сектор свободной рассадки
     * @type {Function}
     */
    var onFreeSeatingSectorClick = options.onFreeSeatingSectorClick || function() {};

    /**
     * Отношение размера неактивного места к базовому
     * @type {number}
     */
    var inactivePlaceSizeRatio = options.inactivePlaceSizeRatio || 1;

    /**
     * Увеличение радиуса места при выборе
     * @type {number}
     */
    var placeRadiusDeltaOnSelect = options.placeRadiusDeltaOnSelect || 0;
    var halfPlaceRadiusDeltaOnSelect = placeRadiusDeltaOnSelect / 2;

    /**
     * Слой картой мест
     * @type {SchemeDesigner.Layer}
     */
     var backgroundLayer = new SchemeDesigner.Layer('background', {zIndex: 0, visible: false, active: false});

    /**
     * Слой с местами
     * @type {SchemeDesigner.Layer}
     */
    var placesLayer = new SchemeDesigner.Layer('places', {zIndex: 10});

    /**
     * Слой с мультиместами
     * @type {SchemeDesigner.Layer}
     */
    var multiplePlacesLayer = new SchemeDesigner.Layer('multiplePlaces', {zIndex: 10});

    /**
     * Слой с метками
     * @type {SchemeDesigner.Layer}
     */
     var labelsLayer = new SchemeDesigner.Layer('labels', {zIndex: 0});

    /**
     * Объект SchemeDesigner
     * @type {SchemeDesigner.Scheme}
     */
    var schemeDesigner = new SchemeDesigner.Scheme(canvasElement, options.schemeOptions);

    /**
     * Определение Retina
     * @type {number}
     */
    var devicePixelRatio = schemeDesigner.devicePixelRatio;

    /**
     * Данные схемы
     * @type {Object}
     */
    var schemeData = null;

    /**
     * 2 Пи
     * @type {number}
     */
    var TwoPi = Math.PI * 2;

    /**
     * Тач устройство
     * {Boolean}
     */
    var touchSupported = SchemeDesigner.Tools.touchSupported();

    /**
     * Установить данные схемы
     * @param {Object} value
     */
    this.setSchemeData = function (value) {
        schemeData = value;
    };

    /**
     * Зум в центр
     * @param {Number} delta
     */
    this.zoomToCenter = function (delta) {
        schemeDesigner.getZoomManager().zoomToCenter(delta);
    };

    this.toCenter = function () {
        schemeDesigner.getScrollManager().toCenter();
    };

    /**
     * Объект SchemeDesigner.Scheme
     * @return {SchemeDesigner.Scheme}
     */
    this.getSchemeDesigner = function() {
        return schemeDesigner;
    };

    /**
     * Выбрать место по id
     * @param {String} id
     */
    this.selectPlaceById = function(id) {
        var places = placesLayer.getObjects();
        var placesCount = places.length;
        for (var i = 0; i < placesCount; i++) {
            var place = places[i];
            var ticketId = place.params.ticketId;
            if (ticketId === id) {
                place.params.isSelected = true;
                schemeDesigner.updateCache(false);
                schemeDesigner.requestRenderAll();
                break;
            }
        }
    };

    /**
     * Убрать выбор места по id
     * @param {String} id
     */
    this.unselectPlaceById = function(id) {
        var places = placesLayer.getObjects();
        var placesCount = places.length;
        for (var i = 0; i < placesCount; i++) {
            var place = places[i];
            var ticketId = place.params.ticketId;
            if (ticketId === id) {
                place.params.isSelected = false;
                schemeDesigner.updateCache(false);
                schemeDesigner.requestRenderAll();
                break;
            }
        }
    };

    /**
     * Рендер схемы
     */
    this.render = function() {
        var places = getPlaces(); // Места
        var placesCount = places.length;

        for (var i = 0; i < placesCount; i++) {
            places[i].h = places[i].h * devicePixelRatio;
            places[i].w = places[i].w * devicePixelRatio;
            places[i].р = places[i].p * devicePixelRatio;
            places[i].x = places[i].x * devicePixelRatio;
            places[i].y = places[i].y * devicePixelRatio;
        }

        for (var i = 0; i < placesCount; i++) {
            var placeObject = createPlaceObject(places[i]);
            placesLayer.addObject(placeObject);
        }

        var labels = getLabels(); // Лейблы
        var labelsCount = labels.length;

        for (var i = 0; i < labelsCount; i++) {
            labels[i].x = labels[i].x * devicePixelRatio;
            labels[i].y = labels[i].y * devicePixelRatio;
        }

        for (var j = 0; j < labelsCount; j++) {
            var labelObject = createLabelObject(labels[j]);
            labelsLayer.addObject(labelObject);
        }

        var multiplePlaces = getMultiplePlaces(); // Мультибилеты
        var multiplePlacesCount = multiplePlaces.length;

        for (var i = 0; i < multiplePlacesCount; i++) {
            if (!multiplePlaces[i].hasOwnProperty('geometry')) {
                console.log('Нет геометрии билетов без мест');
            } else { 
                multiplePlaces[i].geometry.x = multiplePlaces[i].geometry.x * devicePixelRatio;
                multiplePlaces[i].geometry.y = multiplePlaces[i].geometry.y * devicePixelRatio;
                multiplePlaces[i].geometry.w = multiplePlaces[i].geometry.w * devicePixelRatio;
                multiplePlaces[i].geometry.h = multiplePlaces[i].geometry.h * devicePixelRatio;
            }
        }

        for (var k = 0; k < multiplePlacesCount; k++) {
            var multiplePlace = createMultiplePlaceObject(multiplePlaces[k]);
            multiplePlacesLayer.addObject(multiplePlace);
        }

        /**
         * add background object
         */
        // var bg = createBackground();
        // backgroundLayer.addObject(bg);

        /**
         * Добавляем слои
         */
        schemeDesigner.addLayer(labelsLayer);
        schemeDesigner.addLayer(placesLayer);
        schemeDesigner.addLayer(multiplePlacesLayer);
        // schemeDesigner.addLayer(backgroundLayer);

        /**
         * Отрисовываем схему
         */
        schemeDesigner.render();
    };

    /**
     * Получить места схемы
     * @return {Array}
     */
    var getPlaces = function() {
        return typeof schemeData.tickets !== 'undefined' ? schemeData.tickets : [];
    };

    /**
     * Получить места схемы
     * @return {Array}
     */
    var getMultiplePlaces = function() {
        return typeof schemeData.multiple_tickets !== 'undefined' ? schemeData.multiple_tickets : [];
    };

    /**
     * Получить метки схемы
     * @return {Array}
     */
    var getLabels = function() {
        return typeof schemeData.texts !== 'undefined' ? schemeData.texts : [];
    };

    /**
     * Получить сектора
     * @return {Array}
     */
    var getSectors = function() {
        return typeof schemeData.sectors !== 'undefined' ? schemeData.sectors : [];
    };

    /**
     * Получить валюту
     * @return {Array}
     */
    var getСurrency = function() {
        return typeof schemeData.currency !== 'undefined' ? schemeData.currency : [];
    };

    /**
     * Получить список парсеров
     * @return {Array}
     */
    var getParsers = function() {
        return typeof schemeData.parsers !== 'undefined' ? schemeData.parsers : [];
    };

    /**
     * Получить группы
     * @return {Array}
     */
    var getGroups = function() {
        return typeof schemeData.groups !== 'undefined' ? schemeData.groups : [];
    };

    /**
     * Отрисовать круглое место
     * @param {SchemeObject} schemeObject
     * @param {Scheme} schemeDesigner
     * @param {View} view
     */
    var renderCirclePlace = function(schemeObject, schemeDesigner, view) {
        var context = view.getContext();

        context.beginPath();

        var placeParams = schemeObject.getParams();
        var isAvail = placeParams.avail;

        var isHovered = isAvail && schemeObject.isHovered && !touchSupported;
        var isSelected = isAvail && placeParams.isSelected;
        var backgroundColor = isAvail ? (placeParams.color || '#8a2be2') : '#ccc';

        context.lineWidth = 1; // Off
        context.strokeStyle = backgroundColor;
        context.fillStyle = backgroundColor;

        var relativeX = Math.round((schemeObject.getX() / devicePixelRatio) * 100) / 100;
        var relativeY = Math.round((schemeObject.getY() / devicePixelRatio) * 100) / 100;
        var width = schemeObject.getWidth() / devicePixelRatio;

        if (isSelected || isHovered) {
            context.fillStyle = 'white';
        } else {
            width = width - placeRadiusDeltaOnSelect;
            relativeX = relativeX + halfPlaceRadiusDeltaOnSelect;
            relativeY = relativeY + halfPlaceRadiusDeltaOnSelect;
        }

        if (!isAvail) {
            var newWidth = width * inactivePlaceSizeRatio;
            var widthDeltaHalf = (width - newWidth) / 2;
            width = newWidth;
            relativeX = relativeX + widthDeltaHalf;
            relativeY = relativeY + widthDeltaHalf;
        }

        var halfWidth = width / 2;
        var radiusPositive = halfWidth < 0 ? 1 : halfWidth;

        // if (isAvail && placeParams.seat == 19 && placeParams.row == 4) console.log('row: ' + placeParams.row + ', arc-x: ' + (relativeX ) + ', w: ' + radiusPositive);

        context.arc(relativeX + halfWidth, relativeY + halfWidth, radiusPositive, 0, TwoPi, false);
        context.fill();
        
        if (isSelected || isHovered) {
            // номер места
            context.font = (halfWidth >> 0) + 'px Arial';
            context.fillStyle = 'black';

            context.textAlign = 'center';
            context.textBaseline = 'middle';

            context.fillText(
                placeParams.seat,
                relativeX + halfWidth,
                relativeY + halfWidth
            );
            context.stroke();
        }
    };

    /**
     * Отрисовать прямоугольное место
     * @param {SchemeObject} schemeObject
     * @param {Scheme} schemeDesigner
     * @param {View} view
     */
    var renderRectPlace = function(schemeObject, schemeDesigner, view) {
        var context = view.getContext();

        context.beginPath();

        var placeParams = schemeObject.getParams();
        var isAvail = placeParams.avail;

        var isHovered = isAvail && schemeObject.isHovered && !touchSupported;
        var isSelected = isAvail && placeParams.isSelected;
        var backgroundColor = isAvail ? (placeParams.color || '#8a2be2') : '#ccc';

        context.lineWidth = 0; // Off
        context.strokeStyle = backgroundColor;
        context.fillStyle = backgroundColor;

        var relativeX = schemeObject.getX() / devicePixelRatio;
        var relativeY = schemeObject.getY() / devicePixelRatio;
        var width = schemeObject.getWidth() / devicePixelRatio;
        var height = schemeObject.getHeight() / devicePixelRatio;

        context.rect(relativeX, relativeY, width, height);

        context.fill();
        // context.stroke();
    };

    /**
     * Отрисовать прямоугольное место
     * @param {SchemeObject} schemeObject
     * @param {Scheme} schemeDesigner
     * @param {View} view
     */
    var renderRectWithBorderPlace = function(schemeObject, schemeDesigner, view) {
        var context = view.getContext();

        context.beginPath();

        var placeParams = schemeObject.getParams();
        var isAvail = placeParams.avail;

        var isHovered = isAvail && schemeObject.isHovered && !touchSupported;
        var isSelected = isAvail && placeParams.isSelected;
        var backgroundColor = isAvail ? (placeParams.color || '#8a2be2') : '#ccc';

        context.lineWidth = placeParams.geometry.line_width / devicePixelRatio;
        context.strokeStyle = backgroundColor;
        context.fillStyle = '#ffffff';

        var relativeX = schemeObject.getX() / devicePixelRatio;
        var relativeY = schemeObject.getY() / devicePixelRatio;
        var width = schemeObject.getWidth() / devicePixelRatio;
        var height = schemeObject.getHeight() / devicePixelRatio;

        context.rect(relativeX, relativeY, width, height);

        context.fill();
        context.stroke();
    };

    /**
     * Функция рендеринга лейбла
     * @param {SchemeObject} schemeObject
     * @param {Scheme} schemeDesigner
     * @param {View} view
     */
    var renderLabel = function(schemeObject, schemeDesigner, view)
    {
        var params = schemeObject.getParams();
        var fontSize = params.fsize;
        var context = view.getContext();
        context.fillStyle = params.color;
        context.font = fontSize + ' Arial';
        context.textAlign = 'start';
        context.textBaseline = 'middle';

        // todo разбивка на строки, если нужно
        var lines = [params.text];
        var textHeight = lines.length * parseInt(fontSize);

        renderTextLines(
            view,
            lines,
            fontSize,
            schemeObject.getX() / devicePixelRatio,
            (schemeObject.getY() / devicePixelRatio) - (textHeight / 2)
        );
    };

    /**
     * Запрос на перерисовку схемы через ощутимое время
     */
    var lazyRefreshTimeout = null;
    var requestLazyRefresh = function() {
        if (lazyRefreshTimeout) {
            clearTimeout(lazyRefreshTimeout);
        }

        lazyRefreshTimeout = setTimeout(function() {
            schemeDesigner.requestRenderAll();
        }, 500);
    };

    /**
     * Метод ховера на место
     * @param {SchemeObject} schemeObject
     * @param {SchemeDesigner} schemeDesigner
     * @param {View} view
     * @param e
     * @return {boolean}
     */
    var placeHoverFunction = function(schemeObject, schemeDesigner, view, e) {
        var averageDimension = (schemeObject.getWidth() + schemeObject.getHeight()) / 2;
        var scale = schemeDesigner.getZoomManager().getScale();
        var realAverageDimension = averageDimension * scale;

        // если реальный размер 6 пискселей и меньше - не отображаем ховер для производителньости
        if (realAverageDimension <= 6) {
            requestLazyRefresh();
            return false;
        }

        return true;
    };

    /**
     * Фукнкция очистки места
     * @param {SchemeDesigner.SchemeObject} schemeObject
     * @param {SchemeDesigner.Scheme} schemeDesigner
     * @param {SchemeDesigner.View} view
     */
    var clearPlaceFunction = function (schemeObject, schemeDesigner, view) {
        var context = view.getContext();
        var borderWidth = 0;
        var width = schemeObject.getWidth();
        context.clearRect(schemeObject.getX() - borderWidth,
            schemeObject.getY() - borderWidth,
            width + (borderWidth * 2),
            width + (borderWidth * 2)
        );
    };

    /**
     * Создать объект места
     * @param {Object} data
     * @return {SchemeDesigner.SchemeObject}
     */
    var createPlaceObject = function (data) {
        var ticketId = data.i || null,
            isAvail = !!ticketId,
            sectors = getSectors(),
            currency = getСurrency(),
            sectorData = typeof data.z !== 'undefined' && (typeof sectors[data.z] !== 'undefined'),
            width = sectorData ? (isAvail ? sectors[data.z].wa : sectors[data.z].w) : 10,
            height = sectorData ? (isAvail ? sectors[data.z].ha : sectors[data.z].h) : 10;
            delta = Math.round((sectors[data.z].ha - sectors[data.z].h) * 50) / 100;

            // if (isAvail && data.s == 19 && data.r == 4) console.log('row: ' + data.r + ', x: ' + data.x + ', hradius: ' + halfPlaceRadiusDeltaOnSelect);
        
        return new SchemeDesigner.SchemeObject({
            x: isAvail ? data.x - halfPlaceRadiusDeltaOnSelect : data.x + delta,
            y: isAvail ? data.y - halfPlaceRadiusDeltaOnSelect : data.y + delta,
            width: (width + placeRadiusDeltaOnSelect) * devicePixelRatio,
            height: (height + placeRadiusDeltaOnSelect) * devicePixelRatio,
            info: typeof data.e !== 'undefined' ? data.e : '',
            parser: typeof data.q !== 'undefined' ? data.q : null,
            sector: data.z,
            sectorName: typeof data.z !== 'undefined' && (typeof sectors[data.z] !== 'undefined') ? sectors[data.z].n : null,
            seat: data.s,
            row: data.r,
            color: typeof data.z !== 'undefined' && (typeof sectors[data.z] !== 'undefined') ? sectors[data.z].c : '#999',
            ticketId: ticketId,
            price: data.p || null,
            nominalPrice: data.n || null,
            currency: typeof data.c !== 'undefined' && (typeof currency[data.c] !== 'undefined') ? currency[data.c].n : null,
            avail: isAvail,
            active: isAvail,
            renderFunction: renderCirclePlace,
            mouseOverFunction: placeHoverFunction,
            mouseLeaveFunction: placeHoverFunction,
            cursorStyle: 'pointer',
            isSelected: false,
            clickFunction: null,
            clearFunction: clearPlaceFunction
        });
    };

    /**
     * Создать объект мультиместа
     * @param {Object} data
     * @return {SchemeDesigner.SchemeObject}
     */
    var createMultiplePlaceObject = function (data) {
        var isAvail = data.tickets && data.tickets.length > 0;
        var sectors = getSectors();
        var geometry = data.geometry || {};

        return new SchemeDesigner.SchemeObject({
            x: geometry.x,
            y: geometry.y,
            width: geometry.w,
            height: geometry.h,
            sector: data.sector,
            sectorName: typeof data.sector !== 'undefined' && (typeof sectors[data.sector] !== 'undefined') ? sectors[data.sector].name : null,
            seat: data.seat,
            row: data.row,
            color: data.color,
            tickets: data.tickets,
            geometry: data.geometry || null,
            avail: isAvail,
            active: isAvail,
            renderFunction: data.geometry_type === 'rectWithBorder' ? renderRectWithBorderPlace : renderRectPlace,
            mouseOverFunction: placeHoverFunction,
            mouseLeaveFunction: placeHoverFunction,
            cursorStyle: 'pointer',
            isSelected: false,
            clickFunction: null
        });
    };

    /**
     * Создать объект метки
     * @param {Object} data
     * @return {SchemeDesigner.SchemeObject}
     */
    var createLabelObject = function (data) {
        var fontSize = data.s;
        var context = schemeDesigner.getView().getContext();
        context.font = fontSize + ' Arial';
        var textWidth = context.measureText(data.t).width;
        return new SchemeDesigner.SchemeObject({
            x: data.x,
            y: data.y,
            width: textWidth,
            height: parseInt(data.s),
            text: data.t,
            fname: data.f,
            fsize: data.s,
            color: data.c,
            active: false,
            renderFunction: renderLabel,
            cursorStyle: 'default',
            clickFunction: null
        });
    };

    /**
     * Создать объект карты мест
     * @param {SchemeDesigner.SchemeObject} schemeObject
     * @param {SchemeDesigner.Scheme} schemeDesigner
     * @param {SchemeDesigner.View} view
     * @return {SchemeDesigner.SchemeObject}
     */
    var createBackground = function (data) {
        return new SchemeDesigner.SchemeObject({
            x: 0.5,
            y: 0.5,
            width: 8600,
            height: 7000,
            cursorStyle: 'default',
            renderFunction: createBackgroundObject
        })
    };
    var createBackgroundObject = function(schemeObject, schemeDesigner, view) {
        var context = view.getContext();
        context.beginPath();
        context.lineWidth = 4;
        context.strokeStyle = 'rgba(12, 200, 15, 0.2)';
        context.fillStyle = 'rgba(12, 200, 15, 0.2)';

        var width = schemeObject.width;
        var height = schemeObject.height;

        context.rect(schemeObject.x, schemeObject.y, width, height);
        context.fill();
        context.stroke();
    };

    /**
     * Разбить текст на строки по ширине по пробелам
     * @param {SchemeDesigner.View} view
     * @param {String} text
     * @param {Number} width
     * @return {Array}
     */
    var getTextLinesBySpaces = function(view, text, width) {
        var context = view.getContext();
        var words = text.split(' ');
        var lines = [];
        var i = 0;
        while (i < words.length) {
            var line = '';
            do {
                if(line !== '') {
                    line += ' ';
                }

                line += words[i];

                i++;
                if(i === words.length) {
                    break;
                }

                var linePreview = line + ' ' + words[i];
                var metrics = context.measureText(linePreview);

            } while(metrics.width <= width);

            lines.push(line);
        }

        return lines;
    };

    /**
     * Разбить текст на строки по символам переноса и по пробелам
     * @param {SchemeDesigner.View} view
     * @param {String} text
     * @param {Number} width
     * @return {Array}
     */
    var getTextLines = function(view, text, width) {
        var result = [];
        var globalLines = text.split("\n");
        var i = 0;

        while (i < globalLines.length) {
            var globalLine = globalLines[i];
            var lines = getTextLinesBySpaces(view, globalLine, width);

            // разбиваем по пробелам
            var j = 0;
            while (j < lines.length) {
                var line = lines[j];
                result.push(line);
                j++;
            }

            i++;
        }

        return result;
    };

    /**
     * Отрисовать текст, разбитый по строкам
     * @param {SchemeDesigner.View} view
     * @param {Array} lines
     * @param {Number} lineHeight
     * @param {Number} x
     * @param {Number} y
     */
    var renderTextLines = function(view, lines, lineHeight, x, y) {
        var context = view.getContext();
        var i = 0;
        var currentY = y;
        while (i < lines.length) {
            var line = lines[i];
            context.fillText(line, x, currentY);
            currentY += lineHeight;
            i++;
        }
    };

    /**
     * Клик на место
     * @param {SchemeObject} schemeObject
     */
    var clickOnPlace = function (schemeObject)
    {
        var params = schemeObject.getParams();
        if (!params.avail) {
            return false;
        }

        schemeObject.params.isSelected = !schemeObject.params.isSelected;

        if (schemeObject.params.isSelected) {
            if (typeof onPlaceSelect === 'function') {
                onPlaceSelect.apply(schemeDesigner, [schemeObject]);
            }
        } else {
            if (typeof onPlaceUnselect === 'function') {
                onPlaceUnselect.apply(schemeDesigner, [schemeObject]);
            }
        }
    };


    /**
     * Клик на место
     * @param {SchemeObject} schemeObject
     */
    var clickOnFreeSeatingSector = function (schemeObject)
    {
        var params = schemeObject.getParams();
        if (!params.avail) {
            return false;
        }

        onFreeSeatingSectorClick.apply(schemeDesigner, [schemeObject]);
    };


    /**
     * Показать бабл места
     * @param {SchemeObject} schemeObject
     * @param {MouseEvent} event
     */
    var showPlaceTooltip = function(schemeObject, event) {
        if (typeof showTooltipFunction === 'function') {
            showTooltipFunction.apply(schemeDesigner, [tooltipElement, schemeObject, event]);
        }
    };

    /**
     * Скрыть лейбл места
     */
    var hidePlaceTooltip = function() {
        if (typeof hideTooltipFunction === 'function') {
            hideTooltipFunction.apply(schemeDesigner, [tooltipElement]);
        }
    };
    
    /**
     * Наведение на место
     * @param schemeObject
     * @param {MouseEvent} e
     */
    var mouseOverPlace = function(schemeObject, e) {
        if (!schemeObject.params.avail) {
            return;
        }

        // если это тач устройство, ховер запрещаем
        if (touchSupported) {
            return;
        }

        showPlaceTooltip(schemeObject, e);
    };

    /**
     * Снятие наведения с места
     * @param schemeObject

     */
    var mouseLeavePlace = function(schemeObject) {

        // если это тач устройство, ховер запрещаем
        if (touchSupported) {
            return;
        }

        hidePlaceTooltip();
    };

    /**
     * Клик на объект
     */
    canvasElement.addEventListener('schemeDesigner.clickOnObject', function (e) {
        var schemeObject = e.detail;
        if (schemeObject.getLayerId() === 'places') {
            clickOnPlace(schemeObject);
        } else if (schemeObject.getLayerId() === 'multiplePlaces') {
            clickOnFreeSeatingSector(schemeObject);
        }
    }, false);

    /**
     * Наведение на объект
     */
    canvasElement.addEventListener('schemeDesigner.mouseOverObject', function (e) {
        var schemeObject = e.detail.data;
        var event = e.detail.originalEvent;
        if (schemeObject.getLayerId() === 'places') {
            mouseOverPlace(schemeObject, event);
        }
    }, false);

    /**
     * Снятие наведения объекта
     */
    canvasElement.addEventListener('schemeDesigner.mouseLeaveObject', function (e) {
        var schemeObject = e.detail;
        if (schemeObject.getLayerId() === 'places') {
            mouseLeavePlace(schemeObject);
        }
    }, false);

    /**
     * Выход мышкой за пределы канваса
     */
    canvasElement.addEventListener('mouseleave', function (e) {
        hidePlaceTooltip();
    }, false);

    /**
     * После завершения построения
     */
    // canvas.addEventListener('schemeDesigner.beforeRenderAll', function (e) {
    //     console.time('renderAll');
    // }, false);
};
