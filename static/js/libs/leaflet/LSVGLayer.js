L.SVGOverlay = L.ImageOverlay.extend({
  initialize: function (source, bounds, options) {
    // (String, LatLngBounds, Object)
    this._url = source;
    this._bounds = toLatLngBounds(bounds);

    L.setOptions(this, options);
  },

  _initImage: function () {
    // var img = (this._image = L.DomUtil.create(
    //     'img',
    //     'leaflet-image-layer ' +
    //     (this._zoomAnimated ? "leaflet-zoom-animated" : '') +
    //     (this.options.className || '')
    // ));

    var img = (this._image = jQuery(this._url).attr('class', 'leaflet-image-layer ' +
         (this._zoomAnimated ? "leaflet-zoom-animated" : '') +
         (this.options.className || '')
        ).get(0));

    img.onselectstart = falseFn;
    img.onmousemove = falseFn;

    // @event load: Event
    // Fired when the ImageOverlay layer has loaded its image
    img.onload = bind(this.fire, this, "load");
    img.onerror = bind(this._overlayOnError, this, "error");

    if (this.options.crossOrigin) {
      img.crossOrigin = '';
    }

    if (this.options.zIndex) {
      this._updateZIndex();
    }

    img.src = this._url;
    img.alt = this.options.alt;
  },

});

L.svgOverlay = function (url, bound, options) {
  return new L.SVGOverlay(url, bound, options);
};

// @function falseFn(): Function
// Returns a function which always returns `false`.
function falseFn() { return false; }

// @function bind(fn: Function, …): Function
// Returns a new function bound to the arguments passed, like [Function.prototype.bind](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Function/bind).
// Has a `L.bind()` shortcut.
function bind(fn, obj) {
  var slice = Array.prototype.slice;

  if (fn.bind) {
    return fn.bind.apply(fn, slice.call(arguments, 1));
  }

  var args = slice.call(arguments, 2);

  return function () {
    return fn.apply(obj, args.length ? args.concat(slice.call(arguments)) : arguments);
  };
}

// @factory L.latLngBounds(corner1: LatLng, corner2: LatLng)
// Creates a `LatLngBounds` object by defining two diagonally opposite corners of the rectangle.

// @alternative
// @factory L.latLngBounds(latlngs: LatLng[])
// Creates a `LatLngBounds` object defined by the geographical points it contains. Very useful for zooming the map to fit a particular set of locations with [`fitBounds`](#map-fitbounds).
function toLatLngBounds(a, b) {
	if (a instanceof L.LatLngBounds) {
		return a;
	}
	return new L.LatLngBounds(a, b);
}
