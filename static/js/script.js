$(document).ready(function () {
    // Анимированный скролл
    $('.header__nav > a').click( function () { // ловим клик по ссылке с классом go_to
        var scroll_el = $(this).attr('href').replace('/',''); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top - 100 }, 500); // анимируем скроолинг к элементу scroll_el
            $('.header__nav').removeClass('active');
        }
    });

    // Отправка корп клиентам
    $('#corporate-clients .corporate__inputs .support__button').on("click", function () {
        var name = $(this).parents('.corporate__inputs').find('[name=name]').val();
            phone = $(this).parents('.corporate__inputs').find('[name=phone]').val();
            people_num = $(this).parents('.corporate__inputs').find('[name=count]').val();
            match_id = $(this).parents('.corporate__inputs').find('[name=match]').val();
            email = $(this).parents('.corporate__inputs').find('[name=email]').val();
            comments = $(this).parents('.corporate__inputs').find('[name=comment]').val();

        if (name.length && phone.length && match_id > 0) {
            $.ajax({
                url: '/ajax/',
                method: "POST",
                data: { type : 'corporate-form', match_id : match_id, name : name, email : email, phone : phone, people_num : people_num, comments : comments, csrfmiddlewaretoken: getCookie('csrftoken')},
                success: function (data, textStatus){
                    if (data != 'none') {
                        $('#callback_modal.thx .order_id').text(data);
                        $('#callback_modal.thx').addClass('active');
                        $('.corporate__form input').val('');
                        $('.corporate__form textarea').val('');
                        $('.corporate__form select').val(0);

                        $(".corporate__input").removeClass('error');
                    }
                }
            });
        } else {
            if (!name.length) {
                $(this).parents('.corporate__inputs').find('[name=name]').parent().addClass('error');
            } else {
                $(this).parents('.corporate__inputs').find('[name=name]').parent().removeClass('error');
            }

            if (!phone.length) {
                $(this).parents('.corporate__inputs').find('[name=phone]').parent().addClass('error');
            } else {
                $(this).parents('.corporate__inputs').find('[name=phone]').parent().removeClass('error');
            }

            if (match_id <= 0) {
                $(this).parents('.corporate__inputs').find('[name=match]').parent().addClass('error');
            } else {
                $(this).parents('.corporate__inputs').find('[name=match]').parent().removeClass('error');
            }
        }
    })

    // Отправка callback
    $('#callback_modal.callback_form .support__button, .match_empty .support__button').on("click", function () {
        var name = $(this).parents('.corporate__inputs').find('[name=name]').val()
            phone = $(this).parents('.corporate__inputs').find('[name=phone]').val()
            comments = $(this).parents('.corporate__inputs').find('.corporate__textarea textarea').val()

        if (name.length && phone.length) {
            $.ajax({
                url: '/ajax/',
                method: "POST",
                data: { type : 'callback-form', name : name, match : callback_match_info.name, datetime : callback_match_info.datetime, phone : phone, comments : comments, csrfmiddlewaretoken: getCookie('csrftoken')},
                success: function (data, textStatus){
                    if(data != 'none'){
                        $('#callback_modal.callback_form').removeClass('active')
                        $('#callback_modal.thx .order_id').text(data);
                        $('#callback_modal.thx').addClass('active');
                        $('.corporate__form input').val('');
                        $('.corporate__form textarea').val('');

                        $(".corporate__input").removeClass('error');
                    }
                }
            });
        } else {
            if (!name.length) {
                $(this).parents('.corporate__inputs').find('[name=name]').parent().addClass('error');
            } else {
                $(this).parents('.corporate__inputs').find('[name=name]').parent().removeClass('error');
            }

            if (!phone.length) {
                $(this).parents('.corporate__inputs').find('[name=phone]').parent().addClass('error');
            } else {
                $(this).parents('.corporate__inputs').find('[name=phone]').parent().removeClass('error');
            }
        }  
    })

    // Прозрачная шапка
    if ($('.banner').length) {
        $(window).on('scroll load', function () {
            if ($(this).scrollTop() > 0) {
                $('.header').removeClass('transparent');
            } else {
                $('.header').addClass('transparent'); 
            }
        });
    }

    // Меню в шапке
    $('.header_drop').on('click', function () {
        $(this).toggleClass('active'); 
    });

    // Команды
    $('.filter__teams').on('click', function () {
        if ($(this).hasClass('filtered')) {
            $(this).removeClass('filtered')
            $('.matches_ajax').fadeOut()
            $('.championat.static').fadeIn()
        } else{
            $('.filter__window').toggleClass('active');
        }
    });

    // Cлайдер в новостей
    if ($('.news__slider').length) {
        $('.news__slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });
    }

    // Схема / список
    $('.switch__element').on('click', function () {
        if (!$(this).hasClass('blank')) {
            var id = $(this).data('id');

            $('.map__condition, .switch__element').removeClass('active');
            $(this).addClass('active');
            $('.map__condition[data-id="' + id +'"]').addClass('active');
        } else {
            $('.mobile_info').removeClass('active');
        }
    });

    // Подсветка секторов
    $('.map_table tr').on('mouseenter', function () {
        var id = String($(this).data('slug')).toLowerCase();

        $('.map__small_map').find('.active#' + id).attr('class', 'active animated');
    });

    // Нажатие на схему рядом со списком и тригерное нажатие на эту строку в списке
    $('.map__small_map').on('click', '.active', function () {
        var id = String($(this).data('id')).toLowerCase();

        if (!$('.map_table tr[data-slug=' + id + ']').hasClass("loaded")) {
            $($('.map_table tr[data-slug=' + id + ']').find('.map_table__show')[0]).trigger('click');
        }  
    });

    $('.map_table tr').on('mouseleave', function () {
        var id = String($(this).data('slug')).toLowerCase();

        $('.map__small_map').find('.active#' + id).attr('class', 'active');
    });

    // Подсветка списка
    $('.map__small_map .active').on('mouseenter', function () {
        var id = String($(this)[0].getAttribute('id')).toLocaleUpperCase();

        $('.map_table tr[data-slug="' + id +'"]').addClass('animated');
    });

    $('.map__small_map .active').on('mouseleave', function () {
        var id = String($(this)[0].getAttribute('id')).toLocaleUpperCase();

        $('.map_table tr[data-slug="' + id +'"]').removeClass('animated');
    });

    // Открыть сектор
    $('.map__small_map .active').on('click', function () {
        var id = $(this)[0].getAttribute('id');
        
        $('.map_table tr').removeClass('first active');
        $('.map_table tr[data-id="' + id +'"]').removeClass('animated').addClass('first active');
    });


    if ($('#map').length) {
        init_scheme();

        function init_scheme() {
            var window_width = ($(window).width() / 100) * 90;
            
            if ($('#map > svg').length) {
                var $scheme = document.querySelector('#map > svg'),
                    viewBox = $scheme.getAttribute('viewBox').split(' ');

                window.map = L.map('map', {
                    crs: L.CRS.Simple,
                    zoom: 1,
                    minZoom: 0,
                    maxZoom: 4,
                    scrollWheelZoom: false,
                });

                var instant_width = parseInt(viewBox[2], 10),
                    instant_height = parseInt(viewBox[3], 10),
                    scale = window.innerWidth * .9 / instant_width;

                if (instant_height > instant_width) {
                    scale = window.innerWidth * .9  / instant_height
                }

                var width = (window.innerWidth * .8 ),
                    height = (window.innerHeight - 300);

                $('#map').css('height', height + 100 + 'px');

                L.svgOverlay(
                    '#map > svg',
                    [[0, 0], [(height), (width)]]
                ).addTo(map);

                $(".leaflet-control-zoom").stick_in_parent({
                    parent: $('#map'),
                    offset_top: 120
                });

                map.fitBounds([[0, 0], [(height + 50), (width)]]);
                map.setMaxBounds([[0, 0], [(height + 50), (width)]]);

                var $svg = $('.leaflet-overlay-pane').find('svg'),
                    $mapPane = $('.leaflet-map-pane');

                $('#map').on('mousedown', function (e) {
                    $svg.css('will-change', 'transform');
                    $mapPane.css('will-change', 'transform');
                }).on('mouseup', function (e) {
                    $svg.css('will-change', 'unset');
                    $mapPane.css('will-change', 'unset');
                });
            }
        }
    }

    // Pакрыть подробную схему
    $('body').on('click', '.sector_popup__close', function () {
        $('.sector_popup').removeClass('active');
        $('.sector_popup .mapa').html("");

        $('#map .active').removeClass("opacity");
        $('.legend_filter-sector .legend_filter__input, .legend_filter-sector .legend_filter__button').removeClass("active");
        $('.legend_filter-sector .filter_input').val("");
    }); 

    if (parseInt($('.header__counter').text())) {
        $('.mini_cart').removeClass('inactive');
    }

    // Pакрыть подробную схему
    $('.header__menu').on('click', function () {
        $(this).toggleClass('active');
        $('.header__nav').toggleClass('active');
    }); 

    // Pакрыть козину в шапке
    $('.header__cart').on('click', function () {
        if (!isMobile.any()) {
            $(this).siblings('.mini_cart_parrent').toggleClass('active');
        } else {
            location.href = '/checkout/';
        }
    }); 

    // Открыть нижнюю корзину (моб)
    $('.mobile_cart').on('click', function () {
        $('.mini_cart').addClass('active');
    }); 

    // Скрыть нижнюю корзину (моб)
    $('.cart_inner__close').on('click', function () {
        $('.mini_cart').removeClass('active');
    }); 

    $('.championat__button-request').on('click', function () {
        callback_match_info.name = $(this).parents('a.championat__element').data('match_name');
        callback_match_info.datetime = $(this).parents('a.championat__element').data('datetime');
        $('#callback_modal').addClass('active');
    });

    // Открыть заказ звонка
    $('.banner__callback, .callback_buttn').on('click', function () {
        $('#callback_modal').addClass('active');
    }); 

    // Скрыть модальник
    $('.modal__close').on('click', function () {
        $(this).closest('.modal').removeClass('active');
        callback_match_info.name = '';
        callback_match_info.datetime = '';
    }); 

    // Очистить заказ
    $('.bottom_cart__clear').on('click', function () {
        $('.remove_ticket').each(function () {
            $( this ).trigger('click');
        });
    }); 

    // Скролл
    $(window).on('scroll', function () {
        // Меню в шапке
        if ($('.header_drop').length) {
            $('.header_drop').removeClass('active');
        }
    });

    $(window).on('click', function (e) {
        // Фильтр
        if ($('.filter__window').length) {
            if (!$(e.target).parents('.filter__wrp').length) {
                $('.filter__window').removeClass('active');
            }
        }
    })

    // Поддрежи команду
    $(".to_matches").on("click", function () {
        $('body, html').animate({
            scrollTop: $('.matches').offset().top
        }, 500);
    });

    // Кнопка 'Назад к выбору билетов'
    $(".cart__back").on("click", function () {
        if (window.history.back() == undefined){
            window.location = '/';
        } else {
            window.history.back();
        }
    });

    // Скрыть описание события на моб.
    $(".map, .map_table, .map__condition").on("click, touchstart", function () {
        $('.mobile_info').removeClass('active');
    });

    // Показать описание события на моб.
    $(".match__h1-mob").on("click", function () {
        $('.mobile_info').addClass('active');
    });

    // Страны у телефона
    if ($(".cart").length) {
        window.intlTelInput($(".cart #phone")[0], {
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function () {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "RU";
                    callback(countryCode);

                    setTimeout(function () {
                        $("input[name='phone_code']").attr("value", $(".selected-flag").attr("code"));
                    }, 100);
                });
            },
            utilsScript: "/static/js/libs/intlTelInput/utils.js"
        });

        $(document).on("click", ".country", function () {
            phoneMask();
        });
    }

    if (!!jQuery.fn.multipleSelect) {
        $("#cert_filter").multipleSelect({
            single: 'true',
            animate: 'slide'
        });

        $("#user_currency").multipleSelect({
            single: 'true',
            animate: 'slide'
        });

        $('#group_select').multipleSelect({
            single: 'true',
            animate: 'slide'
        });
    }

    // Расчет всех услуг
    function calc_cert() {
        var nominal = 0;

        if ($('#summa').val()) {
            nominal = parseInt($('#summa').val());
        } else {
            nominal = parseInt($('.cert_filter option:selected').val());
        }

        $('.cert_dop__element input:checked').each(function () {
            nominal += parseInt($(this).data('price'));
        });

        $('#total').val(nominal);
    }

    // Очистка поля 'Сумма сертификата', если выбрали из селекта
    $('.cert_form select').on('change', function () {
        $('#summa').val('');
    });

    $('.cert_form input').on('change', function () {
        $(this).parents('.cert_dop__check').siblings().find('input').prop('checked', false);
        calc_cert();
    });

    calc_cert();

    // Открыть форму заказа
    $('.cert_certs__block').on('click', function () {
        $(".cert_window__image").attr("style", $(this).find(".cert_certs__img").attr("style"));
        $(".cert_window__h3 span").html($(this).find(".cert_certs__h3").html());
        $("input[name=type]").attr("value", $(this).find(".cert_certs__h3").html());

        $(".cert_window, .cert_overlay").addClass("active");
        $("body").addClass("body_noscroll");
    });
    
    // Закрыть форму заказа
    $('.cert_window__close, .cert_overlay').on('click', function () {
        $(".cert_window, .cert_overlay").removeClass("active");
        $("body").removeClass("body_noscroll");
    });

    $('#certificate-form').on('submit', function () {
        $('.cert_submit__wrp').addClass("loading");
        sendCertificate();
        return false;
    });

    // Валидация формы
    $("#order_cart input").on("change input", function () {
        $("#order_cart input").each(function () {
            var input = $(this),
                id = input.attr('id');

            switch (id) {
                case 'name':
                    input.addClass('ok');
                    input.removeClass('error');
                    name_val = input.val();

                    if (name_val.length > 0) {
                        input.removeClass('error');
                    } else {
                        input.removeClass('ok');
                        input.addClass('error');
                    }
                    break;
                case 'phone':
                    input.addClass('ok');
                    input.removeClass('error');
                    phone_val = input.val();

                    if (phone_val.length > 0) {
                        input.removeClass('error');
                    } else {
                        input.removeClass('ok');
                        input.addClass('error');
                    }
                    break;
                case 'email':
                    input.addClass('ok');
                    input.removeClass('error');
                    email_val = input.val();

                    if ($("#pay_3").is(':checked')) {
                        if (email_val.length > 0 && email_val.indexOf('@') >= 0 && email_val.indexOf('.') >= 0) {
                            input.removeClass('error');
                        } else {
                            input.removeClass('ok');
                            input.addClass('error');
                        }
                    }
                    break;
            }
        });

        if ($("#order_cart #name").hasClass("ok") &&
            $("#order_cart #phone").hasClass("ok") &&
            $("#order_cart #email").hasClass("ok")) {
            $(".cart__submit").addClass('active');
        } else {
            $(".cart__submit").removeClass('active');
        }
    });

    // Переключение подсказок у радио в корзине
    $("body").on("click", ".cart__check", function () {
        var id = $(this).data('id');

        $('.cert_input, .cart_aletrs').removeClass('active');

        switch ($(this).data('id')) {
            case "cart_aletrs":
                $(this).find('.cart_aletrs').addClass('active');
                break;
            case "cert_input":
                $(this).find('.cert_input').addClass('active');
                break;
        }
    });

    // Закрыть окно подсказки
    $('.map_profit').on('submit', function () {
        $(this).removeClass("active");
    });

    $("#order_cart #phone").on("click", function () {
        phoneMask();
    });

    var input = document.querySelector("#order_cart #phone"),
    handleChange = function () {
        $("input[name='phone_code']").attr("value", $(".selected-flag").attr("code"));
    };

    if ($("#order_cart #phone").length){
        input.addEventListener('change', handleChange);
        input.addEventListener('keyup', handleChange);
    }

    $(".country-name").on("click", function () {
        setTimeout(function () {
            $("input[name='phone_code']").attr("value", $(".selected-flag").attr("code"));
        }, 100);
    });

    // Добавление к номеру телефона кода
    $(".cart__submit").on("click", function () {
        var code = $(".selected-flag").attr("code") ? $(".selected-flag").attr("code") : "";

        if ($("#order_cart #phone").val()) {
            $("#order_cart #phone").attr("value", code + " " + $("#order_cart #phone").val());
        }
    });

    // Цифры в кругах
    $("body").on("mouseenter click", "#crocus .active", function () {
        $("[data-text=" + $(this).attr("hash") + "]").show();
    });

    // Показать подсказку сектора
    $(".sec_desc").on("click", function () {
        $(this).parent().find(".drop_col").toggleClass("active");
    });

    // Переключение валюты
    $('[name=user_currency]').on('change', function () {
        var val = $(this).val();

        $.ajax({
            url: "/ajax/",
            method: "POST",
            data: { type : 'change_user_currency', pk : val, csrfmiddlewaretoken: getCookie('csrftoken')},
            success: function (data, textStatus){
                window.location.reload()
            }
        })
    });

    // Турнирная таблица моб.
    $(".tour_t__wrp-title .tour_t__col").on("click", function () {
        var id = $(this).index() + 1;

        $(".tour_t__wrp-title .tour_t__col, .tour_t__col-teams").removeClass("active");

        $(this).addClass("active");
        $(".tour_t__col-teams:nth-child(" + id + ")").addClass("active");
    });

    // Jivosite trigger
    $('body, html').on('click', '.info_block__show-jivo', function () {
        $('.globalClass_ET, body div#jivo-iframe-container:not(.jivo-c-mobile)').show();
        jivo_api.open({start: 'chat'});
    });

    // Открыть фильтр в легенде
    $('.legend_filter__button').on('click', function () {
        if ($(this).hasClass("active")) {
            $('#map .active').removeClass("opacity");
            $(this).parent().find('.filter_input').val("");
        }

        $(this).toggleClass('active');
        $(this).parent().find('.legend_filter__input').toggleClass('active');

        $('.legend_row').animate({
            scrollLeft: $(".legend_row").width()
        }, 500);
    });

    // Фильтр легенды на общей схеме
    $(".legend_filter-scheme .filter_input").on('keyup', function (e) {
        var filter_price = $(this).val();

        $('.legend_row_el').removeClass("active");
        $('.legend_filter__button').addClass("loading"); 
        $('#map .active').removeClass("opacity");

        $('#map .active').each(function () {
            var sector_price = parseInt($(this).data("price").split('-')[0]);

            if (sector_price > filter_price) {
                $(this).addClass("opacity"); 
            }
        });

        setTimeout(function () {
            $('.legend_filter__button').removeClass("loading"); 
        }, 500);

        custom_filtr();
    });

    // Фильтр легенды на схеме сектора
    $(".legend_filter-sector .filter_input").on('keyup', function (e) {
        var filter_price = $(this).val();

        $('#mapa .active').removeClass("opacity");
        $('.legend_filter__button').addClass("loading");
        $('#mapa .active').removeClass("opacity");

        $('#mapa .active').each(function () {
            var sector_price = parseInt($(this).data("price"));

            if (sector_price > filter_price) {
                $(this).addClass("opacity");
            }
        });

        setTimeout(function () {
            $('.legend_filter__button').removeClass("loading"); 
        }, 500);

        custom_filtr();
    });

    // Включить фильтр
    $("body").on("click", ".legend_row_el", function () {
        $(this).toggleClass("active");
        $('#map .active').addClass("opacity");

        if ($('.legend_row_el.active').length) {
            $('.legend_row_el.active').each(function () {
                $('#map .active.m_' + $(this).data("color")).removeClass("opacity");
            });
        } else {
            $('#map .active').removeClass("opacity"); 
        }

        $('.legend_filter-scheme .legend_filter__input, .legend_filter-scheme .legend_filter__button').removeClass("active");
        $('.legend_filter-scheme .filter_input').val("");

        color_filtr_sheme();
    }); 
    
});

$(window).on('resize load', function () {
    // Видео на баннере
    if ($('.banner__video').length) {
        if (($(window).width() / $(window).height()) > 1.777) {
            $('.banner__video').addClass('rotated');
        }
    }

    $('.banner__video').addClass('active');   
});

// Падеж слова
function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

// Детект мобильного браузера
var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

// Маска для телефона
function phoneMask() {
    $('#order_cart #phone').mask($('#order_cart #phone').attr('placeholder').replace(/\d/g, "0") + "0000000000");
}