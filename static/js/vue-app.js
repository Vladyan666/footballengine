const requests = axios.create({
    baseURL: '/',
    headers: {'X-CSRFToken': window.csrf}
});


var app = new Vue({
    delimiters : ["[[","]]"],
	el: '#vue-app',
	data: {
		tabs: [''],
        // для создания матча
		match: {
			command_first: {
				name: '',
				pk: 0
            },
			command_second: {
				name: '',
				pk: 0
            },
			stadium: {
				name: '',
				pk: 0
			},
			champ: {
				name: '',
				pk: 0
			},
			datetime: {
			    date: '',
                field: 'date'
            },
            group: ''
		},
        // для создания стадионов
        stadiums: '',
        // для создания чемпионатов
        champs:{
		    name: '',
            pk: 0
        },
        // для создания команды
        team: {
		    name: '',
            team_type: 1,
        },
        // хранилище для выпадайки
        storage : {
		    command_first: [],
		    command_second: [],
		    stadium: [],
		    champ: [],
        },
        // ответ при создании матча
        resp: {
		    match: '',
            staticmatch: ''
        }
	},
    watch: {
        'match.group': function (val) {
            console.log(val)
        }
    },
	methods: {
        set_value(i, item){
            this.match[$(item.target).parents('.form-check-label').find('input').data('field')] = i
            this.storage[$(item.target).parents('.form-check-label').find('input').data('field')] = []
        },
        set_value2(i, item){
            this['champs'] = i
            this.storage['champ'] = []
        },
        create_team(){
           let vm = this
            $.ajax({
                method: "POST",
                data: vm.team,
                url: '/api/team/',
                success: function(data){
                    console.log(data)
                    iii = data
                    vm.team = {
                        name: '',
                        team_type: 1
                    }
                    $('.btn-secondary').trigger('click')
                }
            })
        },
        updateStadiums(){
            let vm = this;
            if(vm.champs.pk){
                $.ajax({
                    url: '/control_panel/',
                    method: "POST",
                    data: {
                        type : 'updatestads',
                        champ : vm.champs.pk
                    },
                    success: function (data, textStatus){
                        alert(data + ' - матчей обновлено')
                    }
                })
            }
            else{
                alert('Выберите чемпионат')
            }
        },
        parseStadiums() {
            let vm = this;
            if(vm.stadiums){
                $.ajax({
                    method: "POST",
                    data: {
                        type: 'create_stads',
                        url: vm.stadiums
                    },
                    url: '/control_panel/',
                    success: function(data){
                        alert(String(data) + ' Стадионов создано')
                        vm.stadiums = ''
                    }
                })
            }
        },
        create_match(){
            let vm = this;
            $.ajax({
                method: "POST",
                data: {
                    command_first: vm.match.command_first.pk,
                    command_second: vm.match.command_second.pk,
                    stadium: vm.match.stadium.pk,
                    champ: vm.match.champ.pk,
                    datetime: vm.match.datetime.date,
                    group: vm.match.group
                },
                url: '/api/match/custom_create/',
                success: function(data){
                    vm.match = {
                        command_first: {
                            name: '',
                            pk: 0
                        },
                        command_second: {
                            name: '',
                            pk: 0
                        },
                        stadium: {
                            name: '',
                            pk: 0
                        },
                        champ: {
                            name: '',
                            pk: 0
                        },
                        datetime: {
                            date: '',
                            field: 'date'
                        },
                        group: ''
                    }
                    vm.resp = data
                }
            })
        }
	},
	beforeMount(){

	},
	mounted (){
	    let vm = this;
        $(document).ready(function () {
            jQuery.curCSS = function(element, prop, val) {
                return jQuery(element).css(prop, val);
            };
            $(function() {
                $('.autosearch').autocomplete({
                    source: function(request, response){
                        // организуем кроссдоменный запрос
                        if( request.term.length > 0 ){
                            curr_object = $(this)[0].element
                            $.ajax({
                                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                                method: "GET",
                                data : {
                                    name: request.term
                                },
                                url: "/api/" + $(this)[0].element.attr('name') + "/",
                                // обработка успешного выполнения запроса
                                success: function(data){
                                    vm.storage[curr_object.data('field')] = data.results
                                }
                            })
                        }
                        else{
                            vm.storage[$(this)[0].element.data('field')] = []
                        }
                    },
                    minLength: 0,
                    select: function( event, ui ) {
                        log( ui.item ?
                            "Выбрано: " + ui.item.value + " aka " + ui.item.id :
                            "Нет ничего подходящего " + this.value );
                    }
                })
            })
        })
	},

})