var app = new Vue({
    delimiters : ["[[","]]"],
	el: '#vue-area',
	data: {
		static_tickets: [],
		carryall_tickets: [],
		tickets_count: 0,
		tickets_price: 0,
		tickets_slug: 'билет'
	},
	methods: {
		// Падеж слова
		declOfNum(number, titles) {
			cases = [2, 0, 1, 1, 1, 2];
			return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
		},
		// Подсчет кол-ва билетов и общей цены
		totalTicketsCountAndPrice(){
			let vm = this,
				count = 0,
				total_price = 0;

			for (let i = 0; i < vm.carryall_tickets.length; i++) {
				count += vm.carryall_tickets[i].count;
				total_price += vm.carryall_tickets[i].count * vm.carryall_tickets[i].price;
			}

			for (let i = 0; i < vm.static_tickets.length; i++) {
				count += vm.static_tickets[i].count;
				total_price += vm.static_tickets[i].count * vm.static_tickets[i].price_val;

				if ($('.static_ticket_buy input[data-id=' + vm.static_tickets[i].price + ']')) {
					$('.static_ticket_buy input[data-id=' + vm.static_tickets[i].price + ']').val(vm.static_tickets[i].count);
				}
			}

			if (count) {
				$('.mini_cart').removeClass('inactive');
				$('.mini_cart, .mobile_cart').addClass('active');
			} else {
				$('.mini_cart').addClass('inactive');
				$('.mini_cart, .mobile_cart').removeClass('active');
			}

			vm.tickets_slug = vm.declOfNum(vm.tickets_count, ['билет', 'билета', 'билетов']);
			vm.tickets_price = total_price;
			vm.tickets_count = count;
		},
		
    	// Обновить данные корзины
    	reloadTickets(data){
			let vm = this;
			
			vm.carryall_tickets = data['c_tickets'];
			vm.static_tickets = data['s_item'];
			window.added_ticks = data['c_tickets'].map(k => k['hash']);
			vm.totalTicketsCountAndPrice();
		},

		// Загрузить корзину (при загрузке страницы)
		getCart(){
			let vm = this;
			
			$.ajax({
				url: '/ajax/',
				method: "POST",
				data: {
					type: 'get_cart'
				},
				success: function (data, textStatus){
					vm.reloadTickets(data);
				}
			});
		},

		// Удалить билет кериола
		removeTicketC(c){
			let vm = this;
			
			$.ajax({
				url: '/ajax/',
				method: "POST",
				data: {
					id: c.id,
					hash: c.hash,
					type: 'remove_carryall_ticket_fromcart'
				},
				success: function (data, textStatus){
					vm.reloadTickets(data);
				}
			});
		},

		// Удалить билет админочный (полностью)
		removeTicketS(c){
			let vm = this;
			
			$.ajax({
				url: '/ajax/',
				method: "POST",
				data: {
					id: c.id,
					type: 'mass_del_static_ticket'
				},
				success: function (data, textStatus){
					vm.reloadTickets(data);
					$('.static_ticket_buy input[data-id=' + c.price + ']').val('0');
				}
			});
		},

		// Удалить билет админочный (полностью)
		removeTicketSone(c){
			let vm = this;
			
			$.ajax({
				url: '/ajax/',
				method: "POST",
				data: {
					id: c.id,
					type: 'mass_del_static_ticket'
				},
				success: function (data, textStatus){
					vm.reloadTickets(data);
				}
			});
		},
	},
	beforeMount(){

	},
	mounted (){
		let vm = this;

		vm.getCart();

        // Покупка одиночных билетов кериола
        $('body').on('click', '.mapa .active, .book_carryall_tick .map_table__check', function(){

			// Проверка схема или список
        	if ($(this).hasClass('map_table__check')) {
				var ticket_inf = JSON.stringify($(this).parents('.map_table__row').data('json')),
					type = 'put_carryall_ticket';
					
            	if ($(this).hasClass('active')) {
            		type = 'remove_carryall_ticket';
				}

            	$(this).toggleClass('active');
			} else {
            	var ticket_inf = $(this).attr('json').replace(/'/g,'"').replace(/\\n/g, '^');

				if ($(this).attr('class').split(' ')[1] != 'selected') {
					var type = 'put_carryall_ticket';

					$(this).attr('class', 'active selected');
				} else {
					var type = 'remove_carryall_ticket';

					$(this).attr('class', 'active');
				}
			}
			// Запрос
			$.ajax({
				url: '/ajax/',
				method: "POST",
				data: {type: type, json: ticket_inf, match_id: window.data_id},
				success: function (data, textStatus) {
					vm.reloadTickets(data);
				}
			});
        })

        // Покупка ручных билетов из админки
        $('.static_ticket_buy:not(.static_ticket_buy_m) span').on('click', function(){
            var id = $(this).parent().find('input').data('id');

            if ($(this).hasClass('plus')) {
                var c = parseInt($(this).siblings('input').val()) + 1;
				
				$(this).siblings('input').val(c);

                // Добавление в корзину
                $.ajax({
                    url: '/ajax/',
                    method: "POST",
                    data: { type : 'add_static_ticket', id : id},
                    success: function (data, textStatus){
                        vm.reloadTickets(data);
                    }
                });
            } else {
				var c = parseInt($(this).siblings('input').val()) - 1;
				
                if (c >= 0) {
					$(this).siblings('input').val(c);
					
					del_id = $('.caryall_mini_row-static[data-pr_id='+ id + ']').data('id');

					// Удаление из корзины
					$.ajax({
						url: '/ajax/',
						method: "POST",
						data: { type : 'del_static_ticket', id : del_id},
						success: function (data, textStatus){
							vm.reloadTickets(data);
						}
					});
                }
            }

            $(this).toggleClass('active');
        })

        // Мультибилетов
        $('body').on('click', '.static_ticket_buy.static_ticket_buy_m span', function(){
            var id = $(this).parent().find('input').data('id'),
                count = parseInt($(this).siblings('input').val()),
                max_count = $(this).parent().find('input').data('count'),
				ticket_inf = $(this).siblings('input').attr('data-json').replace(/'/g,'"').replace(/\\n/g, '^');
				
            if ($(this).hasClass('plus')) {
				if (count < max_count) {
					var count = parseInt($(this).siblings('input').val()) + 1;

					$(this).siblings('input').val(count);

					// Добавление в корзину
					$.ajax({
						url: '/ajax/',
						method: "POST",
						data: {
							type : 'put_carryall_ticket',
							json : ticket_inf,
							count: count,
							match_id: window.data_id
						},
						success: function (data, textStatus){
							vm.reloadTickets(data);
						}
					});
				}
            } else {
                if (count > 0) {
                    count = count - 1;
                    $(this).siblings('input').val(count);

                    // Удаление из корзины
                    $.ajax({
                        url: '/ajax/',
                        method: "POST",
                        data: {
                            type : 'remove_carryall_ticket',
							json : ticket_inf,
							count: count,
							match_id: window.data_id
                        },
                        success: function (data, textStatus){
                            vm.reloadTickets(data);
                        }
                    });
                }
            }
		});
	}
});