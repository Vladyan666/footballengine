from rest_framework.routers import DefaultRouter
from .api import *
router = DefaultRouter()

router.register(r'team', TeamViewSet)
router.register(r'champ', ChampViewSet)
router.register(r'stadium', StadiumViewSet)
router.register(r'match', MatchViewSet)
router.register(r'siteorder', SiteOrderViewSet)
router.register(r'ticketcarryall', TicketCarryallViewSet)
#router.register(r'event-place', EventPlaceViewSet)