import requests
import dateutil.parser
import django_filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.pagination import PageNumberPagination
from rest_framework import serializers, viewsets, status
from rest_framework.authentication import BaseAuthentication, SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from .serializers import *
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter
from footballengine.settings import settings_global as djang_settings

CARRYALL_TOKEN = '457aec5c56d5393d5ee4bacb9512a5d641227ca5'
CARRYALL_HEADERS = {'Authorization': 'Token %s' % CARRYALL_TOKEN}
CARRYALL_URL = 'http://carryall2.tix-system.com'

class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS

class TeamFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = Team
        fields = ['name']

class MatchFilter(django_filters.FilterSet):
    champ = django_filters.CharFilter(field_name="champ__name", lookup_expr="icontains")

    class Meta:
        model = Match
        fields = ['champ']

class ChampFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = Champ
        fields = ['name']

class StadiumFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = Stadium
        fields = ['name']

class SiteOrderFilter(django_filters.FilterSet):
    utm = django_filters.CharFilter(field_name="utm", lookup_expr="icontains")
    bin_from = django_filters.CharFilter(field_name="custom_bin_id", lookup_expr="gte")
    bin_to = django_filters.CharFilter(field_name="custom_bin_id", lookup_expr="lte")

    class Meta:
        model = SiteOrder
        fields = ['name', 'phone', 'email', 'custom_bin_id', 'bin_id', 'utm', 'bin_from', 'bin_to']

class DefaultPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 30

class TeamViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = TeamFilter
    pagination_class = DefaultPagination

class ChampViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Champ.objects.all()
    serializer_class = ChampSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = ChampFilter
    pagination_class = DefaultPagination

class StadiumViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Stadium.objects.all()
    serializer_class = StadiumSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = StadiumFilter
    pagination_class = DefaultPagination

class TicketCarryallViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = TicketCarryall.objects.all()
    serializer_class = TicketCarryallSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    pagination_class = DefaultPagination


class SiteOrderViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = SiteOrder.objects.all()
    serializer_class = SiteOrderSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = SiteOrderFilter
    pagination_class = DefaultPagination

class MatchViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Match.objects.all().select_related('champ',)
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = MatchFilter
    serializer_class = MatchSerializer


    @list_route(methods=['post'])
    def custom_create(self, request):
        from datetime import datetime
        import pytils
        import re
        import requests
        # строим дату
        dt = datetime.strptime(request.POST.get('datetime', datetime.now()), '%Y-%m-%dT%H:%M')
        # teams
        team1 = Team.objects.get(pk=request.POST.get('command_first', None))
        team2 = Team.objects.get(pk=request.POST.get('command_second', None))
        # матч превью
        # team_1_slug = re.sub(' ', '-', team1.name)
        # team_1_slug = re.sub("[\.|\#|\(|\)|\']", '', team_1_slug)
        # team_2_slug = re.sub(' ', '-', team2.name)
        # team_2_slug = re.sub("[\.|\#|\(|\)|\']", '', team_2_slug)
        team_1_slug = team1.alias
        team_2_slug = team2.alias
        teams_alias = pytils.translit.translify(team_1_slug + '-' + team_2_slug).lower()
        staticmatch, created = Staticmatch.objects.get_or_create(command_first=team1, command_second=team2, defaults={'alias': teams_alias})
        # чемпионат
        champ = Champ.objects.get(pk=request.POST.get('champ', None))
        champ.teams.add(team1,team2)
        # стадион
        stadium = Stadium.objects.get(pk=request.POST.get('stadium', None))

        params = dict(
            name=team1.name + ' - ' + team2.name,
            champ=champ,
            static_match=staticmatch,
            stadium=stadium,
            command_first=team1,
            command_second=team2,
            datetime=dt
        )

        # group
        if request.POST.get('group', None):
            group, g_create = Group.objects.get_or_create(name=request.POST.get('group', None), champ=champ)

            params.update({
                'group' : group
            })


        teams_alias = teams_alias + '-' + datetime.strftime(dt, '%d-%m-%Y')
        match, creat = Match.objects.get_or_create(**params, defaults={'alias': teams_alias})

        resp = {}
        if created:
            resp.update({
                'staticmatch': staticmatch.get_absolute_url()
            })
        if creat:
            resp.update({
                'match': match.get_absolute_url()
            })

        if djang_settings.DEBUG:
            if creat:
                message = '{} создал матч: {} в {}'.format(request.user.username, team1.name + ' - ' + team2.name + ' ' + match.get_absolute_url(), datetime.now().strftime('%d-%m-%Y %H:%M'))
                requests.get('https://api.telegram.org/bot918983608:AAGOnF6VQL7Z5K2HJIwL17WxIlgTCdWM2A0/sendMessage?chat_id=-395781011&text={}'.format(
                        message), timeout=2)
        return Response(resp)

# class EventPlaceViewSet(viewsets.ModelViewSet):
#     queryset = EventPlace.objects.all()
#     serializer_class = EventPlaceSerializer
#
#     # обновить площадки в бд
#     @list_route(methods=['get'])
#     def checkout_places(self, request):
#         values = []
#         created = 0
#         def get_places(num):
#             r = requests.get('http://carryall2.tix-system.com/api_crud/event_places/?city_id=1&page={}'.format(num), headers=CARRYALL_HEADERS).json()
#             try:
#                 values.extend(r['results'])
#             except:
#                 pass
#
#             if r['next']:
#                 get_places(r['next'].split('page=')[1])
#
#         get_places(1)
#
#         for i in values:
#             obj, create = EventPlace.objects.update_or_create(city=City.objects.get(api_id=i['city_id']), api_id=i['id'], name=i['name'], is_active=i['is_active'])
#             created += create
#
#         return HttpResponse(str(created))
#
# class HallViewSet(viewsets.ModelViewSet):
#     select_related = ('event_place',)
#     queryset = Hall.objects.select_related(*select_related)
#     serializer_class = HallSerializer
#
#     #обновить залы в бд
#     @list_route(methods=['get', 'post'])
#     def checkout_scenes(self, request):
#         values = []
#         created = 0
#         created2 = 0
#
#         def get_scenes(num):
#             r = requests.get('http://carryall2.tix-system.com/api_crud/halls/?city_id=1&page={}'.format(num),
#                              headers=CARRYALL_HEADERS).json()
#             try:
#                 values.extend(r['results'])
#             except:
#                 pass
#
#             if r['next']:
#                 get_scenes(r['next'].split('page=')[1])
#
#         get_scenes(1)
#
#         for i in values:
#             try:
#                 obj, create = Hall.objects.update_or_create(api_id=i['id'], name=i['name'], event_place=EventPlace.objects.get(api_id=i['event_place_id']), slug=i['slug'])
#                 created += create
#                 if i['hall_configuration_ids']:
#                     r2 = requests.get('http://carryall2.tix-system.com/api_crud/hall_configurations/?hall_id={}'.format('&hall_id='.join([str(qqq) for qqq in i['hall_configuration_ids']])), headers=CARRYALL_HEADERS).json()
#                     for s in r2['results']:
#                         obj2, created2 = HallConfiguration.objects.update_or_create(api_id=s['id'], defaults={'name': s['name'], 'hall': obj, 'slug': i['slug'], 'hall_map': s['hall_map']})
#             except:
#                 pass
#
#         return HttpResponse('Залов созданно - {}, конфигураций залов - {}'.format(str(created), str(created2)))
#
#     #получить залы для площадки
#     @list_route(methods=['post'])
#     def get_halls_by_ep(self, request):
#         halls = Hall.objects.filter(event_place=EventPlace.objects.get(api_id=request.data.get('id', 0)))
#         response = self.get_serializer(halls, many=True)
#         return Response(response.data)
#
#
# class SectorViewSet(viewsets.ModelViewSet):
#     queryset = Sector.objects.all()
#     serializer_class = SectorSerializer
#
#     #получить сектора для зала
#     @list_route(methods=['post'])
#     def get_sectors_by_hall(self, request):
#         sectors = Sector.objects.filter(hall=HallConfiguration.objects.get(api_id=request.data.get('id', 0)))
#         response = self.get_serializer(sectors, many=True)
#         return Response(response.data)
#
#     #обновить сектора в бд
#     @list_route(methods=['get'])
#     def checkout_sectors(self, request):
#         results = []
#         created2 = 0
#         # try:
#         for q in HallConfiguration.objects.all().order_by('-id'):
#             r2 = requests.get('http://carryall2.tix-system.com/api_crud/sectors/?hall_configuration_id={}'.format(q.api_id), headers=CARRYALL_HEADERS).json()
#
#             for i in r2['results']:
#                 try:
#                     obj2, create2 = Sector.objects.update_or_create(hall=q, name=i['name'], defaults={'api_id': i['id'], 'slug': i['slug'], 'is_active': i['is_active']})
#                     created2 += create2
#                 except:
#                     pass
#         # except:
#         #     pass
#
#         return HttpResponse('Секторов созданно - {}'.format(str(created2)))
#
#
# class SingleEventViewSet(viewsets.ModelViewSet):
#     queryset = SingleEvent.objects.all()
#     serializer_class = SingleEventSerializer
#
#
# class SeanceFilter(filters.FilterSet):
#     class Meta:
#         model = Seance
#         fields = ['starts_at', 'event', 'hall']
#
# class SeanceViewSet(viewsets.ModelViewSet):
#     authentication_classes = (SessionAuthentication, TokenAuthentication)
#     queryset = Seance.objects.all()
#     serializer_class = SeanceSerializer
#     filter_backends = (filters.DjangoFilterBackend, SearchFilter)
#     filterset_class = SeanceFilter
#
#     #получить список сеансов с API
#     @list_route(methods=['post'])
#     def get_seances_by_hall(self, request):
#         # seances = requests.get(CARRYALL_URL + '/api/seances_list/?hall_ids={}&format=json'.format(request.data.get('id', 0)),
#         #                        headers=CARRYALL_HEADERS).json()['seances']
#         seances = []
#
#         def get_seances(num):
#             r = requests.get(CARRYALL_URL + '/api_crud/seances/?hall_id={}&page={}&date_from={}&format=json'.format(request.data.get('id', 0), num, datetime.now().strftime('%Y-%m-%d')),
#                          headers=CARRYALL_HEADERS).json()
#             try:
#                 seances.extend(r['results'])
#             except:
#                 pass
#
#             if r['next']:
#                 get_seances(r['next'].split('page=')[1].split('&')[0])
#
#         get_seances(1)
#
#         events = list(set([i.get('event_id') for i in seances]))
#
#         events_names = []
#
#         def get_events(num, events):
#             temp = {
#                 'id': events,
#                 'page': num,
#                 'format': 'json'
#             }
#             r = requests.get(CARRYALL_URL + '/api_crud/events/', params=temp, headers=CARRYALL_HEADERS).json()
#
#             events_names.extend(r.get('results', []))
#
#             if r.get('next'):
#                 get_events(r['next'].split('page=')[1].split('&')[0], events)
#
#         if events:
#             get_events(1, events)
#
#
#         seances = sorted(seances, key=lambda k: k['starts_at'])
#
#         for i in seances:
#             i['starts_at'] = dateutil.parser.parse(i['starts_at']).strftime('%d-%m-%Y, %H:%M')
#             for e in events_names:
#                 if i['event_id'] == e['id']:
#                     i['name'] = e['name']
#                     i['slug'] = e['slug']
#                     break
#
#         return Response(seances)
#
#     #получить биелеты с API для списка
#     @list_route(methods=['post'])
#     def get_parser_tickets(self, request):
#         item = requests.get(CARRYALL_URL + '/api/event_tickets/?seance_id={}&format=json'.format(request.data.get('id', 0)),
#                             headers=CARRYALL_HEADERS).json()
#         tickets = get_tickets_groups_(item['tickets'])
#
#         for i in tickets:
#             try:
#                 i['select_seats'] = []
#                 i['seats'].sort(key=lambda x: (int(x[0])))
#             except Exception:
#                 pass
#
#
#         return Response(tickets)
#
#     #получить билеты с API для схемы
#     @list_route(methods=['post'])
#     def get_parser_tickets_scheme(self, request):
#         tickets = requests.get(CARRYALL_URL + '/api/event_tickets/?seance_id={}&format=json'.format(request.data.get('id', 0)),
#                                headers=CARRYALL_HEADERS).json()
#         return Response(tickets)
#
#     #получить схему
#     @list_route(methods=['post'])
#     def get_scheme(self, request):
#         event = requests.get(CARRYALL_URL + '/api/seance/?id={}&format=json'.format(request.data.get('id', 0)),
#                              headers=CARRYALL_HEADERS).json()
#         svg_map = requests.get(event['hall']['hall_map']).text.encode('utf-8')
#         return Response(svg_map)
#
#
# class OrderViewSet(viewsets.ModelViewSet):
#     queryset = Order.objects.all().select_related('buyer', 'seller', 'tickets', 'offer', 'offer__sector', 'offer__seance__event')
#     serializer_class = OrderSerializer
#
#     #создать сделку
#     @list_route(methods=['post'])
#     def create_order(self, request):
#         if request.data[0].get('pk', 0):
#             seller = Broker.objects.get(pk=request.data[0].get('broker'))
#             buyer = Broker.objects.get(pk=request.user.id)
#             order = Order.objects.create(buyer=buyer,
#                                  seller=seller,
#                                  comment=request.data[1].get('user_comment'),
#                                  take_time='{}, с {} до {}'.format(request.data[1].get('user_date'),
#                                                                                         request.data[1].get('user_hour_start'),
#                                                                                         request.data[1].get('user_hour_end')))
#             order.offer = Offer.objects.get(id=request.data[0].get('pk'))
#             tickets = [Ticket.objects.get(offer__pk=request.data[0].get('pk'), seat=i) for i in request.data[0].get('select_seats')]
#             order.tickets.add(*tickets)
#             order.save()
#             for t in tickets:
#                 t.status = '3'
#                 t.buyer = buyer
#                 t.in_order = order
#                 t.save()
#             response = order.pk
#
#             log_action(request.user, 'stock', 'book_create',
#                        offer=order.offer, order=order)
#         else:
#             response = 'Онлайн бронь в разработке'
#         return Response(response)
#
#     #создать сделку
#     @list_route(methods=['post'])
#     def create_order_mass(self, request):
#         response = []
#         if len(request.data[0]):
#             for item in request.data[0]:
#                 seller = Broker.objects.get(pk=item.get('broker'))
#                 buyer = Broker.objects.get(pk=request.user.id)
#                 order = Order.objects.create(buyer=buyer,
#                                      seller=seller,
#                                      comment=request.data[1].get('user_comment'),
#                                      take_time='{}, с {} до {}'.format(request.data[1].get('user_date'),
#                                                                                             request.data[1].get('user_hour_start'),
#                                                                                             request.data[1].get('user_hour_end')))
#                 order.offer = Offer.objects.get(id=item.get('pk'))
#                 tickets = [Ticket.objects.get(offer__pk=item.get('pk'), seat=i) for i in item.get('select_seats')]
#                 order.tickets.add(*tickets)
#                 order.save()
#                 for t in tickets:
#                     t.status = '3'
#                     t.buyer = buyer
#                     t.in_order = order
#                     t.save()
#                 response.append(str(order.pk))
#
#                 log_action(request.user, 'stock', 'book_create',
#                            offer=order.offer, order=order)
#         else:
#             response = 'Онлайн бронь в разработке'
#         return Response(','.join(response))
#
#     #подтвердить сделку
#     @list_route(methods=['post'])
#     def confirm_order(self, request):
#         order = Order.objects.get(id=request.data.get('id'))
#
#         if not request.user.can_complete_order(order):
#             return Response(
#                 {
#                     'detail': 'Подтверждение сделки недоступно.'
#                 },
#                 status=status.HTTP_206_PARTIAL_CONTENT
#             )
#         order.status = '1'
#         order.save()
#         order.tickets.all().update(status='2')
#         response = order.pk
#
#         company = request.user.company
#         tariff = company.get_active_tariff()
#         tariff.turnover += order.total_cost()
#         tariff.nominal += order.total_nominal_cost()
#         tariff.save()
#         # TariffLog.objects.create(
#         #     tariff=tariff,
#         #     bill=company.company_bills.filter(status=1).first(),
#         #     turnover=order.total_cost()
#         # )
#         log_action(request.user, 'stock', 'book_confirm',
#                    offer=order.offer, order=order)
#
#         return Response(response)
#
#     #отменить сделку (для продавца)
#     @list_route(methods=['post'])
#     def abort_order(self, request):
#         order = Order.objects.get(id=request.data.get('id'))
#         order.status = '5'
#         order.save()
#         order.tickets.all().update(status='1', buyer=None, in_order=None)
#         log_action(request.user, 'stock', 'book_cancel', offer=order.offer, order=order)
#         response = order.pk
#         return Response(response)
#
#     # отказаться от сделки (для покупателя)
#     @list_route(methods=['post'])
#     def refuse_order(self, request):
#         order = Order.objects.get(id=request.data.get('id'))
#         order.status = '4'
#         order.save()
#         order.tickets.all().update(status='1', buyer=None, in_order=None)
#         log_action(request.user, 'stock', 'book_cancel', offer=order.offer, order=order)
#         response = order.pk
#
#         return Response(response)
#
#     # отказаться от сделки (для покупателя)
#     @list_route(methods=['get'])
#     def get_my_orders(self, request):
#         filters = {request.query_params['type']: request.user, 'status': request.query_params['status']}
#         orders = [i for i in Order.objects.filter(**filters)]
#         response = self.get_serializer(orders, many=True)
#         return Response(response.data)
#
# class TicketViewSet(viewsets.ModelViewSet):
#     queryset = Ticket.objects.all()
#     serializer_class = TicketSerializer
#
# class DefaultPagination(PageNumberPagination):
#     page_size = 3
#     page_size_query_param = 'page_size'
#     max_page_size = 30
#
# class OfferFilter(filters.FilterSet):
#     class Meta:
#         model = Offer
#         fields = ['broker', 'seance', 'seance__event']
#
# class OfferViewSet(viewsets.ModelViewSet):
#     authentication_classes = (SessionAuthentication, TokenAuthentication)
#     queryset = Offer.objects.all().select_related('seance', 'sector', 'broker').prefetch_related('offer_tickets',)
#     serializer_class = OfferSerializer
#     pagination_class = DefaultPagination
#     filter_backends = (filters.DjangoFilterBackend, SearchFilter)
#     filterset_class = OfferFilter
#     search_fields = ['__all__']
#
#
#     def update(self, request, *args, **kwargs):
#         instance = self.get_object()
#         serializer = self.get_serializer(instance=instance, data=request.data)
#         if serializer.is_valid():
#             if not request.user.can_modify_offer(instance):
#                 return Response(
#                     {
#                         'detail': 'Редактирование предложения недоступно.'
#                     },
#                     status=status.HTTP_400_BAD_REQUEST
#                 )
#             serializer.save()
#             return Response(serializer.data)
#
#     def destroy(self, request, *args, **kwargs):
#         instance = self.get_object()
#         if not request.user.can_delete_offer(instance):
#             return Response(
#                 {
#                     'detail': 'Удаление предложения недоступно.'
#                 },
#                 status=status.HTTP_400_BAD_REQUEST
#             )
#         self.perform_destroy(instance)
#         return Response(status=status.HTTP_204_NO_CONTENT)
#
#     #получить список моих предложений
#     @list_route(methods=['post'])
#     def get_my_offers(self, request):
#         offers = [i for i in Offer.objects.filter(broker=request.user, offer_tickets__status__in=['1', '3']).distinct()[:10]]
#         response = self.get_serializer(offers, many=True)
#         return Response(response.data)
#
#     #получить список предложений для сеанса
#     @list_route(methods=['post'])
#     def get_offers_by_seance(self, request):
#         params = dict(
#             offer_tickets__status__in=['1', '3'],
#             seance__api_id=request.data.get('id', 0)
#         )
#         if request.data.get('sort', None) == 'self':
#             params['broker'] = request.user.pk
#
#         offers = [i for i in Offer.objects.filter(**params).distinct()]
#         response = self.get_serializer(offers, many=True)
#         tickets = response.data
#         for i in tickets:
#             i['select_seats'] = []
#             i['seats'].sort(key=lambda x: (int(x['seat'])))
#         return Response(tickets)
#
#     #создать предложение
#     @list_route(methods=['post'])
#     def create_offer(self, request):
#         user = Broker.objects.get(pk=request.user.id)
#
#         tickets_seats = []
#         # преобразоываваем места
#         for val in request.data.get('seats').split(','):
#             item = val.split('-')
#             if len(item) == 2:
#                 start = int(item[0])
#                 end = int(item[1])
#                 for i in range(start, end + 1):
#                     tickets_seats.append(i)
#             else:
#                 tickets_seats.append(item[0])
#
#         tariff = user.company.get_active_tariff()
#
#
#         if tariff:
#             if not (tariff.tickets_sell_limit + len(tickets_seats) < tariff.tariff.tickets_sell_limit):
#                 return Response(
#                     {
#                         'detail': 'Превышено максимальное кол-во продаваемых билетов в тарифе'
#                     },
#                     status=status.HTTP_400_BAD_REQUEST
#                 )
#             if not (tariff.tickets_limit + 1 < tariff.tariff.tickets_limit):
#                 return Response(
#                     {
#                         'detail': 'Превышено максимальное кол-во сделок в тарифе'
#                     },
#                     status=status.HTTP_400_BAD_REQUEST
#                 )
#
#         if not user.can_make_offer():
#             return Response(
#                 {
#                     'detail': 'У вас нет прав.'
#                 },
#                 status=status.HTTP_400_BAD_REQUEST
#             )
#
#         event_place = EventPlace.objects.get(api_id=request.data.get('place', 0))
#         hall = Hall.objects.get(api_id=request.data.get('hall'))
#         hall_configuration = HallConfiguration.objects.get(api_id=request.data.get('hall_configuration'))
#         sector = Sector.objects.get(api_id=request.data.get('sector'))
#         event = SingleEvent.objects.get_or_create(name=request.data.get('seance')['event_name'], event_place=event_place, hall=hall_configuration)[0]
#         seance = Seance.objects.get_or_create(event=event,
#                                               api_id=request.data.get('seance')['id'],
#                                               slug=request.data.get('seance')['slug'],
#                                               starts_at=dateutil.parser.parse(request.data.get('seance')['starts_at'], dayfirst=True),
#                                               hall=hall)[0]
#
#         created_tickets = 0
#         doubles = 0
#         offer = Offer.objects.create(
#             broker=user,
#             row=request.data.get('row'),
#             nominal=request.data.get('nominal'),
#             sellprice=request.data.get('sell_price'),
#             parserprice=request.data.get('public'),
#             sector=sector,
#             seance=seance,
#             breakdown=request.data.get('sell_strategy'),
#             event_place=event_place,
#             hall=hall,
#             no_refund=request.data.get('no_refund'),
#             cashless=request.data.get('cashless'),
#             named=request.data.get('named'),
#             api=request.data.get('api'),
#             electronic=request.data.get('electronic'),
#         )
#
#         for t in tickets_seats:
#             Ticket.objects.create(seat=str(t), broker=user, offer=offer, sellprice=request.data.get('sell_price'))
#             created_tickets += 1
#
#         message = 'Билетов создано: {}'.format(created_tickets)
#
#         if doubles:
#             message = '{}, дублей {}.'.format(message, doubles)
#
#         company = user.company
#         tariff = company.get_active_tariff()
#         # tariff.
#         tariff.tickets_limit += 1
#         tariff.tickets_sell_limit += created_tickets
#         tariff.save()
#
#         # TariffLog.objects.create(
#         #     tariff=tariff,
#         #     bill=company.company_bills.filter(status=1).first(),
#         #     nominal=offer.nominal
#         # )
#         # log_action(user, 'stock', 'ticket_add', offer=offer)
#
#         return Response(message)
#
#     @list_route(methods=['post', 'get'])
#     def api_booking(self, request):
#         buyer = request.user
#         offers = request.data[0]['offers']
#         for item in offers:
#             off = Offer.objects.get(pk=item['order'])
#             ticks = [i for i in off.offer_tickets.filter(pk__in=item['seats'].split(',')).update(status='3')]
#             order = Order.objects.create(seller=off.broker, buyer=request.user, offer=off, comment='API Бронь')
#             order.tickets.add(*ticks)
#             sendSMS(user.phone, "Ваш пароль для входа в Grissly Tickets: {}".format(user.login_code))
#             log_action(request.user, 'stock', 'api_book_create',
#                        offer=order.offer, order=order)
#
#         return Response(request.user.company.name)
