
ANALYTICS_VERSION = "1.0.0"
UTM_SOURCE = 'utm_source'
UTM_MEDIUM = 'utm_medium'
UTM_CAMPAIGN = 'utm_campaign'
UTM_TERM = 'utm_term'
UTM_CONTENT = 'utm_content'
UTM_CAMPAIGN_ID = 'utm_campaign_id'
UTM_GROUP_ID = 'utm_group_id'
UTM_TERM_ID = 'utm_term_id'
REFERER = 'referer'

UTM_CHOICES = (
    (UTM_SOURCE, u'%s | Источник' % UTM_SOURCE),
    (UTM_MEDIUM, u'%s | Тип источника (cpc, баннер, рассылка)' % UTM_MEDIUM),
    (UTM_CAMPAIGN, u'%s | Название рекламной кампании' % UTM_CAMPAIGN),
    (UTM_TERM, u'%s | Kлючевое слово' % UTM_TERM),
    (UTM_CAMPAIGN_ID, u'%s | ID рекламной кампании' % UTM_TERM),
    (UTM_GROUP_ID, u'%s | ID группы объявления' % UTM_TERM),
    (UTM_TERM_ID, u'%s | ID ключевого слова' % UTM_TERM),
    (REFERER, u'%s | Переход с сайта' % REFERER)
)

ANALITYCS_KEY = '_analitycs'