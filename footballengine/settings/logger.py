# import logging
# import sys
# def exception_hook(exc_type, exc_value, exc_traceback):
#     logging.error(
#         "Uncaught exception",
#         exc_info=(exc_type, exc_value, exc_traceback)
#     )
#
# sys.excepthook = exception_hook
#
# #class GriFormatter(logging.Formatter):
# #    pass
#
# #logging._defaultFormatter = GriFormatter # change formatter
# """
# class GriLogRecord(logging.LogRecord):
#
#     def __init__(self, name, level, pathname, lineno, msg, args, exc_info, func=None, sinfo=None, **kwargs):
#         super(GriLogRecord, self).__init__(name, level, pathname, lineno,
#                  msg, args, exc_info, func, sinfo, **kwargs)
#         self.hostname = ""
#
# logging._logRecordFactory = GriLogRecord
# """
#
# class GriLogging(logging.Logger):
#     def _kwargs(self, kwargs):
#         if not kwargs.get('extra'):
#             kwargs['extra'] = {}
#         if not kwargs['extra'].get('hostname'):
#             kwargs['extra']['hostname'] = 'Unset'
#         return kwargs
#
#     def info(self, msg, *args, **kwargs):
#         kwargs = self._kwargs(kwargs)
#         super(GriLogging, self).info(msg, *args, **kwargs)
#
#     def critical(self, msg, *args, **kwargs):
#         kwargs = self._kwargs(kwargs)
#         super(GriLogging, self).critical(msg, *args, **kwargs)
#
#     def error(self, msg, *args, **kwargs):
#         kwargs = self._kwargs(kwargs)
#         super(GriLogging, self).error(msg, *args, **kwargs)
#
#     def warning(self, msg, *args, **kwargs):
#         kwargs = self._kwargs(kwargs)
#         super(GriLogging, self).warning(msg, *args, **kwargs)
#
#     def debug(self, msg, *args, **kwargs):
#         kwargs = self._kwargs(kwargs)
#         super(GriLogging, self).debug(msg, *args, **kwargs)
#
# logging.setLoggerClass(GriLogging)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {'format': "%(asctime)s: %(hostname)s : %(levelname)s : %(name)s:%(lineno)s  - %(message)s"},
    },
    'handlers': {
        'console': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'logstash': {
            'level': 'ERROR',
            'class': 'logstash.TCPLogstashHandler',
            'host': '213.239.216.111',
            'port': 5959,  # Default value: 5959
            'version': 1,
            'message_type': 'django',  # 'type' field in logstash message. Default value: 'logstash'.
            'fqdn': False,  # Fully qualified domain name. Default value: false.
            'tags': ['footballengine', ],  # list of tags. Default: None.
        }
    },
    'loggers': {
        '': {
            'level': 'ERROR',
            'handlers': ['console', 'logstash']
        }
    }
}