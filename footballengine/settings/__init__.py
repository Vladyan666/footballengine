from .settings_global import *
from .logger import *
from .ads_settings import *
try:
    from .settings_local import *
except ImportError as e:
    print(e)