from rest_framework import viewsets, status as HTTPstatus
from rest_framework import serializers
from django.contrib.auth.models import User

from engine.models import *
        
        
class TeamSerializer(serializers.ModelSerializer):
    team_type_display = serializers.CharField(source='get_team_type_display', read_only=True)

    class Meta:
        model = Team
        fields = ('name', 'team_type', 'pk', 'team_type_display')


class ChampSerializer(serializers.ModelSerializer):
    champ_year = serializers.CharField(source='football_years')

    class Meta:
        model = Champ
        fields = ('name', 'pk', 'champ_year')


class StadiumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stadium
        fields = ('name', 'pk')


class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        fields = ('pk', 'command_first', 'command_second', 'champ', 'stadium', 'static_match', 'alias')

class SiteOrderItemSerializer(serializers.ModelSerializer):
    event_name = serializers.SlugRelatedField(read_only=True, source='price.match', slug_field='name')
    stadium = serializers.SlugRelatedField(read_only=True, source='price.match.stadium', slug_field='name')
    city = serializers.SlugRelatedField(read_only=True, source='price.match.stadium.city', slug_field='name')
    price_val = serializers.SlugRelatedField(read_only=True, source='price', slug_field='price')
    sector = serializers.SlugRelatedField(read_only=True, source='price', slug_field='sector')

    class Meta:
        model = SiteOrderItem
        fields = '__all__'

class TicketCarryallSerializer(serializers.ModelSerializer):
    event_name = serializers.SlugRelatedField(read_only=True, source='match', slug_field='name')
    stadium = serializers.SlugRelatedField(read_only=True, source='match.stadium', slug_field='name')
    city = serializers.SlugRelatedField(read_only=True, source='match.stadium.city', slug_field='name')

    class Meta:
        model = TicketCarryall
        fields = '__all__'

class SiteOrderSerializer(serializers.ModelSerializer):
    c_tickets = TicketCarryallSerializer(many=True)
    s_item = SiteOrderItemSerializer(many=True)

    class Meta:
        model = SiteOrder
        fields = '__all__'


# class HallSerializer(serializers.ModelSerializer):
#
#     ep_name = serializers.SlugRelatedField(read_only=True, source='event_place', slug_field='name')
#     ep_api_id = serializers.SlugRelatedField(read_only=True, source='event_place', slug_field='api_id')
#
#     class Meta:
#         model = Hall
#         fields = ('pk', 'name', 'api_id', 'event_place', 'ep_name', 'ep_api_id')
#
#
# class SectorSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Sector
#         fields = ('pk', 'hall', 'name', 'slug', 'api_id')
#
#
# class SingleEventSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = SingleEvent
#         fields = ('pk', 'name', 'slug', 'api_id', 'event_place', 'isactive')
#
#     def create(self, validated_data):
#         obj, created = SingleEvent.objects.get_or_create(name=validated_data.pop('name', None), defaults=validated_data)
#
#         self.is_created = created
#
#         instance = obj
#
#         return instance
#
#
# class SeanceSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Seance
#         fields = ('pk', 'event', 'api_id', 'slug', 'starts_at', 'hall', 'is_active')
#
#     def create(self, validated_data):
#         obj, created = Seance.objects.get_or_create(event=validated_data.pop('event', None), starts_at=validated_data.pop('starts_at', None), defaults=validated_data)
#
#         self.is_created = created
#
#         instance = obj
#
#         return instance
#
#
# class CompanySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Company
#         fields = ('pk', 'name', 'site', 'location')
#
# class BrokerSerializer(serializers.ModelSerializer):
#
#     company_name = serializers.SlugRelatedField(read_only=True, source='company', slug_field='name')
#     company_location = serializers.SlugRelatedField(read_only=True, source='company', slug_field='location')
#
#     class Meta:
#         model = Broker
#         fields = ('pk', 'company', 'username', 'phone', 'email', 'company_name', 'company_location', 'first_name')
#
#
# class OrderSerializer(serializers.ModelSerializer):
#     buyer_name = serializers.SlugRelatedField(read_only=True, source='buyer', slug_field='first_name')
#     buyer_phone = serializers.SlugRelatedField(read_only=True, source='buyer', slug_field='phone')
#     seller_name = serializers.SlugRelatedField(read_only=True, source='seller', slug_field='first_name')
#     seller_phone = serializers.SlugRelatedField(read_only=True, source='seller', slug_field='phone')
#     tickets_l = serializers.SlugRelatedField(read_only=True, source='tickets', slug_field='seat', many=True)
#     row = serializers.SlugRelatedField(read_only=True, source='offer', slug_field='row')
#     sector = serializers.SlugRelatedField(read_only=True, source='offer.sector', slug_field='name')
#     ev_name = serializers.SlugRelatedField(read_only=True, source='offer.seance.event', slug_field='name')
#     starts_at = serializers.SlugRelatedField(read_only=True, source='offer.seance', slug_field='starts_at')
#     ev_place = serializers.SlugRelatedField(read_only=True, source='offer.event_place', slug_field='name')
#
#     class Meta:
#         model = Order
#         fields = ('pk', 'buyer', 'tickets', 'offer', 'status', 'created', 'comment', 'buyer_name', 'seller_name', 'buyer_phone', 'seller_phone', 'tickets_l', 'row', 'sector', 'ev_name', 'starts_at', 'ev_place')
#
#
# class TicketSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Ticket
#         fields = ('pk', 'seat', 'broker', 'buyer', 'status')
#
#
# class OfferSerializer(serializers.ModelSerializer):
#
#     seats = TicketSerializer(many=True, source='offer_tickets', read_only=True)
#     broker_name = serializers.SlugRelatedField(read_only=True, source='broker', slug_field='first_name')
#     sector_name = serializers.SlugRelatedField(read_only=True, source='sector', slug_field='name')
#     sector_slug = serializers.SlugRelatedField(read_only=True, source='sector', slug_field='slug')
#     seance_name = serializers.SlugRelatedField(read_only=True, source='seance.event', slug_field='name')
#     seance_date = serializers.SlugRelatedField(read_only=True, source='seance', slug_field='starts_at')
#     seance_api_id = serializers.SlugRelatedField(read_only=True, source='seance', slug_field='api_id')
#     seance_place = serializers.SlugRelatedField(read_only=True, source='seance.event.event_place', slug_field='name')
#     seance_hall = serializers.SlugRelatedField(read_only=True, source='seance.hall', slug_field='name')
#     seance_hall_id = serializers.SlugRelatedField(read_only=True, source='seance.hall', slug_field='api_id')
#
#     class Meta:
#         model = Offer
#
#         fields = ('pk', 'broker', 'created', 'seats', 'sector', 'seance', 'broker_name', 'breakdown', 'status', 'sector_name', 'row', 'nominal', 'sellprice', 'sector_slug', 'seance_name', 'seance_date', 'seance_place', 'seance_hall_id', 'seance_hall', 'parserprice', 'modified', 'seance_api_id', 'no_refund', 'electronic', 'named', 'cashless', 'api', 'show')
#
#     def update(self, instance, validated_data):
#         tickets = self.context['request'].data.pop('txt_seats').replace(' ', '').split(',')
#         tickets2 = list(instance.offer_tickets.filter(status='1').values_list('seat', flat=True))
#         result = list(set(tickets2) - set(tickets))
#
#         for attr in validated_data:
#             instance.__setattr__(attr, validated_data.get(attr))
#
#         for item in instance.offer_tickets.filter(status='1', seat__in=result):
#             item.delete()
#         instance.save()
#         return instance
