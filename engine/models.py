from __future__ import unicode_literals

from django.conf import settings as django_settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.contrib.sites.models import Site
from ckeditor.fields import RichTextField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.forms import ValidationError
from django.db.models import Q, ObjectDoesNotExist

from .utils import custom_bin_id as CUSTOM_ID_FUNC
from .utils import from_other_to_other, to_rubles

import datetime
import uuid
import pytils
import re

# валюта
CURRENCY = (
    ('rub', 'rur'),
    ('rur', 'rur'),
    ('eur', 'eur'),
)

INDEX_PAGE_TYPE = (
    (1, 'Чемпионат'),
    (2, 'Чемпионат + плей-офф (система KHL)'),
    (3, 'Турнирная таблица (Группы)'),
)

class Currency(models.Model):
    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюты'

    name = models.CharField('Название [RU]', max_length=20, unique=True)
    name_en = models.CharField('Название [EN]', max_length=20, null=True, blank=True)
    slug = models.CharField('Код валюты', max_length=20, unique=True)
    original_rate = models.DecimalField('Оригинальный курс', max_digits=5, decimal_places=2)
    our_rate = models.DecimalField('Курс к рублю', max_digits=5, decimal_places=2, help_text='Наценка +2%')
    lower_sign = models.CharField('Код валюты (lower)', max_length=20, null=True, blank=True, editable=False)
    mark = models.CharField('Значок валюты', max_length=20, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.lower_sign = self.slug.lower()
        super().save(*args, **kwargs)

def default_currency_():
    try:
        return Currency.objects.get(slug='RUR').pk
    except:
        pass

class Page(models.Model):
    class Meta:
        abstract = True

    alias = models.SlugField(max_length=200, verbose_name=u"Url")
    menushow = models.BooleanField(default=True, verbose_name=u"Показывать в меню")
    sitemap = models.BooleanField(default=True, verbose_name=u"Показывать в карте сайта")
    og_title = models.CharField(max_length=200, verbose_name="OG Title", null=True, blank=True)
    og_description = models.TextField(max_length=2000, verbose_name="OG Description", null=True, blank=True)
    og_type = models.CharField(max_length=200, verbose_name="OG Type", null=True, blank=True)
#    og_image = models.CharField(default='/media/uploads/admin/2017/08/08/fifa-2018.png', max_length=500, verbose_name="OG image (1080x1080px)", null=True, blank=True)
    og_type_pb_time = models.DateField(default=datetime.date.today, verbose_name=u"Время публикации")
    og_type_author = models.CharField(max_length=200, verbose_name="OG author", null=True, blank=True)
    seo_h1=models.CharField(max_length=200, verbose_name="H1", null=True, blank=True)
    seo_title=models.CharField(max_length=200, verbose_name="Title", null=True, blank=True)
    seo_description=models.CharField(max_length=500, verbose_name="Description", null=True, blank=True)
    seo_keywords=models.CharField(max_length=500, verbose_name="Keywords", null=True, blank=True)
    menutitle=models.CharField(max_length=200, verbose_name=u"Название в меню", null=True, blank=True)
    content = RichTextField(config_name='default', null=True, blank=True)
    
class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name=models.CharField(max_length=200, verbose_name=u"Название")
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:textpage', args=[self.alias])
    
    def __str__(self):
        return self.name
        
class News(Page):
    class Meta:
        verbose_name = u"Новостная страница"
        verbose_name_plural = u"Новости"
        
    datetime = models.DateTimeField(verbose_name=u"Дата публикации")
    image = models.ImageField(verbose_name=u"Картинка новости", null=True, blank=True)
    name = models.CharField(max_length=200, verbose_name=u"Название")
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    match = models.ForeignKey('Match', on_delete=models.deletion.SET_NULL, related_name='match_news', verbose_name=u"Матч", null=True, blank=True)

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:news_item', args=[self.alias])
    
    def __str__(self):
        return self.name
        
class Fed(Page):
    class Meta:
        verbose_name = u"Федерация"
        verbose_name_plural = u"Федерации"

    name=models.CharField(max_length=200, verbose_name=u"Название")
    acronym = models.CharField(max_length=10, verbose_name=u"Аббревиатура")
    image = models.ImageField(verbose_name=u"Лого", null=True, blank=True)
    
    def __str__(self):
        return self.name
        
        
class Fed_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Федерация"
        verbose_name_plural = u"Уник Федерации"

    static = models.ForeignKey(Fed, verbose_name='Статическая модель', related_name = 'static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")

    def __str__(self):
        return self.static.name + '-' + self.host.name
    
class Country(Page):
    class Meta:
        verbose_name = u"Страна"
        verbose_name_plural = u"Страны"
        
    name = models.CharField(max_length=200, verbose_name=u"Название")
    flag = models.ImageField(verbose_name=u"Флаг", null=True, blank=True)
    tz = models.IntegerField(verbose_name=u"Временная зона", default=3, help_text='UTC + сколько часов')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
    
    def __str__(self):
        return self.name

class Country_Uniq(Page):
    class Meta:
        verbose_name = u"Уник страна"
        verbose_name_plural = u"Уник Страны"
        
    name=models.CharField(max_length=200, verbose_name=u"Название")
    flag = models.ImageField(verbose_name=u"Флаг", null=True, blank=True)
    static = models.ForeignKey(Country, verbose_name='Статическая модель', related_name = 'static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    
    def __str__(self):
        return self.name
    
class City(Page):
    class Meta:
        verbose_name = u"Город"
        verbose_name_plural = u"Города"
        
    name=models.CharField(max_length=200, verbose_name=u"Название")
    image = models.ImageField(verbose_name=u"Фотография", null=True, blank=True)
    menuposition = models.IntegerField(verbose_name=u"Позиция в меню", null=True, blank=True)
    country=models.ForeignKey(Country, verbose_name=u"Страна", null=True, blank=True, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name

class City_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Город"
        verbose_name_plural = u"Уник Города"
        
    name=models.CharField(max_length=200, verbose_name=u"Название")
    image = models.ImageField(verbose_name=u"Фотография", null=True, blank=True)
    menuposition = models.IntegerField(verbose_name=u"Позиция в меню", null=True, blank=True)
    country=models.ForeignKey(Country, verbose_name=u"Страна", null=True, blank=True, on_delete=models.CASCADE)
    static = models.ForeignKey(City, verbose_name='Статическая модель', related_name = 'static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    
    def __str__(self):
        return self.name
    
class Stadium(Page):
    class Meta:
        verbose_name = u"Стадион"
        verbose_name_plural = u"Стадионы"

    name = models.CharField(max_length=200, verbose_name=u"Название")
    photo = models.ImageField(verbose_name=u"Фото Стадиона", null=True, blank=True)
    image = models.FileField(verbose_name=u"Схема", null=True, blank=True)
    svg_scheme = models.CharField(max_length=600, verbose_name=u"Схема SVG", null=True, blank=True)
    big_image = models.FileField(verbose_name=u"Подробная схема", null=True, blank=True)
    traffic = models.IntegerField(verbose_name=u"Вместимость", null=True, blank=True)
    city = models.ForeignKey(City, verbose_name=u"Город", null=True, blank=True, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:arena', args=[self.alias])

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        for i in self.static.all():
            i.alias = self.alias
            i.save()


class Stadium_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Стадион"
        verbose_name_plural = u"Уник Стадионы"

    static = models.ForeignKey(Stadium, verbose_name='Статическая модель', related_name='static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:arena', args=[self.alias])
    
    def __str__(self):
        return self.static.name

    def save(self, *args, **kwargs):
        if self.content:
            self.alias = self.static.alias
            super().save(*args, **kwargs)
        else:
            pass

TEAM_TYPE = (
    (1, 'Футбол'),
    (2, 'Хоккей'),
    (3, 'Баскетбол'),
    (4, 'Регби'),
    (5, 'UFC'),
    (6, 'Теннис'),
    (7, 'Волейбол'),
    (8, 'Бокс')
)

class Team(Page):
    class Meta:
        verbose_name = u"Команда"
        verbose_name_plural = u"Команды"

    team_type = models.PositiveSmallIntegerField(verbose_name='Тип команды', default=1, choices=TEAM_TYPE)
    logo2 = models.ImageField(verbose_name=u"Логотип", null=True, blank=True)
    children_age = models.IntegerField(verbose_name=u"Для детей от", null=True, blank=True)
    eng_name = models.CharField(max_length=200, verbose_name=u"Название на английском", null=True, blank=True)
    code = models.CharField(max_length=5, verbose_name=u"Код страны", null=True, blank=True)
    fed = models.ForeignKey(Fed, verbose_name=u"Федерация", null=True, blank=True, on_delete=models.CASCADE)
    stadium = models.ForeignKey(Stadium, verbose_name=u"Домашний стадион", null=True, blank=True, on_delete=models.CASCADE)
    offsite = models.CharField(max_length=200, verbose_name=u"Официальный сайт", null=True, blank=True)


    logo = models.CharField(max_length=3000, verbose_name=u"Логотип", null=True, blank=True)
    flag = models.CharField(max_length=3000, verbose_name=u"Флаг", null=True, blank=True)
    photo = models.CharField(max_length=3000, verbose_name=u"Фотография команды", null=True, blank=True)
    forma = models.CharField(max_length=3000, verbose_name=u"Фотография формы", null=True, blank=True)

    widget = models.TextField(verbose_name=u"Виджет", null=True, blank=True)
    email = models.CharField(max_length=200, verbose_name=u"Email", null=True, blank=True)
    phone = models.CharField(max_length=200, verbose_name=u"Телефон", null=True, blank=True)
    fax = models.CharField(max_length=200, verbose_name=u"Fax", null=True, blank=True)
    fansite = models.CharField(max_length=2000, verbose_name=u"Неофициальные сайты", null=True, blank=True)
    founded = models.IntegerField(verbose_name=u"Год основания", null=True, blank=True)

    techteam = models.BooleanField(default=False, verbose_name=u"Техническая команда")
    
    name=models.CharField(max_length=200, verbose_name=u"Название")
    alt_name = models.CharField(max_length=200, verbose_name=u"Альтернативное название", null=True, blank=True)
    short_description=models.TextField(verbose_name=u"Краткое описание", null=True, blank=True)
    current_status=models.CharField(max_length=200, verbose_name=u"Текущий статус", null=True, blank=True)
    address=models.CharField(max_length=200, verbose_name=u"Адрес", null=True, blank=True)
    country_fed=models.CharField(max_length=200, verbose_name=u"Наименование федерации", null=True, blank=True)

    _curr_name = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.alt_name:
            self._curr_name = self.name
            self.name = self.alt_name

    def default_name(self):
        if self._curr_name:
            return self._curr_name
        else:
            return self.name

    def get_club_name(self):
        return {
            1: 'ФК',
            2: 'ХК',
            3: 'БК',
            4: 'РК',
            5: 'Команда UFC',
            6: 'Теннисная команда'
        }.get(self.team_type, 'Команда')


    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:team', args=[self.alias])

    def __str__(self):
        return '{} - {}'.format(self.name, self.get_team_type_display())

    def save(self, *args, **kwargs):
        if self.pk:
            if self._curr_name:
                self.name = self._curr_name

        if not self.alias:
            slug = pytils.translit.translify(self.name).lower()
            slug = re.sub(' ', '-', slug)
            slug = re.sub("[\.|\#|\(|\)|\']", '', slug)
            self.alias = slug
        super().save(*args, **kwargs)
        for i in self.static.all():
            i.alias = self.alias
            i.save()

class Team_Uniq(Page):
    class Meta:
        verbose_name = u"Уник команда"
        verbose_name_plural = u"Уник команды"

    static = models.ForeignKey(Team, verbose_name='Статическая модель', related_name='static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:team', args=[self.alias])
    
    def __str__(self):
        return self.static.name

    def save(self, *args, **kwargs):
        if self.content:
            self.alias = self.static.alias
            super().save(*args, **kwargs)
        else:
            pass
        
class Champ(Page):
    class Meta:
        verbose_name = u"Чемпионат"
        verbose_name_plural = u"Чемпионаты"

    name=models.CharField(max_length=200, verbose_name=u"Название")
    fed = models.ForeignKey(Fed, verbose_name=u"Федерация чемпионата", null=True, blank=True, on_delete=models.CASCADE)
    image = models.ImageField(verbose_name=u"Лого", null=True, blank=True)
    year = models.IntegerField(verbose_name=u"Год проведения", null=True, blank=True)
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в чемпионате", blank=True)
    priority = models.IntegerField(verbose_name=u"Приоритет", default=0)
    active = models.BooleanField(default=False, verbose_name=u"Активный чемпионат")

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:champ', args=[self.alias])
        
    def football_years(self):
        item = str(self.year) + ' / ' + str(self.year+1)
        return item

    def __str__(self):
        return self.name + ' - ' + str(self.football_years())

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        for i in self.static.all():
            i.alias = self.alias
            i.save()
    

class Champ_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Чемпионат"
        verbose_name_plural = u"Уник Чемпионаты"

    static = models.ForeignKey(Champ, verbose_name='Статическая модель', related_name = 'static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    alias = models.SlugField(max_length=200, verbose_name=u"Url", null=True, blank=True)
        
    def __str__(self):
        return self.static.name

    def save(self, *args, **kwargs):
        if self.content:
            self.alias = self.static.alias
            super().save(*args, **kwargs)
        else:
            pass

class Group(Page):
    class Meta:
        verbose_name = u"Группа"
        verbose_name_plural = u"Группы"

    name = models.CharField(max_length=200, verbose_name=u"Название")
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат", related_name="champ_groups", null=True, blank=True, on_delete=models.CASCADE)
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в группе", blank=True)
    scoretable = models.TextField(verbose_name=u"Ручная таблица группы", null=True, blank=True)
    qualifying = models.BooleanField(default=False, verbose_name=u"Отброчная группа")
    menuposition = models.IntegerField( verbose_name=u"Позиция вывода" , null=True , blank=True )
    image = models.ImageField(verbose_name=u"Фотография", null=True, blank=True)
    
    def __str__(self):
        return self.name

class Group_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Группа"
        verbose_name_plural = u"Уник Группы"

    name=models.CharField(max_length=200, verbose_name=u"Название")
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат", null=True, blank=True, on_delete=models.CASCADE)
    teams = models.ManyToManyField(Team, verbose_name=u"Команды в группе", blank=True)
    scoretable = models.TextField(verbose_name=u"Ручная таблица группы", null=True, blank=True)
    qualifying = models.BooleanField(default=False, verbose_name=u"Отброчная группа")
    menuposition = models.IntegerField( verbose_name=u"Позиция вывода" , null=True , blank=True )
    static = models.ForeignKey(Group, verbose_name='Статическая модель', related_name = 'static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    
    def __str__(self):
        return self.name
        
class Player(Page):
    class Meta:
        verbose_name = u"Игрок"
        verbose_name_plural = u"Игроки"

    name=models.CharField(max_length=200, verbose_name=u"Название")
    eng_name = models.CharField(max_length=200, verbose_name=u"Имя на английском")
    number = models.CharField(max_length=100, verbose_name=u"Номер в команде", null=True, blank=True)
    birth = models.DateField(verbose_name=u"День рождения", null=True, blank=True)
    team = models.ForeignKey(Team, verbose_name=u"Сборная", null=True, blank=True, on_delete=models.CASCADE)
    height = models.CharField(max_length=100, verbose_name=u"Рост", null=True, blank=True)
    weight = models.CharField(max_length=100, verbose_name=u"Вес", null=True, blank=True)
    image = models.ImageField(verbose_name=u"Фотография", null=True, blank=True)
    position=models.CharField(max_length=100, verbose_name=u"Позиция", null=True, blank=True)
    nationality=models.ManyToManyField(Country,verbose_name=u"Национальность", blank=True)
    
    def __str__(self):
        return self.name

class Player_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Игрок"
        verbose_name_plural = u"Уник Игроки"

    name=models.CharField(max_length=200, verbose_name=u"Название")
    eng_name = models.CharField(max_length=200, verbose_name=u"Имя на английском")
    number = models.CharField(max_length=100, verbose_name=u"Номер в команде", null=True, blank=True)
    birth = models.DateField(verbose_name=u"День рождения", null=True, blank=True)
    team = models.ForeignKey(Team, verbose_name=u"Сборная", null=True, blank=True, on_delete=models.CASCADE)
    height = models.CharField(max_length=100, verbose_name=u"Рост", null=True, blank=True)
    weight = models.CharField(max_length=100, verbose_name=u"Вес", null=True, blank=True)
    image = models.ImageField(verbose_name=u"Фотография", null=True, blank=True)
    position = models.CharField(max_length=100, verbose_name=u"Позиция", null=True, blank=True)
    nationality = models.ManyToManyField(Country,verbose_name=u"Национальность", blank=True)
    static = models.ForeignKey(Player, verbose_name='Статическая модель', related_name = 'static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    
    def __str__(self):
        return self.name
        
class Staticmatch(Page):
    class Meta:
        verbose_name = u"Статический Матч"
        verbose_name_plural = u"Статические Матчи"
        
    command_first = models.ForeignKey(Team, verbose_name=u"Первая команда", related_name="static_command_first", on_delete=models.CASCADE)
    command_second = models.ForeignKey(Team, verbose_name=u"Вторая команда", related_name="static_command_second", on_delete=models.CASCADE)

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:match_preview', args=[self.alias])

    def __str__(self):
        return self.command_first.name + ' - ' + self.command_second.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        for i in self.static.all():
            i.alias = self.alias
            i.save()

class Staticmatch_Uniq(Page):
    class Meta:
        verbose_name = u"Уник статический Матч"
        verbose_name_plural = u"Уник статические Матчи"

    static = models.ForeignKey(Staticmatch, verbose_name='Статическая модель', related_name='static', on_delete=models.CASCADE)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:match_preview', args=[self.alias])

    def __str__(self):
        return self.static.command_first.name + ' - ' + self.static.command_second.name

    def save(self, *args, **kwargs):
        if self.content:
            self.alias = self.static.alias
            super().save(*args, **kwargs)
        else:
            pass
        
CHOCE_1 = '1'
CHOCE_2 = '2'
CHOCE_3 = '3'
CHOCE_4 = '4'

CHOICE_MATCH_STATUS=(
	(CHOCE_1, 'Матч не начался'),
	(CHOCE_2, 'Первый тайм'),
	(CHOCE_3, 'Второй тайм'),
	(CHOCE_4, 'Матч окончен'),
)

class Match(Page):
    class Meta:
        verbose_name = u"Матч"
        verbose_name_plural = u"Матчи"

    name = models.CharField(max_length=200, verbose_name=u"Название")
    datetime = models.DateTimeField(verbose_name=u"Время начала матча")
    datetime_tz = models.DateTimeField(verbose_name=u"Время начала матча (по локальному времени страны)", null=True, blank=True)
    static_match = models.ForeignKey(Staticmatch, verbose_name='Статический матч', related_name="static_matches", on_delete=models.CASCADE)
    score = JSONField(verbose_name=u"Счет", null=True, blank=True)
    champ = models.ForeignKey(Champ, verbose_name=u"Чемпионат", related_name="champ_matches", null=True, blank=True, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, verbose_name=u"Группа", related_name="group_matches", null=True, blank=True, on_delete=models.CASCADE)
    stadium = models.ForeignKey(Stadium, verbose_name=u"Стадион", null=True, blank=True, on_delete=models.CASCADE)
    command_first = models.ForeignKey(Team, verbose_name=u"Первая команда", related_name="command_first", on_delete=models.CASCADE)
    command_second = models.ForeignKey(Team, verbose_name=u"Вторая команда", related_name="command_second", on_delete=models.CASCADE)
    count_number = models.CharField(max_length=250, verbose_name='Порядковый номер', blank=True)
    goals = JSONField(verbose_name=u"Голы", null=True, blank=True)
    foals = JSONField(verbose_name=u"Нарушения", null=True, blank=True)
    people = models.IntegerField(verbose_name=u"Посещаемость", null=True, blank=True)
    penalty = JSONField(verbose_name=u"Наказания", null=True, blank=True)
    osnova1 = JSONField(verbose_name=u"Основной состав 1 команды", null=True, blank=True)
    osnova2 = JSONField(verbose_name=u"Основной состав 2 команды", null=True, blank=True)
    zamena1 = JSONField(verbose_name=u"Запасной состав 1 команды", null=True, blank=True)
    zamena2 = JSONField(verbose_name=u"Запасной состав 2 команды", null=True, blank=True)
    trener1 = JSONField(verbose_name=u"Тренер 1 команды", null=True, blank=True)
    trener2 = JSONField(verbose_name=u"Тренер 2 команды", null=True, blank=True)
    referee = JSONField(verbose_name=u"Главный судья", null=True, blank=True)
    assistent1 = JSONField(verbose_name=u"1 Боковой судья", null=True, blank=True)
    assistent2 = JSONField(verbose_name=u"2 Боковой судья", null=True, blank=True)
    reserve = JSONField(verbose_name=u"Резрвный судья", null=True, blank=True)
    text = JSONField(verbose_name=u"Текстовая трансляция", null=True, blank=True)
    statistic = JSONField(verbose_name=u'Статистика матча', null=True, blank=True)
    techmatch = models.BooleanField(default=False, verbose_name=u"Технический матч")
    popular = models.BooleanField(default=False, verbose_name=u"Популярный матч")
    beautiful = models.BooleanField(default=False, verbose_name=u"Красивое оформление", help_text='Ну по крайней мере я так считаю...')
    hide_match = models.BooleanField(default=False, verbose_name=u"Скрыть матч в списке вывода")
    tour = models.CharField(max_length=100, verbose_name=u"Тур", null=True, blank=True)
    match_status = models.CharField(max_length=100, verbose_name=u"Статус матча", choices=CHOICE_MATCH_STATUS, default=CHOCE_1, null=True, blank=True)
    carryall_id = models.CharField(max_length=100, verbose_name=u"Carryall ID мероприятия", null=True, blank=True)
    parse_url = models.CharField(max_length=500, verbose_name=u"Url для парса", null=True, blank=True)
    match_markups = JSONField(verbose_name=u"Наценки", default=list, null=True, blank=True)
    stop_online_sell = models.IntegerField(verbose_name=u"Убрать онлайн оплаты", help_text='Кол-во минут до начала мероприятия', default=120, null=True, blank=True)
    no_sellout = models.BooleanField(default=False, verbose_name='Убрать Акции', help_text='Убирается подсветка билетов бинтранета')
    only_list = models.BooleanField(default=False, verbose_name='Билеты только списком')
    default_scheme_colors = models.BooleanField(default=False, verbose_name='Не использовать стандартные цвета схемы', help_text='Если вы сами покрасили схему, поставьте галку')
    fake_match = models.BooleanField(default=False, verbose_name='Угадайка')
    special_seats = JSONField(verbose_name=u"Особые места", null=True, blank=True, help_text='[{"sector": "514", "row": "3", "seats": "1,2,3,4-15"}]')

    _curr_alias = None

    def get_relative_url(self):
        return reverse('main:match', args=[self.static_match.alias, self.alias])

    def get_absolute_url(self):
        return ('https://%s' % Site.objects.get_current().domain) + reverse('main:match', args=[self.static_match.alias, self.alias])

    def __str__(self):
        return self.name

    def match_is_past(self):
        if self.datetime:
            if self.datetime.replace(tzinfo=None) > datetime.datetime.now():
                return False
            else:
                return True
        else:
            return False

    def svg_content(self):
        if self.stadium.image:
            return Stadium.objects.get(pk=self.stadium.pk).image.read()


    def news_ready(self):
        return self.match_news.all().count()

    def news_potential(self):
        return SiteSettings.objects.filter(Q(main_team__in=[self.command_first, self.command_second])|Q(main_champ__teams__in=[self.command_first, self.command_second])).distinct().count()

    def news_for_match(self):
        news_ready = self.match_news.all()
        news_potential = SiteSettings.objects.filter(Q(main_team__in=[self.command_first, self.command_second])|Q(main_champ__teams__in=[self.command_first, self.command_second])).distinct()
        items = []
        for i in news_potential:
            if news_ready.filter(host=i.host).count():
                items.append([i.host, news_ready.get(host=i.host)])
            else:
                items.append([i.host, None])

        return items

    def get_next_match(self):
        if SiteSettings.objects.filter(main_team=self.command_first).count():
            return self.get_next_by_datetime(command_first=self.command_first)
        elif SiteSettings.objects.filter(main_team=self.command_second).count():
            return self.get_next_by_datetime(command_second=self.command_second)
        else:
            return None

    def get_next_match_for_team(self, team):
        print("test")
        try:
            #TODO rework to get_next_by, currently get_next_by doesn't support complex queries, such as ours where we need to check if only 1 from 2 fields equals to the same value
            next_match = Match.objects.filter((Q(command_first=team) | Q(command_second=team)) & Q(datetime__gt=self.datetime)).order_by("datetime").first()
            return next_match
        except ObjectDoesNotExist:
            return None


    def get_winner(self):
        pass

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._curr_alias = self.alias

    def save(self, *args, **kwargs):
        if self.pk:
            if self._curr_alias != self.alias:
                from django.contrib.redirects.models import Redirect
                sites = SiteSettings.objects.filter(Q(main_team=self.command_first)|Q(main_team=self.command_second)|Q(main_champ__teams__in=[self.command_first, self.command_second])).distinct()
                redirs = []
                for s in sites:
                    redirs.append(Redirect(site=s.host, site_id=s.host.pk,
                             old_path='/matches/{}/{}/'.format(self.static_match.alias, self._curr_alias),
                             new_path='/matches/{}/{}/'.format(self.static_match.alias, self.alias)))
                Redirect.objects.bulk_create(redirs)

        try:
            if self.stadium.city.country:
                self.datetime_tz = self.datetime + datetime.timedelta(hours=self.stadium.city.country.tz - 3)
        except:
            pass
        super().save(*args, **kwargs)
        for i in self.static.all():
            i.alias = self.alias
            i.save()

        
class Match_Uniq(Page):
    class Meta:
        verbose_name = u"Уник Матч"
        verbose_name_plural = u"Уник Матчи"

    static = models.ForeignKey(Match, verbose_name='Статическая модель', related_name='static', on_delete=models.CASCADE, default=None)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    alias = models.SlugField(max_length=200, verbose_name=u"Url", null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.content:
            self.alias = self.static.alias
            super().save(*args, **kwargs)
        else:
            pass

class Price(models.Model):
    class Meta:
        verbose_name = u"Билет к матчу"
        verbose_name_plural = u"Билеты к матчу"

    match = models.ForeignKey(Match, verbose_name=u"Матч", related_name="static_tickets", on_delete=models.CASCADE)
    sector = models.CharField(max_length=80, verbose_name=u"Сектор билета", null=True, blank=True)
    count = models.IntegerField(verbose_name=u"Кол-во", null=True, blank=True, default=10)
    color = models.CharField(max_length=80, verbose_name=u"Цвет сектора", help_text='Пример: rgb(0-255) - (255,255,255), или hex - #000', null=True, blank=True)
    image = models.ImageField(verbose_name=u"Картинка", null=True, blank=True)
    vip = models.BooleanField(default=False, verbose_name=u"VIP")
    menuposition = models.IntegerField(verbose_name=u"Порядок", null=True, blank=True)
    price = models.FloatField(verbose_name=u"Цена", null=True, blank=True)
    description = models.TextField(verbose_name=u"Описание", null=True, blank=True)
    light_sectors = models.CharField(max_length=80, verbose_name=u"Подсвечивать сектора", null=True, blank=True, help_text='b110-b120,b122,b125-b130(кароч как на зрителях)')
    currency = models.ForeignKey(Currency, verbose_name=u"Валюта", on_delete=models.SET_DEFAULT, default=default_currency_())

    def ruble_price(self):
        if self.currency.lower_sign == 'rur':
            return self.price
        else:
            return int(self.price) * self.currency.our_rate

    def light_s(self):
        tickets_seats = []
        # преобразоываваем места
        if self.light_sectors:
            for val in self.light_sectors.split(','):
                item = val.split('-')
                if len(item) == 2:
                    symbol = val[0]
                    start = int(''.join(filter(str.isdigit, item[0])))
                    end = int(''.join(filter(str.isdigit, item[1])))
                    for i in range(start, end + 1):
                        tickets_seats.append('{}{}'.format(symbol, i))
                else:
                    tickets_seats.append(item[0])

        return tickets_seats

    def __str__(self):
        return self.sector

class SiteOrderItem(models.Model):
    class Meta:
        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'

    price = models.ForeignKey(Price, verbose_name='Тип цены', related_name="site_order_i", on_delete=models.CASCADE)
    order = models.ForeignKey('SiteOrder', verbose_name='Заказ', related_name="s_item", on_delete=models.CASCADE)
    count = models.IntegerField(verbose_name=u"Количество", default=0, null=True, blank=True)
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True, editable=False)
    updated = models.DateTimeField(verbose_name='Обновлен', auto_now=True, editable=False)

    def get_total(self):
        total = int(self.price.price) * int(self.count)

        return total

    def get_tick_num(self):
        total = int(self.count)
        return total

    def curr_price(self):
        if self.order.user_currency.lower_sign == 'rur':
            if self.price.currency.lower_sign == 'rur':
                return round(self.price.price)
            else:
                return to_rubles(int(self.price.price), self.price.currency.our_rate)
        else:
            return from_other_to_other(self.price.price, self.price.currency.our_rate, self.order.user_currency.our_rate)

    @property
    def match(self):
        return self.price.match


class TicketCarryall(models.Model):
    class Meta:
        verbose_name = u"Билет с керриола"
        verbose_name_plural = u"Билеты с керриола"

    match = models.ForeignKey(Match, verbose_name=u"Матч", null=True, blank=True, on_delete=models.CASCADE)
    order = models.ForeignKey('SiteOrder', verbose_name=u"Заказ", related_name="c_tickets", null=True, blank=True,
                              on_delete=models.CASCADE)
    hash = models.CharField(max_length=100, verbose_name=u"hash - Билета")
    sector = models.CharField(max_length=40, verbose_name=u"Сектор билета", null=True, blank=True)
    row = models.CharField(max_length=40, verbose_name=u"Ряд билета", null=True, blank=True)
    seat = models.CharField(max_length=40, verbose_name=u"Место билета", null=True, blank=True)
    currency = models.CharField(max_length=40, verbose_name=u"Валюта", null=True, blank=True)
    source = models.CharField(max_length=40, verbose_name=u"Источник", null=True, blank=True)
    price = models.FloatField(verbose_name=u"Цена", null=True, blank=True)
    ruble_price = models.FloatField(verbose_name=u"Цена в рублях", null=True, blank=True)
    reserved = models.BooleanField(default=True, verbose_name=u"Забронирован")
    count = models.IntegerField(verbose_name='Кол-во', default=1)

    def __str__(self):
        return self.hash

ORDER_CATEGORY = (
    (1, 'Заказ'),
    (2, 'Обратный звонок'),
    (3, 'Корп. заявка'),
    (4, 'Сертификат')
)

class SiteOrder(models.Model):
    class Meta:
        verbose_name = 'Корзина клиента'
        verbose_name_plural = 'Корзины клиентов'

    token = models.UUIDField(default=uuid.uuid4, editable=False)
    phone = models.CharField(max_length=40, verbose_name='Телефон для связи')
    name = models.CharField(max_length=40, verbose_name='Имя')
    email = models.CharField(max_length=40, verbose_name='E-mail')
    comment = models.TextField(verbose_name=u"HTML - контент в баннере", null=True, blank=True)
    bin_id = models.IntegerField(verbose_name='Id бинтранета', default=0)
    paymethod = models.IntegerField(verbose_name='Метод оплаты', default=0)
    utm = JSONField(verbose_name=u"UTM метки", default=list, null=True, blank=True)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    order_type = models.PositiveSmallIntegerField(verbose_name='Тип заказа', default=1, choices=ORDER_CATEGORY)
    custom_bin_id = models.ForeignKey('FinallyOrder', verbose_name='Номер заказа', related_name="custom_bin_id", on_delete=models.CASCADE, null=True, blank=True)
    user_currency = models.ForeignKey('Currency', verbose_name='Предпочитаемая валюта', on_delete=models.SET_DEFAULT, default=default_currency_())

    @classmethod
    def set_session(cls, request):
        # Инициализация корзины пользователя
        cart = cls.objects.filter(token=request.session.get(django_settings.CART_SESSION_ID), bin_id=0).first()
        if not cart:
            # Сохраняем корзину пользователя в сессию
            this_site = Site.objects.get_current()
            cart = cls.objects.create(token=str(uuid.uuid4()), bin_id=0, host=this_site, user_currency=SiteSettings.objects.get(host=this_site).default_currency)
            request.session[django_settings.CART_SESSION_ID] = cart.token
        return cart

    get_session = set_session

    def custom_id_str(self):
        return CUSTOM_ID_FUNC(self.custom_bin_id.id)

    def total_count(self):
        tot = 0

        tot += self.c_tickets.all().count()

        for i in self.s_item.all():
            tot += i.count if i.count else 1
        return tot

    def total_sum(self):
        tot = 0
        for i in self.c_tickets.all():
            tot += (i.price * round(i.count))
        for i in self.s_item.all():
            tot += (i.count * round(i.curr_price()) if i.count else round(i.curr_price()))
        return tot

    def __str__(self):
        return self.name


class FinallyOrder(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    hash = models.CharField(max_length=200, verbose_name=u"хеш", null=True)

    def __str__(self):
        return str(self.id)


class SiteSettings(models.Model):
    class Meta:
        verbose_name = 'Настройки сайта'
        verbose_name_plural = 'Настройки сайтов'

    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    main_team = models.ForeignKey(Team, verbose_name=u"Сайт для команды", on_delete=models.CASCADE, null=True, blank=True)
    main_champ = models.ManyToManyField(Champ, verbose_name=u"Сайт для чемпионата", help_text='Если заполнен, команда не учитывается', null=True, blank=True)
    site_stadiums = models.ManyToManyField(Stadium, verbose_name=u"Стадионы чемпионата", help_text='Если заполнено, командные стадионы не учитываются', blank=True)
    index_type = models.PositiveSmallIntegerField(verbose_name='Тип главной', default=1, choices=INDEX_PAGE_TYPE)
    sport_type = models.PositiveSmallIntegerField(verbose_name='Тип спорта', default=1, choices=TEAM_TYPE)
    default_currency = models.ForeignKey('Currency', verbose_name=u"Валюта по умолчанию", on_delete=models.SET_NULL, default=default_currency_(), null=True, blank=True)
    champ_table_id = models.IntegerField(verbose_name=u"ID чемпионата", null=True, blank=True, help_text='Если нужно выводить таблицы типа KHL')
    site_index_hml = models.TextField(verbose_name=u"Доп контент на главной странице", help_text='Инфографика, статистика и т.д.', null=True, blank=True)
    site_word = models.CharField(max_length=100, verbose_name='Название команды или чемпионата', help_text='Например ЦСКА', null=True, blank=True)
    banner_logo = models.FileField(verbose_name=u"Баннер Лого", help_text='По середине экрана на главной',  null=True, blank=True)
    banner_main = models.FileField(verbose_name=u"Баннер главная", help_text='Картинка для мобилки или ПК (если нет видео)', null=True, blank=True)
    banner_matches = models.FileField(verbose_name=u"Баннер для всех страниц кроме главной", help_text='Картинка для матчей в шапке, если её нету то берем баннер', null=True, blank=True)
    loader = models.FileField(verbose_name=u"Gif загрузки", help_text='Gif загрузки', null=True, blank=True)
    banner_html = models.TextField(verbose_name=u"HTML - контент в баннере", null=True, blank=True)
    video_main = models.CharField(max_length=100, verbose_name='Видео для главной', help_text='Только название видео, добавленное в /static/viedo/', null=True, blank=True)
    support_your_team = models.FileField(verbose_name=u"Поддержи свою команду", help_text='Блок с представителем команды в баннере', null=True, blank=True)
    support_your_team_text = models.TextField(verbose_name=u"Поддержи свою команду текст", null=True, blank=True)
    main_banner_text = models.CharField(max_length=100, verbose_name='Текст в баннере', help_text='Первая строка (побольше)', null=True, blank=True)
    main_banner_text_two = models.CharField(max_length=100, verbose_name='Текст в баннере 2', help_text='Вторая строка (поменьше)', null=True, blank=True)
    font = models.CharField(max_length=100, verbose_name='Шрифт', null=True, blank=True)
    site_logo = models.FileField(verbose_name=u"Лого", null=True, blank=True)
    news_logo = models.FileField(verbose_name=u"Новостная картинка - заглушка", null=True, blank=True)
    site_logo_text = models.CharField(max_length=40, verbose_name='Текст логотипа', null=True, blank=True)
    favicon = models.FileField(verbose_name=u"Фавикон", null=True, blank=True)
    metrics_yandex = models.CharField(max_length=100, verbose_name='Счетчик Яндекс', null=True, blank=True)
    metrics_google = models.CharField(max_length=100, verbose_name='Счетчик Google', null=True, blank=True)
    google_optimize = models.CharField(max_length=100, verbose_name='Счетчик Google optimize', null=True, blank=True)
    mango_phone = models.CharField(max_length=100, verbose_name='Счетчик Mango (манго)', null=True, blank=True)
    phone = models.CharField(max_length=100, verbose_name='Телефон (несколько через запятую без пробелов)', null=True, blank=True)
    robots = models.TextField( verbose_name=u"Robots.txt", null=True, blank=True, default="User-agent: *\nDisallow: /" )
    success = models.FileField(verbose_name=u"Картинка успешного заказа", help_text='Картинка успешного заказа (конверт cskatickets по умолчанию)', null=True, blank=True)
    markups = JSONField(verbose_name=u"Наценки", default=[[0.0, 1000.0], [3000.0, 1500.0], [4500.0, 2500.0], [6000.0, 3000.0], [10000.0, 4500.0], [16000.0, 5500.0], [18000.0, 7000.0], [30000.0, 9000.0], [35000.0, 10000.0], [40000.0, 13000.0], [50000.0, 15000.0], [60000.0, 18000.0], [70000.0, 22000.0], [90000.0, 25000.0], [100000.0, 30000.0], [130000.0, 35000.0], [300000.0, 39000.0]], null=True, blank=True)
    header_extends = models.TextField(verbose_name=u"Дополнения в Header сайта", help_text='Стили, Мета теги и т.д.', null=True, blank=True)
    sendsay_basket_json = JSONField("Настройки забытых корзины для Sendsay", default={}, null=True, blank=True, help_text='Пример: {"s_key":"...","v_key":"...","fid":"..."}')
    sendsay_webpush_json = JSONField("Настройки Webpush для Sendsay", default={}, null=True, blank=True, help_text='Пример: {"key":"...","fid":"...","list":"..."}')
    jivosite = models.CharField(max_length=100, verbose_name='Счетчик JivoSite', null=True, blank=True)
    canvas = models.BooleanField(default=False, verbose_name=u"Canvas схема")
    legend = models.BooleanField(default=False, verbose_name=u"Фильтр цен у секторов")


    def __str__(self):
        return self.host.name

    def save(self, *args, **kwargs):
        if self.pk is None:
            if SiteSettings.objects.filter(host=self.host).exists():
                raise ValidationError('Настройки с таким доменом уже существуют')
            else:
                for s in ['index', 'matches', 'arenas', 'checkout', 'news', 'teams', 'champs', 'contacts', 'oferta', 'aboutus']:
                    TextPage.objects.create(host=self.host, alias=s, name=s)

                for s in range(1, 5):
                    SiteSeoHeaders.objects.get_or_create(host=self.host, header_type=s)
        super().save(*args, **kwargs)


class Certificate(models.Model):
    class Meta:
        verbose_name = 'Сертификат'
        verbose_name_plural = 'Сертификаты'

    name = models.CharField(max_length=100, verbose_name='Название')
    image = models.FileField(verbose_name=u"Изображение")
    active = models.BooleanField(default=True, verbose_name=u"Активность")
    sort = models.IntegerField(verbose_name=u"Порядок вывода", default=0)
    buy_count = models.IntegerField(verbose_name=u"Оформили раз", default=0)

    def __str__(self):
        return self.name


HEADERS_TYPES = (
    (1, 'Title'),
    (2, 'Description'),
    (3, 'Keywords'),
    (4, 'H1'),
)

class SiteSeoHeaders(models.Model):
    class Meta:
        verbose_name = 'SEO заголовки сайта'
        verbose_name_plural = 'SEO заголовки сайтов'

    match = models.TextField(verbose_name=u"Шаблон заголовка для матча", null=True, blank=True, default=_("{{ data.command_first.name }} - {{ data.command_second.name }} - {{ data.datetime }} в городе {{ data.stadium.city.name }} - купить билеты на {{ data.command_first.get_team_type_display }} | {{ domain }}"))
    static = models.TextField(verbose_name=u"Шаблон заголовка для првью матча", null=True, blank=True, default=_("Билеты на матчи {{ data.command_first.name }} - {{ data.command_second.name }}, {{ data.command_first.get_team_type_display }} | {{ domain }}"))
    stadium = models.TextField(verbose_name=u"Шаблон заголовка для стадиона", null=True, blank=True, default=_("Купить билеты на стадионе {{ data.name }} | {{ domain }}"))
    team = models.TextField(verbose_name=u"Шаблон заголовка для команды", null=True, blank=True, default=_("Билеты на матчи команды {{ data.name }} | {{ domain }}"))
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")
    header_type = models.PositiveSmallIntegerField(verbose_name='Тип заголовков', default=1, choices=HEADERS_TYPES)

class StaticText(models.Model):
    class Meta:
        verbose_name = 'Шаблонные тексты для матча'
        verbose_name_plural = 'Шаблонные тексты для матчей'

    match_text = models.TextField(verbose_name=u'Шаблон текста для матча', null=True, blank=True, default='')
    match_preview_text = models.TextField(verbose_name=u'Шаблон текста для превью матча', null=True, blank=True, default='')
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=1, verbose_name=u"Хост")

    def __str__(self):
        return str(self.id)