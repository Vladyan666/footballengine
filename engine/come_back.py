import os
import sys
import re, requests, json, time
import django
import datetime
import pytz
import pytils
from django.db.models import Q
from bs4 import BeautifulSoup

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "footballengine.settings")
django.setup()

from engine.models import *

msc = pytz.timezone("Etc/GMT+3")

#возвращаемся и забираем что осталось
def update_paste_matches():
    cccount = 0
    items = Match.objects.filter(~Q(match_status='4'), Q(parse_url__contains='https://championat.com') | Q(parse_url__contains='https://www.championat.com'), datetime__lte=datetime.datetime.now())
    #items = Match.objects.filter(parse_url__contains='https://www.championat.com/football/_ucl/tournament/2605/match/754561/')
    for this_match in items:
        soup = None
        try:
            req = requests.get(this_match.parse_url, headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'})
            soup = BeautifulSoup(''.join(req.text))
            status = soup.findAll("div", {"class": "match-info__status"})[0].text.strip()
            if status in ['Матч окончен', 'Окончен']:
                get_score = re.sub('[\n\s]','', soup.findAll("div", {"class": "match-info__score-total"})[0].text)
                get_extra_score = soup.findAll("div", {"class": "match-info__score-extra"})[0].text.strip()
                goals = [[], []]
                fouls = [[], []]
                match_stat = soup.findAll("div", {"class": "match-stat__row"})
                players = [[], []]
                players_soup = soup.findAll("table", {"class": "match-lineup__players"})
                for i in match_stat:
                    if '_team1' in str(i):
                        if 'match-score' in str(i):
                            if 'match-stat__desc' in str(i):
                                goals[0].append({"minute": i.findAll("div", {"class": "match-stat__main-value"})[0].contents[0].strip(), "player": i.findAll("a", {"class": "match-stat__player"})[0].text.strip(), "player2": i.findAll("a", {"class": "match-stat__player2"})[0].text.strip() if i.findAll("a", {"class": "match-stat__player2"}) else None , "additional" : "_pen" if i.findAll("div", {"class": "_pen"}) else "_own" if i.findAll("div", {"class": "_own"}) else ""})
                            else:
                                goals[0].append({"minute": i.findAll("div", {"class": "match-stat__main-value"})[0].text.strip(), "player": i.findAll("a", {"class": "match-stat__player"})[0].text.strip(), "player2": i.findAll("a", {"class": "match-stat__player2"})[0].text.strip() if i.findAll( "a", {"class": "match-stat__player2"}) else None, "additional": "_pen" if i.findAll("div", {"class": "_pen"}) else "_own" if i.findAll("div", {"class": "_own"}) else ""})
                        else:
                            if 'match-stat__add-info' in str(i):
                                if i.findAll("a", {"class": "match-stat__player"}):
                                    fouls[0].append({"minute": i.findAll("div", {"class": "match-stat__main-value"})[0].text.strip(), "player": i.findAll("a", {"class": "match-stat__player"})[0].text.strip(), "type": "yellow" if i.findAll("span", {"class": "match-icon"}) else "red"})
                    else:
                        if 'match-score' in str(i):
                            if 'match-stat__desc' in str(i):
                                goals[1].append({"minute": i.findAll("div", {"class": "match-stat__main-value"})[0].contents[0].strip(), "player": i.findAll("a", {"class": "match-stat__player"})[0].text.strip(), "player2": i.findAll("a", {"class": "match-stat__player2"})[0].text.strip() if i.findAll("a", {"class": "match-stat__player2"}) else None , "additional" : "_pen" if i.findAll("div", {"class": "_pen"}) else "_own" if i.findAll("div", {"class": "_own"}) else ""})
                            else:
                                goals[1].append({"minute": i.findAll("div", {"class": "match-stat__main-value"})[0].text.strip(), "player": i.findAll("a", {"class": "match-stat__player"})[0].text.strip(), "player2": i.findAll("a", {"class": "match-stat__player2"})[0].text.strip() if i.findAll( "a", {"class": "match-stat__player2"}) else None, "additional": "_pen" if i.findAll("div", {"class": "_pen"}) else "_own" if i.findAll("div", {"class": "_own"}) else ""})
                        else:
                            if 'match-stat__add-info' in str(i):
                                if i.findAll("a", {"class": "match-stat__player"}):
                                    fouls[1].append({"minute": i.findAll("div", {"class": "match-stat__main-value"})[0].text.strip(), "player": i.findAll("a", {"class": "match-stat__player"})[0].text.strip(), "type" : "yellow" if i.findAll("span", {"class": "match-icon"}) else "red"})

                for i in players_soup[0].find("tbody").findAll("tr"):
                    items = i.findAll("td")
                    if len(items):
                        players[0].append({"number": items[0].text.strip(), "pos": items[1].find("span", {"class": "table-item__amplua"}).text.strip(), "name" : items[1].find("span", {"class": "table-item__name"}).text})
                for i in players_soup[1].find("tbody").findAll("tr"):
                    items = i.findAll("td")
                    if len(items):
                        players[1].append({"number": items[0].text.strip(), "pos": items[1].find("span", {"class": "table-item__amplua"}).text.strip(), "name" : items[1].find("span", {"class": "table-item__name"}).text})
            this_match.goals = goals
            this_match.foals = fouls
            this_match.score = get_score
            if get_score:
                this_match.match_status = '4'
            this_match.osnova1 = players[0]
            this_match.osnova2 = players[1]
            this_match.save()
            cccount += 1
        except Exception:
            pass
    return cccount


def update_dates(matches):

    from datetime import datetime

    dates = {
        'января': 'jan',
        'февраля': 'feb',
        'марта': 'mar',
        'апреля': 'apr',
        'мая': 'may',
        'июня': 'jun',
        'июля': 'jul',
        'августа': 'aug',
        'сентября': 'sep',
        'октября': 'oct',
        'ноября': 'nov',
        'декабря': 'dec',
    }

    rounds = 0

    for m in matches:
        curr_day = m.datetime.day
        query = requests.get(m.parse_url, headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'})
        soup = BeautifulSoup(''.join(query.text))
        try:
            result1 = soup.find('div', {'class': 'match-info__date'}).text.strip()
        except Exception:
            result1 = soup.find('div', {'class': 'match-info__title'}).text.strip()

        # добываем дату
        try:
            dt = result1
            curr_month = re.sub('\d+', '', dt.split(',')[0]).strip()
            dt = re.sub(curr_month, dates[curr_month], dt)
            try:
                h_m = re.search('\d{2,2}:\d{2,2}', dt).group(0)
            except Exception:
                h_m = '00:00'
            dt = dt.split(',')[0] + ', ' + h_m
            dt = datetime.strptime(dt, '%d %b %Y, %H:%M')
        except Exception:
            dt = datetime.now()

        new_day = dt.day

        if curr_day != new_day:
            m.alias = pytils.translit.translify(
                m.command_first.name + '-' + m.command_second.name + '-' + datetime.strftime(m.datetime, '%d-%m-%Y')).lower()
            m.alias = re.sub(' ', '-', m.alias)
            m.alias = re.sub("[\.|\#|\(|\)|\']", '', m.alias)

        m.datetime = dt
        m.save()
        rounds += 1
    return rounds