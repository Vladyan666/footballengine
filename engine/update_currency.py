import os
import sys
import requests
import django
from django.apps import apps


sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "footballengine.settings")
django.setup()

from engine.models import Currency

def update_currs():

    cb_currs = requests.get('https://www.cbr-xml-daily.ru/daily_json.js').json()

    for item in Currency.objects.all():
        try:
            item.original_rate = cb_currs['Valute'][item.slug]['Previous']
            item.our_rate = cb_currs['Valute'][item.slug]['Previous'] * 1.02
            item.save()
        except:
            pass

update_currs()