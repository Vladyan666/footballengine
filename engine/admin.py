from django.contrib import admin
from django.contrib.auth.decorators import user_passes_test
from django.http.response import HttpResponseRedirect
from django.contrib.admin.models import LogEntry
from django.contrib import messages
import threading
from modeltranslation.admin import TranslationAdmin, TranslationGenericStackedInline, TabbedTranslationAdmin, TranslationTabularInline

from engine.models import *

# Register your models here.
# общие поля
page_fields = [
    (u"Настройки страницы", {'fields': [ 'menutitle', 'alias', 'menushow', 'sitemap']}),
    (u"SEO информация", {'fields': ['seo_h1', 'seo_title', 'seo_description', 'seo_keywords', 'content', ]})
]
page_list = ('menushow', 'sitemap')

# Страницы
class TextPageAdmin(TabbedTranslationAdmin):
    fieldsets = [(u'Основные', {'fields': ['name', 'host', 'og_title', 'og_description', 'og_type_pb_time', 'og_type_author',]}), ] + page_fields
    list_filter = ['host']
    search_fields = ['name']
    list_display = ('name', 'alias', 'host')

    # def get_queryset(self, request):
    #     queryset = super(TextPageAdmin, self).get_queryset(request)
    #
    #     return queryset.filter(host__name=request.get_host())

    # def get_prepopulated_fields(self, request, obj=None):
    #     # can't use `prepopulated_fields = ..` because it breaks the admin validation
    #     # for translated fields. This is the official django-parler workaround.
    #     return {"alias": ("name",)}

#
admin.site.register(TextPage, TextPageAdmin)

# news
class NewsAdmin(admin.ModelAdmin):

    list_display = ('name', 'host',)

    fieldsets = [(u'Основные', {'fields': ['name', 'host', 'image', 'datetime', 'seo_title', 'seo_h1', 'seo_description', 'seo_keywords', 'content',  'alias', ]}), ]
    list_filter = ['host']

    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}

#
admin.site.register(News, NewsAdmin)

#Уникальные федерации
class Fed_UniqInline(admin.StackedInline):
    model = Fed_Uniq
    extra = 3

# Федерации
class FedAdmin(admin.ModelAdmin):
    list_display = ('name',) + page_list
    fieldsets = [
                    (u'Информация о федерации', {'fields': ['name', 'acronym', 'image']}),
                ] + page_fields
    inlines = [Fed_UniqInline]
#    list_filter = ['host']


    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("name",)}

#
admin.site.register(Fed, FedAdmin)

#Уникальные Страны
class Country_UniqInline(admin.StackedInline):
    model = Country_Uniq
    extra = 3

# Страны
@admin.register(Country)
class CountryAdmin(TabbedTranslationAdmin):
    list_display = ('name',) + page_list
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name', 'flag', 'tz',]}),
                ] + page_fields
    inlines = [Country_UniqInline]

#
#admin.site.register(Country, CountryAdmin)

#Уникальные города
class City_UniqInline(admin.StackedInline):
    model = City_Uniq
    extra = 3

# города
class CityAdmin(TabbedTranslationAdmin):
    list_display = ('name',) + page_list
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name','country','menuposition','image',]}),
                ] + page_fields
    inlines = [City_UniqInline]
#    list_filter = ['host']

admin.site.register(City, CityAdmin)

#tabular stadium
class Stadium_UniqInline(admin.StackedInline):
    model = Stadium_Uniq
    fieldsets = [(u'Информация о странице', {'fields': ['seo_h1', 'seo_title', 'seo_description', 'content', 'host', ]}),]
    extra = 1

# Стадионы
class StadiumAdmin(TabbedTranslationAdmin):
    list_display = ('name_ru', 'alias',)
    search_fields = ['name_ru']
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name', 'city', 'photo', 'image', 'svg_scheme', 'big_image','traffic', 'alias' ]}),
                ]
    inlines = [Stadium_UniqInline]

admin.site.register(Stadium, StadiumAdmin)


#tabular Champ
class Champ_UniqInline(admin.StackedInline):
    model = Champ_Uniq
    fieldsets = [(u'Информация о странице', {'fields': ['host', 'seo_h1', 'seo_title', 'seo_description', 'content' ]}),]
    extra = 1

# Чемпионаты
class ChampAdmin(TabbedTranslationAdmin):
    list_display = ('name',)
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name', 'fed', 'priority', 'image', 'year', 'active', 'teams', 'alias',]}),
                ]

    filter_horizontal = ('teams',)
    inlines = [Champ_UniqInline]


    exclude = (
    'seo_title_ru', 'seo_h1_ru', 'seo_description_ru', 'seo_description_en', 'content_ru', 'seo_h1_en', 'content_en',
    'seo_title_en', )

admin.site.register(Champ, ChampAdmin)


class Team_UniqInline(TranslationTabularInline):
    model = Team_Uniq
    fieldsets = [(u'Информация о странице', {'fields': ['seo_h1', 'seo_title', 'seo_description', 'content', 'host', ]}),]
    extra = 1

# Команды
@admin.register(Team)
class TeamAdmin(TabbedTranslationAdmin):
    list_display = ('name',)
    search_fields = ['name']
    list_filter = ['stadium__city__country__name', 'team_type']
    inlines = [Team_UniqInline,]
    fieldsets = [(u'Информация о странице', {'fields': ['name', 'alt_name', 'team_type', 'logo2', 'children_age', 'stadium', 'logo', 'flag', 'photo', 'techteam', 'eng_name', 'short_description', 'alias', ]}),]

# Прайс
class PriceInline(admin.TabularInline):
    model = Price
    extra = 0

# unic_match
class Match_UniqInline(admin.StackedInline):
    model = Match_Uniq
    fieldsets = [(u'Информация о странице', {'fields': ['seo_h1', 'seo_title', 'seo_description', 'content', 'host', ]}),]
    extra = 0

# Матчи
class MatchAdmin(TabbedTranslationAdmin):
    list_display = ('name_ru', 'datetime')
    search_fields = ['command_first__name_ru', 'command_second__name_ru']
    inlines = [Match_UniqInline, PriceInline]
    list_filter = ['champ__name_ru', 'champ__year']
    save_on_top = True
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name', 'stadium', 'no_sellout', 'match_status', 'only_list', 'default_scheme_colors', 'foals', 'datetime', 'popular', 'beautiful', 'stop_online_sell', 'match_markups', 'score', 'champ', 'parse_url', 'carryall_id', 'count_number', 'techmatch', 'hide_match', 'tour', 'fake_match', 'special_seats', 'alias' ]}),
                ]
#    list_filter = ['host']

#     def get_prepopulated_fields(self, request, obj=None):
#         # can't use `prepopulated_fields = ..` because it breaks the admin validation
#         # for translated fields. This is the official django-parler workaround.
#         return {"alias": ('name',)}

#
admin.site.register(Match, MatchAdmin)

class Staticmatch_UniqInline(TranslationTabularInline):
    model = Staticmatch_Uniq
    fieldsets = [(u'Информация о странице', {'fields': ['seo_h1', 'seo_title', 'seo_description', 'content', 'host', ]}), ]
    extra = 0

# staticМатчи
class StaticmatchAdmin(TabbedTranslationAdmin):
    list_display = ('command_first', 'command_second',)
    search_fields = ['command_first__name', 'command_second__name']
#    list_filter = ['static__champ', 'static__champ__year']
    inlines = [Staticmatch_UniqInline]
    fieldsets = [
                    (u'Информация о странице', {'fields': ['command_first', 'command_second', 'alias', ]}),
                ]

#
admin.site.register(Staticmatch, StaticmatchAdmin)

# staticМатчи
class GroupAdmin(TabbedTranslationAdmin):
    list_display = ('name', 'champ', 'menuposition',)
    list_editable = ['menuposition']
    search_fields = ['name_ru']
    list_filter = ['champ']
    filter_horizontal = ('teams',)
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name', 'champ', 'teams', 'image', ]}),
                ]

#
admin.site.register(Group, GroupAdmin)

# Натройки сайтов
class SiteSettingsAdmin(TabbedTranslationAdmin):
    list_display = ('host',)
    fieldsets = [(u'Информация о странице', {'fields': ['success', 'site_index_hml', 'main_team', 'legend', 'index_type', 'sport_type', 'champ_table_id', 'default_currency', 'main_champ', 'banner_html', 'news_logo', 'support_your_team', 'support_your_team_text', 'loader', 'site_word', 'host', 'banner_logo', 'banner_main' , 'banner_matches', 'video_main', 'main_banner_text', 'main_banner_text_two', 'font', 'phone', 'site_logo_text', 'site_logo', 'metrics_yandex', 'metrics_google', 'google_optimize', 'mango_phone', 'jivosite', 'robots', 'header_extends', 'sendsay_basket_json', 'sendsay_webpush_json', 'favicon', 'markups', 'canvas']}),]
    list_filter = ['host']
    filter_horizontal = ('main_champ',)
#
admin.site.register(SiteSettings, SiteSettingsAdmin)

#orders
class SiteOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['bin_id', 'phone', 'name', 'email']
    #fieldsets = ('token',)


    actions = None
#
admin.site.register(SiteOrder, SiteOrderAdmin)

#успешный заказ
admin.site.register(FinallyOrder)

#Сетификат
class CertificateAdmin(admin.ModelAdmin):
    list_display = ('name', 'active', 'sort', 'buy_count')
    list_editable = ['sort', 'active']
    fieldsets = [
                    (u'Информация о странице', {'fields': ['name', 'image', 'active', 'sort']}),
                ]

admin.site.register(Certificate, CertificateAdmin)

#Билеты кериола
class TicketCarryallAdmin(admin.ModelAdmin):
    list_display = ('hash','match')
    fieldsets = [
                    (u'Информация о странице', {'fields': ['hash', 'order', 'match', 'price', 'ruble_price', 'sector', 'row', 'seat', 'reserved', 'source', 'currency']}),
                ]
#
admin.site.register(TicketCarryall, TicketCarryallAdmin)

#Билеты кериола
class SiteSeoHeadersAdmin(TabbedTranslationAdmin):
    list_display = ('pk', 'header_type', 'host')
    fieldsets = [
                    (u'Информация о странице', {'fields': ['header_type', 'host', 'match', 'static', 'stadium', 'team']}),
                ]
#
admin.site.register(SiteSeoHeaders, SiteSeoHeadersAdmin)


class StaticTextAdmin(TabbedTranslationAdmin):
    list_display = ('pk', 'host')
    fieldsets = [
                    (u'Информация о странице', {'fields': ['host', 'match_text', 'match_preview_text']}),
                ]
#
admin.site.register(StaticText, StaticTextAdmin)

class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'slug', 'original_rate', 'our_rate', 'lower_sign', 'mark')

admin.site.register(Currency, CurrencyAdmin)

def clear_all_cache():
    """
    Сбрасываем ВЕСЬ кеш
    :return: None
    """

    def _clear_doorway_cache():
        cache = caches['default']
        cache.clear()
        # logger.info('clear_all_cache task finished')

    t = threading.Thread(target=_clear_doorway_cache)
    t.daemon = True
    t.start()
    # logger.info('clear_all_cache task started')

@user_passes_test(lambda u: u.is_superuser)
def clear_all_cache_view(request):
    clear_all_cache()
    LogEntry.objects.create(object_repr='Clear cache', action_flag=True, user=request.user)
    messages.info(request, message='Кэш успешно очищен.')
    return HttpResponseRedirect(reverse_lazy('admin:index'))