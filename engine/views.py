import django
import datetime
import json
import time
import os
import random
import re
import requests
import pytz
import base64

from random import randint
from functools import reduce
from operator import itemgetter

from django.template import Context
from django.template import Template
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_page
from django.utils.dateformat import format
from django.utils import six, translation
from django.db.models import Q
from django.contrib.auth.decorators import user_passes_test
from django.contrib.admin.models import LogEntry
from django.contrib import messages
from django.http.response import HttpResponseRedirect, Http404
from django.http import HttpResponse, JsonResponse
from django.template.context_processors import csrf
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect
from django.utils.formats import dateformat, date_format

from footballengine.settings.settings_global import CARRYALL_HEADERS
from footballengine.settings.settings_global import BINTRANET_URL, BINTRANET_HEADERS
from acquiring_client.helpers import acquiring

from footballengine.settings import settings_global as djang_settings

from .utils import uncustom_bin_id as UNCUSTOM_ID_FUNC
from .utils import custom_bin_id, to_other, from_other_to_other, to_rubles, update_sector_prices, update_ticket_price, \
    update_static_ticket_price

from engine.utils import calculate_price, get_client_ip, sitemap_sections, clear_all_cache, get_current_hostname, \
    crazy_mega_parser, update_urls, get_nearest_matches, get_passed_matches

from engine.come_back import update_paste_matches, update_dates
from engine.templates_resolve import render_matches
from engine.forms import *
from engine.models import *
from footballengine.serializers import SiteOrderSerializer, TicketCarryallSerializer, SiteOrderItemSerializer

import logging

logger = logging.getLogger("payments")

from analytics.constants import ANALITYCS_KEY, UTM_SOURCE, UTM_MEDIUM, UTM_CAMPAIGN, UTM_TERM, UTM_CONTENT, UTM_CHOICES, UTM_CAMPAIGN_ID, UTM_GROUP_ID, UTM_TERM_ID

# moscow timezone
msc = pytz.timezone("Etc/GMT+3")

CARRYALL_URL = 'http://%s:%s' % (random.choice(djang_settings.CLUSTER_IPS), djang_settings.CARRYALL_PORT)

def default_context(request, alias, object):

    if request.get_host().split('.')[0] in ['en']:
        translation.activate('en')

    sitesettings = SiteSettings.objects.get(host=get_current_hostname(request))

    unic_object = None

    try:
        unic_object = django.apps.apps.get_model('engine', object.__name__ + '_Uniq')
    except:
        pass

    if unic_object:
        try:
            uniq_data = unic_object.objects.get(alias=alias, host=get_current_hostname(request))
            if object.__name__ == 'Team':
                data = object.objects.get(alias=alias, team_type=sitesettings.sport_type)
            elif object.__name__ == 'Staticmatch':
                data = object.objects.get(alias=alias, command_first__team_type=sitesettings.sport_type)
            else:
                data = object.objects.get(alias=alias)
            data.content = uniq_data.content
            data.seo_h1 = uniq_data.seo_h1
            data.seo_title = uniq_data.seo_title
            data.seo_description = uniq_data.seo_description
            data.seo_keywords = uniq_data.seo_keywords
        except unic_object.DoesNotExist:
            try:
                if object.__name__ == 'Team':
                    count = object.objects.get(alias=alias, team_type=sitesettings.sport_type).static.filter(
                        host=get_current_hostname(request))
                elif object.__name__ == 'Staticmatch':
                    count = object.objects.get(alias=alias, command_first__team_type=sitesettings.sport_type).static.filter(
                        host=get_current_hostname(request))
                else:
                    count = object.objects.get(alias=alias).static.filter(host=get_current_hostname(request))
            except object.DoesNotExist:
                raise Http404

            if count:
                raise Http404
            else:
                if object.__name__ == 'Team':
                    data = object.objects.get(alias=alias, team_type=sitesettings.sport_type)
                elif object.__name__ == 'Staticmatch':
                    data = object.objects.get(alias=alias, command_first__team_type=sitesettings.sport_type)
                else:
                    data = object.objects.get(alias=alias)

        except Exception:
            raise Http404
    else:
        try:
            data = object.objects.get(alias=alias, host=get_current_hostname(request))
        except:
            raise Http404

    seo_title = None
    seo_description = None
    seo_keywords = None
    seo_h1 = None
    seo_content = None

    if object.__name__ in ['Match', 'Stadium', 'Staticmatch', 'Team', 'champ']:

        if object.__name__ == 'Team':
            params_for_headers = Context({'data': object.objects.get(alias=alias, team_type=sitesettings.sport_type), 'domain': request.get_host()})
        elif object.__name__ == 'Staticmatch':
            params_for_headers = Context({'data': object.objects.get(alias=alias, command_first__team_type=sitesettings.sport_type), 'domain': request.get_host()})
        else:
            params_for_headers = Context({'data': object.objects.get(alias=alias), 'domain': request.get_host()})

        headers = SiteSeoHeaders.objects.filter(host=get_current_hostname(request))

        try:
            seo_title = {"match": Template(headers.get(header_type=1).match).render(params_for_headers),
                         "static": Template(headers.get(header_type=1).static).render(params_for_headers),
                         "stadium": Template(headers.get(header_type=1).stadium).render(params_for_headers),
                         "team": Template(headers.get(header_type=1).team).render(params_for_headers)}
        except:
            pass

        try:
            seo_description = {"match": Template(headers.get(header_type=2).match).render(params_for_headers),
                               "static": Template(headers.get(header_type=2).static).render(params_for_headers),
                               "stadium": Template(headers.get(header_type=2).stadium).render(params_for_headers),
                               "team": Template(headers.get(header_type=2).team).render(params_for_headers)}
        except:
            pass

        try:
            seo_keywords = {"match": Template(headers.get(header_type=3).match).render(params_for_headers),
                            "static": Template(headers.get(header_type=3).static).render(params_for_headers),
                            "stadium": Template(headers.get(header_type=3).stadium).render(params_for_headers),
                            "team": Template(headers.get(header_type=3).team).render(params_for_headers)}
        except:
            pass

        try:
            seo_h1 = {"match": Template(headers.get(header_type=4).match).render(params_for_headers),
                      "static": Template(headers.get(header_type=4).static).render(params_for_headers),
                      "stadium": Template(headers.get(header_type=4).stadium).render(params_for_headers),
                      "team": Template(headers.get(header_type=4).team).render(params_for_headers)}
        except:
            pass


    if object.__name__ in ['Match', 'Staticmatch']:
        try:
            params_for_headers = Context({'data': object.objects.get(alias=alias, command_first__team_type=sitesettings.sport_type), 'domain': request.get_host()})
        except:
            raise Http404
        try:
            content_temp = StaticText.objects.get(host=get_current_hostname(request))
            if object.__name__ in ['Match']:
                seo_content = Template(content_temp.match_text).render(params_for_headers)
            elif object.__name__ in ['Staticmatch']:
                seo_content = Template(content_temp.match_preview_text).render(params_for_headers)
        except:
            pass

    cart = SiteOrder.get_session(request=request)

    currencys = Currency.objects.all()

    c = {}
    c.update(csrf(request))

    context_object = {
        'seo_title': seo_title,
        'seo_description': seo_description,
        'seo_keywords': seo_keywords,
        'seo_h1': seo_h1,
        'seo_content': seo_content,
        'c': c,
        'user': request.user,
        'data': data,
        'cart': cart,
        'currency': cart.user_currency,
        'sitesettings': sitesettings,
        'currencys': currencys,
        'curr_url': request.get_host() + request.path
    }
    return context_object

class BintranetSendForm(object):

    bin_content = None
    form_type = None
    request = None
    current_order = None
    order_number = None
    acquiring_params = None

    # дефолтные параметры
    def __init__(self, request, form_type):
        self.bin_content = {
            'device': u'Mobile' if request.user_agent.is_mobile else 'Desktop',
            'transition': u'Переход из '
                          + request.seo.analytics.get(
                          UTM_SOURCE, False) if request.seo.analytics.get(UTM_SOURCE, False) else u'СЕО',
            'advert': request.seo.analytics.get(UTM_MEDIUM, ''),
            'advertising_company': request.seo.analytics.get(UTM_CAMPAIGN, ''),
            'transition_string': request.seo.analytics.get(UTM_TERM, ''),
            'advertising_campaign_id': request.seo.analytics.get(UTM_CAMPAIGN_ID, ''),
            'advertising_group_id': request.seo.analytics.get(UTM_GROUP_ID, ''),
            'client_referer': request.seo.get_seo_info()['source_id'],
            'term_id': request.seo.analytics.get(UTM_TERM_ID, ''),
            "site_name": get_current_hostname(request).name,
            "client_ip": get_client_ip(request),
            "is_staff": True if request.user.id else False,
            "tickets": [],
            "info": ""
        }
        self.form_type = form_type
        self.request = request

    # функция сохранения сайтового заказа
    def updateSiteOrder(self, params):
        self.current_order = SiteOrder.get_session(request=self.request)
        SiteOrder.objects.filter(pk=self.current_order.pk).update(**params)
        self.current_order = SiteOrder.objects.get(pk=self.current_order.pk)

    # собственно отправка
    def send(self):

        #Текущий заказ

        s = self.current_order
        # создаем кастомный ID и прикручиваем к заказу
        cust_bin_id = FinallyOrder.objects.create(hash=s.token)
        s.custom_bin_id = cust_bin_id
        s.save()

        self.bin_content.update({
            "custom_id": custom_bin_id(s.custom_bin_id.id)
        })

        # Отправка в bintranet
        print(self.bin_content)
        item = requests.post(BINTRANET_URL + '/api/create-order-from-site/',
                             headers=BINTRANET_HEADERS,
                             data=json.dumps(self.bin_content, ensure_ascii=False).encode('utf8'), timeout=10)


        # если заказ успешен записываем ID
        try:
            s.bin_id = item.json().get('pk', -1)
            s.save()
        except:
            s.bin_id = -1
            s.save()

        self.order_number = custom_bin_id(s.custom_bin_id.id)

        if self.acquiring_params:
            # отключиние онлайн оплат если время сеанса скоро придет
            stop_sell = False
            stop_sell = reduce(lambda x, y: x or y, [True if ((int(format(msc.localize(
                i.match.datetime.replace(tzinfo=None)), 'U')) - int(
                time.time())) / 60) - 360 < i.match.stop_online_sell else False for i in
                                                     (s.c_tickets.all() if s.c_tickets.all().count() else s.s_item.all())])

            # Эквайринг
            try:
                test_host = 'test'
                response_acquiring = acquiring(
                    request=self.request,
                    bintranet_id=self.order_number,
                    amount=self.acquiring_params['total_summ'],
                    amount_rub=self.acquiring_params['total_summ'],
                    host=get_current_hostname(self.request).name,
                    customer_name=self.bin_content['client_name'],
                    phone=self.bin_content['client_phone'],
                    email=self.bin_content['client_email'],
                    ip=get_client_ip(self.request),
                    tickets=self.acquiring_params['atol_tickets'],
                    currency="RUB",
                    language='en' if self.request.get_host().split('.')[0] in ['en'] else 'ru'
                )
                if int(self.acquiring_params['paytype']) == 3 and self.acquiring_params['total_summ'] > 0 and not stop_sell:
                    return redirect(response_acquiring)

            except Exception:
                print('не прошел акирсинг:(((')

        return self.order_number


    # Корпоративная заявка
    def corporate_info(self, request):
        match = Match.objects.get(id=request.POST['match_id'])

        # сохраняем в модель ( на всякий случай )
        params = {'phone': request.POST['phone'],
                  'name': request.POST['name'],
                  'email': request.POST.get('email', 'Не указанно'),
                  'comment': match.name
                             + ', '
                             + datetime.datetime.strftime(match.datetime, '%d-%m-%Y %H:%M')
                             + ', кол-во человек - '
                             + request.POST['people_num']
                             + ', Комментарий - '
                             + request.POST['comments'],
                  'order_type': 3}

        self.updateSiteOrder(params)

        # обновляем параметры для бинтры
        self.bin_content.update({"info": match.name + ', '
                                 + datetime.datetime.strftime(match.datetime,
                                 '%d-%m-%Y %H:%M') + ', кол-во человек - ' +
                                 request.POST['people_num']
                                 + ', Комментарий - '
                                 + request.POST['comments'],
                                 "pay_type": 1,
                                 "client_name": request.POST['name'],
                                 "client_phone": request.POST['phone'],
                                 "client_address": 'Не указанно',
                                 "client_email": request.POST.get('email', 'Не указанно')
                                 })

    # Обратный звонок
    def callback_info(self, request):
        params = {'phone': request.POST['phone'],
                  'name': request.POST['name'],
                  'email': request.POST.get('email', 'Не указанно'),
                  'comment': request.POST.get('comments', 'клиент не оставил'),
                  'order_type': 2
                  }
        self.updateSiteOrder(params)

        # обновляем параметры для бинтры
        self.bin_content.update({"info": 'Заказ обратного звонка. Комментарий - '
                                         + request.POST.get('comments', 'клиент не оставил'),
                                 "pay_type": 1,
                                 "client_name": request.POST['name'],
                                 "client_phone": request.POST['phone'],
                                 "client_address": 'Не указанно',
                                 "client_email": request.POST.get('email', 'Не указанно'),
                                 "client_comment": request.POST.get('comments', 'клиент не оставил'),
                                 'status': 37,
                                 })

        # проверка! если есть инфа по матчу то добавляем фейковый билетик
        if request.POST.get('match', None):
            self.bin_content['tickets'] = [{"event_name": request.POST['match'],
                        "event_place_name": '',
                        "event_time_datetime": request.POST['datetime'],
                        "sale": 0,
                        "count": 1}]
        else:
            self.bin_content['tickets'] = []


    # Заказ сертификата
    def certificate_info(self, request):

        _default = 'Не указано'
        cost = request.POST.get('cost', 0)
        self.bin_content.update({
            "client_name": request.POST.get('name', _default),
            "client_phone": request.POST.get('phone', _default),
            "client_comment": request.POST.get('recipient', _default),
            "pay_type": 1,
        })

        params = {'phone': request.POST.get('phone', 'Не указано'),
                  'name': request.POST.get('name', 'Не указано'),
                  'order_type': 4
                  }

        self.updateSiteOrder(params)


        tickets = [{'event_time_carryall_id': 0, 'seat': '', 'info': '',
                             'event_carryall_id': 0, 'sector_name': request.POST.get('type'),
                             'event_name': 'Сертификат',
                             'nominal': None, 'ticket_source_name': '-', 'row': '', 'event_place_name': '',
                             'count': 1, 'info': 'Для: {}, Тип: {}'.format(request.POST.get('recipient'),
                                                                           request.POST.get('cert_type')),
                             'sale': cost if cost else request.POST.get('cost_option'),
                             'event_time_datetime': '2100-01-01T00:00:00Z'}]

        if request.POST.get('service_1'):
            tickets.append(
                {'event_time_carryall_id': 0, 'seat': '', 'info': request.POST.get('service_1').split(',')[1],
                 'event_carryall_id': 0, 'sector_name': '', 'event_name': 'Мерседес S-класса',
                 'nominal': None, 'ticket_source_name': '-', 'row': '', 'event_place_name': '',
                 'count': 1, 'sale': request.POST.get('service_1').split(',')[0],
                 'event_time_datetime': '2100-01-01T00:00:00Z'})

        if request.POST.get('service_2'):
            tickets.append({'event_time_carryall_id': 0, 'seat': '', 'info': 'Депозит',
                                            'event_carryall_id': 0, 'sector_name': '',
                                            'event_name': 'Ужин в ресторане «Novikov Group»',
                                            'nominal': None, 'ticket_source_name': '-', 'row': '',
                                            'event_place_name': '',
                                            'count': 1, 'sale': request.POST.get('service_2'),
                                            'event_time_datetime': '2100-01-01T00:00:00Z'})

        if request.POST.get('service_3'):
            tickets.append({'event_time_carryall_id': 0, 'seat': '', 'info': 'Депозит',
                                            'event_carryall_id': 0, 'sector_name': '',
                                            'event_name': 'Создайте образ в салоне KYNSI',
                                            'nominal': None, 'ticket_source_name': '-', 'row': '',
                                            'event_place_name': '',
                                            'count': 1, 'sale': request.POST.get('service_3'),
                                            'event_time_datetime': '2100-01-01T00:00:00Z'})

        self.bin_content.update({
            "tickets": tickets
        })

    # Order info
    def order_info(self, request):
        context_data = default_context(request, 'checkout', TextPage)
        form = ContactForm(request.POST)

        if form.is_valid():
            _default = 'Не указано'
            name = form.cleaned_data.get('name', _default)
            email = form.cleaned_data.get('email', _default)
            addr = form.cleaned_data.get('addr', _default)
            phone = form.cleaned_data.get('phone_code', '') + form.cleaned_data.get('phone', _default)
            cl_comment = form.cleaned_data.get('textarea', _default)
            paytype = request.POST['pay']

            params = {'phone': phone,
                      'name': name,
                      'email': email,
                      'comment': cl_comment,
                      'order_type': 1,
                      'paymethod': int(paytype)
                      }

            self.updateSiteOrder(params)

            bintranet_tickets = []
            ticks_for_book = {}
            atol_tickets = []
            total_summ = 0

            try:
                # тестовый заказ = отмена брони
                if email == 'test@test.test':
                    1 / 0

                #билеты для бронирования
                for i in context_data['cart'].c_tickets.all():
                    ticks_for_book.update({i.hash: 1})

                # бронирование билетов
                s = requests.Session()
                book_ticks_json = {'tickets': ticks_for_book}
                resp = s.post(CARRYALL_URL + '/api/book_tickets/', timeout=10, headers=CARRYALL_HEADERS,
                              json=book_ticks_json).json()

                successfull_booked = []

                for items in resp['available_tickets']:
                    successfull_booked.append(items['id'])

                for i in context_data['cart'].c_tickets.all():
                    bintranet_tickets.append({
                        "event_name": i.match.name,
                        "event_place_name": i.match.stadium.name + ', ' + i.match.stadium.city.name if i.match.stadium else None,
                        "event_time_datetime": i.match.datetime.strftime("%Y-%m-%dT%H:%M"),
                        "sector_name": i.sector,
                        "ticket_source_name": i.source,
                        "carryall_id": i.hash,
                        "row": i.row if i.row else '-',
                        "seat": i.seat if i.seat else '-',
                        "sale": str(i.ruble_price)[:-2],
                        "count": i.count,
                        "info": ('Бронь - ' + ', '.join(
                            resp['zriteli_orders']) if i.hash in successfull_booked else 'Не в броне')
                    })
                    atol_tickets.append({"count": 1, "price": str(i.ruble_price)[:-2]})
                    total_summ += int(i.ruble_price)
            except Exception:
                for i in context_data['cart'].c_tickets.all():
                    bintranet_tickets.append({
                        "event_name": i.match.name,
                        "event_place_name": i.match.stadium.name + ', ' + i.match.stadium.city.name if i.match.stadium else None,
                        "event_time_datetime": i.match.datetime.strftime("%Y-%m-%dT%H:%M"),
                        "sector_name": i.sector,
                        "ticket_source_name": i.source,
                        "carryall_id": i.hash,
                        "row": i.row if i.row else '-',
                        "seat": i.seat  if i.seat else '-',
                        "sale": str(i.ruble_price)[:-2],
                        "count": i.count,
                    })
                    atol_tickets.append({"count": 1, "price": str(i.ruble_price)[:-2]})
                    total_summ += int(i.ruble_price)

            for i in context_data['cart'].s_item.all():
                bintranet_tickets.append({
                    "event_name": i.price.match.name,
                    "event_place_name": i.match.stadium.name + ', ' + i.match.stadium.city.name if i.match.stadium else None,
                    "event_time_datetime": i.price.match.datetime.strftime("%Y-%m-%dT%H:%M"),
                    "sector_name": i.price.sector,
                    "ticket_source_name": 'Ручные цены',
                    "sale": str(round(i.price.ruble_price())),
                    "count": i.count,
                })
                atol_tickets.append({"count": i.count, "price": str(round(i.price.ruble_price()))})
                total_summ += int(i.price.ruble_price() * i.count)

            new_bcontent = {
                "tickets": bintranet_tickets,
                "client_name": name,
                "client_phone": phone,
                "client_email": email,
                "client_address": addr,
                "client_comment": cl_comment,
                "pay_type": paytype,
                "status": 1,
                "info": 'Сертификат № {}'.format(
                    request.POST['cert_id'] if request.POST['cert_id'] else 'Не указан') if int(paytype) == 29 else '-'
            }

            self.bin_content.update(new_bcontent)

            if int(paytype) == 3 and total_summ:
                self.acquiring_params = {'paytype': paytype, 'atol_tickets': atol_tickets, 'total_summ': total_summ}

    def issue(self):

        # выбираем параметры для отправки заказа
        if self.form_type == 'corporate_form':
            self.corporate_info(self.request)
        if self.form_type == 'callback_form':
            self.callback_info(self.request)
        if self.form_type == 'certificate_form':
            self.certificate_info(self.request)
        if self.form_type == 'order_form':
            self.order_info(self.request)

        # отправляем заказ
        return self.send()


#@cache_page(60 * 15)
def index(request):
    template = loader.get_template('index.html')

    context_data = default_context(request, 'index', TextPage)

    news = News.objects.filter(host=get_current_hostname(request)).order_by('-datetime')[:3]

    select_related = ('command_first', 'command_second', 'stadium', 'champ')

    popular_matches = None

    # Тип главной = Чемпионат
    if context_data['sitesettings'].index_type == 1:
        # если сайт для чемпионата
        if context_data['sitesettings'].main_champ.all().count():
            gte_matches = []
            all_champs = []
            for c in context_data['sitesettings'].main_champ.filter(active=True):
                # достаем pks матчей которые скоро будут
                gte_matches += c.champ_matches.all().filter(
                    datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
                # если у текущего чемпоната есть будущие матчи отображаем его в фильтре
                if c.champ_matches.all().filter(datetime__gte=msc.localize(datetime.datetime.now())).count():
                    all_champs.append(c)

            # матчи на главную сначала 20 с билетами и 10 без (если есть)
            self_matches_all = list(
                Match.objects.filter(carryall_id__isnull=False, hide_match=False, pk__in=gte_matches,
                                     fake_match=False).select_related(*select_related).order_by('datetime')[
                :20]) + list(Match.objects.filter(
                carryall_id__isnull=True,
                hide_match=False, pk__in=gte_matches, fake_match=False).select_related(
                *select_related).order_by('datetime')[:10])
        # если сайт для команды
        else:
            # матчи на главную сначала 20 с билетами и 10 без (если есть)
            self_matches_all = list(Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                command_second=context_data['sitesettings'].main_team), carryall_id__isnull=False, hide_match=False,
                                                         datetime__gte = msc.localize(datetime.datetime.now()),
                                     fake_match=False).select_related(*select_related).order_by('datetime')[
                :20]) + list(Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                command_second=context_data['sitesettings'].main_team),
                carryall_id__isnull=True,
                hide_match=False, datetime__gte = msc.localize(datetime.datetime.now()), fake_match=False).select_related(
                *select_related).order_by('datetime')[:10])
            all_champs = []


            for c in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team], active=True):
                # ищем будущие матчи (их кол-во)
                if c.champ_matches.all().filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                        command_second=context_data['sitesettings'].main_team),
                                                datetime__gte=msc.localize(datetime.datetime.now())).count():
                    # если у текущего чемпоната есть будущие матчи отображаем его в фильтре
                    all_champs.append(c)

        popular_matches = [i for i in self_matches_all if i.popular]

        context_data.update({
            'all_champs': all_champs,
            'self_matches_all': self_matches_all,
            'popular_matches': popular_matches,
        })
    # чемпионат + плей офф (по системе хоккея)
    elif context_data['sitesettings'].index_type == 2:
        # если для чемпионата
        if context_data['sitesettings'].main_champ.all().count():
            gte_matches = []
            all_champs = []
            # ищем активные чемпионаты
            for c in context_data['sitesettings'].main_champ.all():
                gte_matches += c.champ_matches.all().filter(
                    datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
                if c.champ_matches.all().filter(datetime__gte=msc.localize(datetime.datetime.now())).count():
                    all_champs.append(c)
            # матчи на главную сначала 20 с билетами и 10 без (если есть)
            self_matches_all = list(
                Match.objects.filter(carryall_id__isnull=False, hide_match=False, pk__in=gte_matches,
                                     fake_match=False).select_related(*select_related).order_by('datetime')[
                :20]) + list(Match.objects.filter(
                carryall_id__isnull=True,
                hide_match=False, pk__in=gte_matches, fake_match=False).select_related(
                *select_related).order_by('datetime')[:10])

        else:
            # матчи на главную сначала 20 с билетами и 10 без (если есть)
            self_matches_all = list(Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                command_second=context_data['sitesettings'].main_team), carryall_id__isnull=False, hide_match=False,
                                                         datetime__gte = msc.localize(datetime.datetime.now()),
                                     fake_match=False).select_related(*select_related).order_by('datetime')[
                :20]) + list(Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                command_second=context_data['sitesettings'].main_team),
                carryall_id__isnull=True,
                hide_match=False, datetime__gte = msc.localize(datetime.datetime.now()), fake_match=False).select_related(
                *select_related).order_by('datetime')[:10])
            all_champs = []
            # ищем активные чемпионаты
            for c in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]):
                if c.champ_matches.all().filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                        command_second=context_data['sitesettings'].main_team),
                                                datetime__gte=msc.localize(datetime.datetime.now())).count():
                    all_champs.append(c)
        # турнирная таблица плей-офф по хоккею
        try:
            khl_table = []
            # для чемпионата
            if context_data['sitesettings'].main_champ.all().count():
                champ_id = context_data['sitesettings'].champ_table_id
                if champ_id in context_data['sitesettings'].main_champ.all().values_list('pk', flat=True):
                    for i in context_data['sitesettings'].main_champ.filter(pk=champ_id):
                        store = []
                        for g in i.champ_groups.all().filter(Q(name='Финал - Запад') | Q(name='Финал - Восток') | Q(
                                name='Финал Чемпионата')):
                            for t in g.teams.all():
                                # таблица [группа, команды, матчи]
                                item = [g, [], g.group_matches.all().filter(
                                    Q(command_first=t) | Q(command_second=t)).order_by('datetime'), [], 0, None]
                                sss = []
                                for k in g.group_matches.all().filter(
                                                Q(command_first=t) | Q(command_second=t)).values_list('command_first',
                                                                                                      'command_second').distinct():
                                    for s in list(k):
                                        sss.append(s)
                                sss = list(set(sss))
                                results = [0, 0]
                                for ss in g.group_matches.all().filter(
                                                Q(command_first=t) | Q(command_second=t)).order_by('datetime'):
                                    if ss.score:
                                        qwe = ss.score.split(':')
                                        if int(qwe[0]) > (int(qwe[1]) if qwe[1].isdigit() else int(qwe[1][0])):
                                            if ss.command_first.pk == sss[0]:
                                                results[0] += 1
                                            else:
                                                results[1] += 1
                                        else:
                                            if ss.command_first.pk == sss[0]:
                                                results[1] += 1
                                            else:
                                                results[0] += 1
                                item[3] = results
                                item[4] = 7 - g.group_matches.all().filter(
                                    Q(command_first=t) | Q(command_second=t)).order_by('datetime').count()
                                item[5] = g.group_matches.all().order_by('datetime')[0].datetime.timestamp()
                                if not sss in store:
                                    store.append(sss)
                                    item[1] = Team.objects.filter(pk__in=sss)
                                    khl_table.append(item)
                else:
                    pass
            else:
                if champ_id in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]).values_list(
                        'pk', flat=True):
                    for i in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]).filter(
                            pk=champ_id):
                        store = []
                        for g in i.champ_groups.all().filter(Q(name='Финал - Запад') | Q(name='Финал - Восток') | Q(
                                name='Финал Чемпионата')):
                            for t in g.teams.all():
                                # таблица [группа, команды, матчи]
                                item = [g, [], g.group_matches.all().filter(
                                    Q(command_first=t) | Q(command_second=t)).order_by('datetime'), [], 0, None]
                                sss = []
                                for k in g.group_matches.all().filter(
                                                Q(command_first=t) | Q(command_second=t)).values_list(
                                    'command_first', 'command_second').distinct():
                                    for s in list(k):
                                        sss.append(s)
                                sss = list(set(sss))
                                results = [0, 0]
                                for ss in g.group_matches.all().filter(
                                                Q(command_first=t) | Q(command_second=t)).order_by('datetime'):
                                    if ss.score:
                                        qwe = ss.score.split(':')
                                        if int(qwe[0]) > (int(qwe[1]) if qwe[1].isdigit() else int(qwe[1][0])):
                                            if ss.command_first.pk == sss[0]:
                                                results[0] += 1
                                            else:
                                                results[1] += 1
                                        else:
                                            if ss.command_first.pk == sss[0]:
                                                results[1] += 1
                                            else:
                                                results[0] += 1
                                item[3] = results
                                item[4] = 7 - g.group_matches.all().filter(
                                    Q(command_first=t) | Q(command_second=t)).order_by('datetime').count()
                                item[5] = g.group_matches.all().order_by('datetime')[0].datetime.timestamp()
                                if not sss in store:
                                    store.append(sss)
                                    item[1] = Team.objects.filter(pk__in=sss)
                                    khl_table.append(item)

                else:
                    pass
            khl_table = sorted(khl_table, key=itemgetter(5), reverse=True)
        except Exception as e:
            khl_table = []

        popular_matches = [i for i in self_matches_all if i.popular]

        context_data.update({
            'all_champs': all_champs,
            'self_matches_all': self_matches_all,
            'khl_table': khl_table,
            'popular_matches': popular_matches,
        })

    # турнир - по группам
    elif context_data['sitesettings'].index_type == 3:
        # для чемпионата
        if context_data['sitesettings'].main_champ.all().count():
            gte_matches = []
            all_champs = []
            # ищем активные чемпионаты + pks будущих матчей
            for c in context_data['sitesettings'].main_champ.all():
                gte_matches += c.champ_matches.all().filter(
                    datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
                if c.champ_matches.all().filter(datetime__gte=msc.localize(datetime.datetime.now())).count():
                    all_champs.append(c)

            # матчи сортированные по группам
            self_matches_all = Match.objects.filter(hide_match=False, pk__in=gte_matches, fake_match=False).select_related(
                *select_related).order_by('group__menuposition', 'datetime')
            # матчи фильтрованные по группам для фильтра
            group = Match.objects.filter(hide_match=False, pk__in=gte_matches, fake_match=False).select_related(*select_related).distinct(
                'group')

        else:
            # будущие матчи
            self_matches_all = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                command_second=context_data['sitesettings'].main_team),
                                                    datetime__gte=msc.localize(datetime.datetime.now()),
                                                    hide_match=False, fake_match=False).select_related(*select_related).order_by(
                'group__menuposition', 'datetime')
            # матчи фильтрованные по группам для фильтра
            group = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                command_second=context_data['sitesettings'].main_team),
                                         datetime__gte=msc.localize(datetime.datetime.now()),
                                         hide_match=False, fake_match=False).select_related(*select_related)
            all_champs = []
            # ищем активные чемпионаты
            for c in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]):
                if c.champ_matches.all().filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
                        command_second=context_data['sitesettings'].main_team),
                                                datetime__gte=msc.localize(datetime.datetime.now())).count():
                    all_champs.append(c)

        # популярные матчи которые отправляем на верх страницы
        popular_matches = self_matches_all.filter(popular=True)

        context_data.update({
            'all_champs': all_champs,
            'self_matches_all': self_matches_all[:15],
            'popular_matches': popular_matches,
            'groups': group.values_list('group__name', 'group__pk'),
        })

    context_data.update({
        'news': news,
    })

    return HttpResponse(template.render(context_data))


def textpage(request, alias):
    if alias == 'index':
        raise Http404
    template = loader.get_template('textpage.html')

    context_data = default_context(request, alias, TextPage)

    return HttpResponse(template.render(context_data))


def teams(request):
    template = loader.get_template('teams.html')

    context_data = default_context(request, 'teams', TextPage)

    if context_data['sitesettings'].main_champ.all().count():
        champs = context_data['sitesettings'].main_champ.all()
    else:
        champs = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team])

    context_data.update({
        'champs': champs,
    })

    return HttpResponse(template.render(context_data))


def team(request, alias):
    template = loader.get_template('team.html')

    context_data = default_context(request, alias, Team)

    if context_data['sitesettings'].main_champ.all().count():
        matches = Match.objects.filter(Q(command_first=context_data['data']) | Q(command_second=context_data['data']),
                                       datetime__gte=msc.localize(datetime.datetime.now()),
                                       champ__pk__in=context_data['sitesettings'].main_champ.all().values_list('pk', flat=True)
                                       , fake_match=False).order_by('datetime')
        if not context_data['data'].id in context_data['sitesettings'].main_champ.all().values_list('teams__id', flat=True):
            raise Http404
    else:
        matches = Match.objects.filter(Q(command_first=context_data['data']) | Q(command_second=context_data['data']),
                                       datetime__gte=msc.localize(datetime.datetime.now()),
                                       champ__pk__in=Champ.objects.values_list('pk', flat=True).filter(
                                           teams__in=[context_data['sitesettings'].main_team])
                                       , fake_match=False).order_by('datetime')
        if not context_data['data'].pk in Champ.objects.values_list('teams__pk', flat=True).filter(teams__in=[context_data['sitesettings'].main_team]):
            raise Http404


    context_data.update({
        'matches': matches,
    })

    return HttpResponse(template.render(context_data))


def matches(request):
    template = loader.get_template('matches.html')

    context_data = default_context(request, 'matches', TextPage)

    select_related = ('command_first', 'command_second', 'stadium', 'champ', 'static_match')

    if context_data['sitesettings'].main_champ.filter().count():
        gte_matches = []
        lte_matches = []
        for c in context_data['sitesettings'].main_champ.filter(active=True):
            gte_matches += c.champ_matches.all().filter(
                datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
            lte_matches += c.champ_matches.all().filter(
                datetime__lte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)

        try:
            next_month = Match.objects.filter(pk__in=gte_matches).order_by('datetime').first().datetime.month
        except Exception:
            next_month = None

        if next_month and next_month != 12:
            upcoming_matches = Match.objects.filter(pk__in=gte_matches,
                    datetime__month__in=[next_month, next_month+1],
                        datetime__year__in=[datetime.datetime.now().year, datetime.datetime.now().year+1]).select_related(*select_related).order_by('datetime')


        elif next_month == 12:
            upcoming_matches = Match.objects.filter(pk__in=gte_matches,
                    datetime__month__in=[next_month, 1],
                        datetime__year__in=[datetime.datetime.now().year, datetime.datetime.now().year+1]).select_related(*select_related).order_by('datetime')
        else:
            upcoming_matches = []


        gte_matches = Match.objects.filter(pk__in=gte_matches, fake_match=False).select_related(*select_related).order_by('datetime')
        lte_matches = Match.objects.filter(pk__in=lte_matches, fake_match=False).select_related(*select_related).order_by('-datetime')
    else:
        all_champs = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team], active=True)
        gte_matches = []
        lte_matches = []
        for c in all_champs:
            gte_matches += c.champ_matches.all().filter(
                datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
            lte_matches += c.champ_matches.all().filter(
                datetime__lte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)

        try:
            next_month = Match.objects.filter(
                Q(command_first=context_data['sitesettings'].main_team) | Q(
                    command_second=context_data['sitesettings'].main_team),
                        pk__in=gte_matches).order_by('datetime').first().datetime.month
        except Exception:
            next_month = None

        if next_month and next_month != 12:
            upcoming_matches = Match.objects.filter(
                Q(command_first=context_data['sitesettings'].main_team) | Q(
                    command_second=context_data['sitesettings'].main_team),
                pk__in=gte_matches, datetime__month__in=[next_month, next_month+1],
                datetime__year__in=[datetime.datetime.now().year, datetime.datetime.now().year+1]).select_related(*select_related).order_by('datetime')


        elif next_month == 12:
            upcoming_matches = Match.objects.filter(
                Q(command_first=context_data['sitesettings'].main_team) | Q(
                    command_second=context_data['sitesettings'].main_team),
                        pk__in=gte_matches, datetime__month__in=[next_month, 1],
                datetime__year__in=[datetime.datetime.now().year, datetime.datetime.now().year+1]).select_related(*select_related).order_by('datetime')
        else:
            upcoming_matches = []

        gte_matches = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
            command_second=context_data['sitesettings'].main_team),
                                           pk__in=gte_matches, fake_match=False).select_related(*select_related).order_by('datetime')
        lte_matches = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
            command_second=context_data['sitesettings'].main_team),
                                           pk__in=lte_matches, fake_match=False).select_related(*select_related).order_by('datetime')

    # Будущие матчи
    paginator = Paginator(gte_matches, 20)
    page = request.GET.get('next')

    try:
        next = paginator.page(page)
    except PageNotAnInteger:
        next = paginator.page(1)
    except EmptyPage:
        next = paginator.page(paginator.num_pages)

    # Старые матчи
    paginator = Paginator(lte_matches, 20)
    page = request.GET.get('prev')

    try:
        prev = paginator.page(page)
    except PageNotAnInteger:
        prev = paginator.page(1)
    except EmptyPage:
        prev = paginator.page(paginator.num_pages)

    context_data.update({
        'prev': prev,
        'next': next,
        'upcoming_matches': upcoming_matches
    })

    return HttpResponse(template.render(context_data))


def match_preview(request, alias):
    template = loader.get_template('matches_preview.html')

    context_data = default_context(request, alias, Staticmatch)

    if context_data['sitesettings'].main_champ.all().count():
        champ_ids = context_data['sitesettings'].main_champ.all().values_list('pk', flat=True)
        view = False
        for item in context_data['data'].static_matches.all().values_list('champ__pk', flat=True):
            if item in champ_ids:
                view = True

        if not view:
            raise Http404
    else:
        champ_ids = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team.pk]).values_list('pk', flat=True)
        view = False
        for item in context_data['data'].static_matches.all().values_list('champ__pk', flat=True):
            if item in champ_ids:
                view = True

        if not view:
            raise Http404

    matches = context_data['data'].static_matches.all().filter(command_first__team_type=context_data['sitesettings'].sport_type).order_by('datetime')

    context_data.update({
        'matches_gte': matches.filter(datetime__gte=msc.localize(datetime.datetime.now())),
        'matches_lte': matches.filter(datetime__lte=msc.localize(datetime.datetime.now()))
    })

    return HttpResponse(template.render(context_data))


def match(request, static_url, match_url):
    CARRYALL_HEADERS.update({'referer': get_current_hostname(request).name})

    if request.POST.get('optimize_prices', False) == 'True':
        CARRYALL_HEADERS.pop('referer')

    if request.is_ajax():
        result = []
        if request.POST['type'] == 'get_sector_info':
            scheme = requests.get(CARRYALL_URL + '/api_crud/sector/{}/?&currency_code=rur'.format(request.POST.get('pk')),
                                  headers=CARRYALL_HEADERS).json()['sector_map']
            scheme = requests.get(scheme)
            svg = scheme.text
            info = requests.get(
                CARRYALL_URL + '/api/event_tickets/?seance_id=' + request.POST['match_id'] + '&sector_slug=' +
                request.POST['sector'] + '&currency_code=rur&format=json', headers=CARRYALL_HEADERS).json()
            # import pdb
            # pdb.set_trace()

            # Преобразования валют
            context_data = default_context(request, match_url, Match)
            currency = context_data['cart'].user_currency
            # валюты с их курсом в json
            all_currencys = {}
            # вычесляем особенный сектор или нет
            special_seats = {'rows': []}
            try:
                if context_data['data'].special_seats:
                    if str(request.POST['sector']) in [i['sector'] for i in context_data['data'].special_seats]:
                        for i in context_data['data'].special_seats:
                            if str(request.POST['sector']) == i['sector']:
                                special_seats['rows'].append(i['row'])
                                s_seats = []
                                for s_seat in i['seats'].split(','):
                                    if '-' in s_seat:
                                        for qt in range(int(s_seat.split('-')[0]), int(s_seat.split('-')[1]) + 1):
                                            s_seats.append(str(qt))
                                    else:
                                        s_seats.append(s_seat)

                                special_seats.update({i['row']: s_seats})
            except Exception:
                pass

            for cur in context_data['currencys']:
                if cur.lower_sign == 'rur':
                    all_currencys.update({
                        'rub': {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                    })
                all_currencys.update({
                    cur.lower_sign: {'original_rate': cur.original_rate, 'our_rate': cur.our_rate }
                })
            if not request.user.is_authenticated():
                if request.POST.get('optimize_prices', False) == 'True':
                    for i in info['tickets']:
                        i['p'] = calculate_price(i['p'], context_data['sitesettings'].markups)
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['pr'] == 'bintranet.api' or i['pr'] == 'bintranet2.api':
                            i['ot'] = True
                        i.pop('pr')
                        i.pop('np')
                        i.pop('pp')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1

                else:
                    for i in info['tickets']:
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['pr'] == 'bintranet.api' or i['pr'] == 'bintranet2.api':
                            i['ot'] = True
                        i.pop('pr')
                        i.pop('np')
                        i.pop('pp')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1
            else:
                if request.POST.get('optimize_prices', False) == 'True':
                    for i in info['tickets']:
                        i['p'] = calculate_price(i['p'], context_data['sitesettings'].markups)
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1
                        if i['pr'] == 'bintranet.api' or i['pr'] == 'bintranet2.api':
                            i['ot'] = i['pr']
                else:
                    for i in info['tickets']:
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1
                        if i['pr'] == 'bintranet.api' or i['pr'] == 'bintranet2.api':
                            i['ot'] = i['pr']
            # Конец преобразования валют
            result.append(svg)
            result.append(info)
        elif request.POST['type'] == 'get_sector_info_list':

            # Преобразования валют
            context_data = default_context(request, match_url, Match)
            currency = context_data['cart'].user_currency
            # валюты с их курсом в json
            all_currencys = {}
            for cur in context_data['currencys']:
                if cur.lower_sign == 'rur':
                    all_currencys.update({
                        'rub': {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                    })
                all_currencys.update({
                    cur.lower_sign: {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                })

            info = requests.get(CARRYALL_URL + '/api/event_tickets/?seance_id=' + request.POST['match_id'] + '&sector_slug=' + request.POST['sector'] + '&currency_code=rur&format=json', headers=CARRYALL_HEADERS).json()

            # вычесляем особенный сектор или нет
            special_seats = {'rows': []}
            try:
                if context_data['data'].special_seats:
                    if str(request.POST['sector']) in [i['sector'] for i in context_data['data'].special_seats]:
                        for i in context_data['data'].special_seats:
                            if str(request.POST['sector']) == i['sector']:
                                special_seats['rows'].append(i['row'])
                                s_seats = []
                                for s_seat in i['seats'].split(','):
                                    if '-' in s_seat:
                                        for qt in range(int(s_seat.split('-')[0]), int(s_seat.split('-')[1]) + 1):
                                            s_seats.append(str(qt))
                                    else:
                                        s_seats.append(s_seat)

                                special_seats.update({i['row']: s_seats})
            except Exception:
                pass

            if not request.user.is_authenticated():
                if request.POST.get('optimize_prices', False) == 'True':
                    for i in info['tickets']:
                        i['p'] = calculate_price(i['p'], context_data['sitesettings'].markups)
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['pr'] == 'bintranet.api' or i['pr'] == 'bintranet2.api':
                            i['ot'] = i['pr']
                        i.pop('pr')
                        i.pop('np')
                        i.pop('pp')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1
                else:
                    for i in info['tickets']:
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['pr'] == 'bintranet.api' or i['pr'] == 'bintranet2.api':
                            i['ot'] = i['pr']
                        i.pop('pr')
                        i.pop('np')
                        i.pop('pp')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1
            else:
                if request.POST.get('optimize_prices', False) == 'True':
                    for i in info['tickets']:
                        i['p'] = calculate_price(i['p'], context_data['sitesettings'].markups)
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1
                else:
                    for i in info['tickets']:
                        i = update_ticket_price(i, all_currencys, currency)
                        i['custom_id'] = base64.encodebytes(i['pr'].encode('utf-8')).replace(b'\n', b'').decode('ascii')
                        if i['r'] in special_seats['rows'] and i['s'] in special_seats[i['r']]:
                            i['special_seat'] = 1

            # Конец преобразования валют

            if len([i for i in info['tickets'] if i['r']]):
                info['tickets'].sort(key=lambda x: (int(x['r']) if not re.findall('\D', x['r']) else 999,
                                                (int(x['s']) if not re.findall('\D', x['s']) else 999)))

            result.append(info)

        elif request.POST['type'] == 'get_map_info':

            context_data = default_context(request, match_url, Match)
            currency = context_data['cart'].user_currency
            # валюты с их курсом в json
            all_currencys = {}
            for cur in context_data['currencys']:
                if cur.lower_sign == 'rur':
                    all_currencys.update({
                        'rub': {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                    })
                all_currencys.update({
                    cur.lower_sign: {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                })

            map = requests.get(CARRYALL_URL + '/api/event_map/?seance_id=' + request.POST['match_id'] + '&currency_code=rur&format=json', headers=CARRYALL_HEADERS).json()
            try:
                for i in map['hall']['sectors']:
                    for b in i['prices']:
                        if request.POST.get('optimize_prices', False) == 'True':
                            b['max_price'] = calculate_price(b['max_price'], context_data['sitesettings'].markups)
                            b['min_price'] = calculate_price(b['min_price'], context_data['sitesettings'].markups)

                        if currency.lower_sign != b['currency']:
                            b = update_sector_prices(b, all_currencys, currency)
                            b['currency'] = currency.lower_sign

                        # if not request.POST.get('optimize_prices', False) == 'True':
                        #     # переопределяем билеты с бинтранета - ждем Кассандру от Вована
                        #     if i['has_favorable']:
                        #         sector_info = requests.get(
                        #             CARRYALL_URL + '/api/event_tickets/?seance_id={}&sector_slug={}&currency_code=rur&format=json'.format(
                        #                 request.POST['match_id'], i['slug']), headers=CARRYALL_HEADERS).json()
                        #         prices_range = [update_ticket_price(w, all_currencys, currency)['p'] for w in
                        #                         sector_info['tickets']]
                        #         b['max_price'] = max(prices_range)
                        #         b['min_price'] = min(prices_range)

                map = sorted(map['hall']['sectors'], key=lambda x: x.get('slug'))
            except Exception:
                map = None

            result = map

        return HttpResponse(json.dumps(result))
    else:
        template = loader.get_template('match.html')

        context_data = default_context(request, match_url, Match)

        if static_url != context_data['data'].static_match.alias:
            raise Http404

        if context_data['sitesettings'].main_champ.all().count():
            if not context_data['data'].champ.pk in context_data['sitesettings'].main_champ.all().values_list('pk', flat=True):
                raise Http404
        else:
            if not context_data['data'].champ in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]):
                raise Http404

        new_matches = None

        if context_data['sitesettings'].canvas:
            canvas = requests.get(
                CARRYALL_URL + '/api_crud/event_canvas/?seance_id={}'.format(context_data['data'].carryall_id),
                headers=CARRYALL_HEADERS).json()
        else:
            canvas = None

        ticks = None
        match_ended = False

        if context_data['data'].datetime > msc.localize(datetime.datetime.now()):
            if context_data['data'].carryall_id:

                currency = context_data['cart'].user_currency
                # валюты с их курсом в json
                all_currencys = {}
                for cur in context_data['currencys']:
                    if cur.lower_sign == 'rur':
                        all_currencys.update({
                            'rub': {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                        })
                    all_currencys.update({
                        cur.lower_sign: {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                    })

                try:
                    map = requests.get(
                        CARRYALL_URL + '/api/event_map/?seance_id=' + context_data['data'].carryall_id + '&currency_code=rur&format=json',
                        headers=CARRYALL_HEADERS).json()

                    for i in map['hall']['sectors']:
                        lower_min_price = 0
                        lower_max_price = 0
                        for b in i['prices']:
                            if b['currency'] != currency.lower_sign:
                                # перевод цен в один вид
                                b = update_sector_prices(b, all_currencys, currency)
                                b['currency'] = currency.lower_sign

                            # подсчитываем минимальную цену даже если они в нескольких валютах
                            if lower_min_price > b['max_price']:
                                lower_min_price = b['max_price']
                            elif lower_min_price == 0:
                                lower_min_price = b['max_price']

                            if lower_max_price > b['max_price']:
                                lower_max_price = b['max_price']
                            elif lower_max_price == 0:
                                lower_max_price = b['max_price']
                except:
                    map = -1

                acquiring_deadline = 0  # requests.get( CARRYALL_URL + '/api/seance/?id=' + context_data['data'].carryall_id + '&format=json', headers=CARRYALL_HEADERS).json()['acquiring_deadline']
                if acquiring_deadline == 0:
                    pass
                else:
                    context_data['data'].stop_online_sell = acquiring_deadline
                    context_data['data'].save()
                try:
                    svg = requests.get(map['hall']['hall_map']).text
                except:
                    svg = 'Зал не добавлен'

                context_data.update({
                    'map': map,
                    'svg': svg,
                })
        else:
            match_ended = True
            new_matches = Match.objects.filter(Q(command_first=context_data['data'].command_first) | Q(
                command_first=context_data['data'].command_second),
                                               Q(command_second=context_data['data'].command_first) | Q(
                                                   command_second=context_data['data'].command_second),
                                               datetime__gte=msc.localize(datetime.datetime.now())).order_by('datetime')

        if context_data['data'].static_tickets.all():
            static_tickets = []
            static_tickets_vip = []
            # валюты с их курсом в json
            all_currencys = {}
            for cur in context_data['currencys']:
                if cur.lower_sign == 'rur':
                    all_currencys.update({
                        'rub': {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                    })
                all_currencys.update({
                    cur.lower_sign: {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                })

            for i in context_data['data'].static_tickets.all().order_by('menuposition'):
                if i.currency != context_data['currency']:
                    if i.vip:
                        static_tickets_vip.append(
                            update_static_ticket_price(i, all_currencys, context_data['currency']))
                    else:
                        static_tickets.append(update_static_ticket_price(i, all_currencys, context_data['currency']))
                else:
                    if i.vip:
                        static_tickets_vip.append(i)
                    else:
                        static_tickets.append(i)

            context_data.update({
                'static_tickets': static_tickets,
                'static_tickets_vip': static_tickets_vip,
            })

        context_data.update({
            'canvas': canvas,
            'ticks': ticks,
            'match_ended': match_ended,
            'new_matches': new_matches
        })

        context_data.update(
            {
                'first_team_near_matches': get_nearest_matches(context_data['data'].command_first),
                'second_team_near_matches': get_nearest_matches(context_data['data'].command_second)
             }
        )

        context_data.update({'teams_passed_matches': get_passed_matches(context_data['data'])})
        return HttpResponse(template.render(context_data))


def arenas(request):
    template = loader.get_template('arenas.html')

    context_data = default_context(request, 'arenas', TextPage)

    if context_data['sitesettings'].main_champ.all().count():
        champs_pk = context_data['sitesettings'].main_champ.values_list('pk', flat=True)
        items = Match.objects.filter(champ__pk__in=champs_pk).distinct('stadium').values_list('stadium__pk', flat=True)
        stadiums = Stadium.objects.filter(pk__in=items)
    else:
        items = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
            command_second=context_data['sitesettings'].main_team)).distinct('stadium').values_list('stadium__pk', flat=True)
        stadiums = Stadium.objects.filter(pk__in=items)

    paginator = Paginator(stadiums, 15)
    page = request.GET.get('page')

    try:
        stads = paginator.page(page)
    except PageNotAnInteger:
        stads = paginator.page(1)
    except EmptyPage:
        stads = paginator.page(paginator.num_pages)

    context_data.update({
        'stads': stads,
    })

    return HttpResponse(template.render(context_data))


def arena(request, alias):
    template = loader.get_template('arena.html')

    context_data = default_context(request, alias, Stadium)

    if context_data['sitesettings'].main_champ.all().count():
        stad_matches = Match.objects.filter(stadium=context_data['data'],
                                            datetime__gte=msc.localize(datetime.datetime.now()),
                                            champ__pk__in=context_data['sitesettings'].main_champ.all().values_list(
                                            'pk', flat=True),
                                            fake_match=False).order_by('datetime')
        if not context_data['sitesettings'].main_champ.filter(champ_matches__stadium=context_data['data']).exists():
            raise Http404
    else:
        stad_matches = Match.objects.filter(stadium=context_data['data'],
                                            datetime__gte=msc.localize(datetime.datetime.now()),
                                            champ__pk__in=Champ.objects.values_list('pk', flat=True).filter(
                                                teams__in=[context_data['sitesettings'].main_team]),
                                            fake_match=False).order_by('datetime')
        if not Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team)|Q(command_second=context_data['sitesettings'].main_team), stadium=context_data['data']).exists():
            raise Http404

    context_data.update({
        'stad_matches': stad_matches,
    })

    return HttpResponse(template.render(context_data))


def champs(request):
    template = loader.get_template('champs.html')

    context_data = default_context(request, 'champs', TextPage)

    if context_data['sitesettings'].main_champ.all().count():
        champs = context_data['sitesettings'].main_champ.all()
    else:
        champs = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team])

    context_data.update({
        'champs': champs,
    })

    return HttpResponse(template.render(context_data))


def champ(request, alias):
    template = loader.get_template('champ.html')

    context_data = default_context(request, alias, Champ)

    if context_data['sitesettings'].main_champ.all().count():
        if not context_data['data'].pk in context_data['sitesettings'].main_champ.all().values_list('pk', flat=True):
            raise Http404
    else:
        if not context_data['data'] in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]):
            raise Http404

    if context_data['sitesettings'].main_champ.all().count():
        lte_matches = Match.objects.filter(datetime__lte=msc.localize(datetime.datetime.now()),
                                           champ=context_data['data'], fake_match=False).order_by('datetime')
        gte_matches = Match.objects.filter(datetime__gte=msc.localize(datetime.datetime.now()),
                                           champ=context_data['data'], fake_match=False).order_by('datetime')
    else:
        lte_matches = Match.objects.filter(datetime__lte=msc.localize(datetime.datetime.now()),
                                           champ=context_data['data'], fake_match=False).order_by('datetime')
        gte_matches = Match.objects.filter(datetime__gte=msc.localize(datetime.datetime.now()),
                                           champ=context_data['data'], fake_match=False).order_by('datetime')

    # Будущие матчи
    paginator = Paginator(gte_matches, 20)
    page = request.GET.get('next')

    try:
        next = paginator.page(page)
    except PageNotAnInteger:
        next = paginator.page(1)
    except EmptyPage:
        next = paginator.page(paginator.num_pages)

    # Старые матчи
    paginator = Paginator(lte_matches, 20)
    page = request.GET.get('prev')

    try:
        prev = paginator.page(page)
    except PageNotAnInteger:
        prev = paginator.page(1)
    except EmptyPage:
        prev = paginator.page(paginator.num_pages)

    context_data.update({
        'prev': prev,
        'next': next,
    })

    return HttpResponse(template.render(context_data))


def news(request):
    template = loader.get_template('news.html')

    context_data = default_context(request, 'news', TextPage)

    news = News.objects.filter(host=get_current_hostname(request)).order_by('datetime')

    context_data.update({
        'news': news,
    })

    return HttpResponse(template.render(context_data))

def auto_news(request):

    template = loader.get_template('auto_news.html')

    if request.POST:
        if request.is_ajax():
            if request.POST.get('type', None) == 'get-template':
                m = Match.objects.get(pk=request.POST.get('pk', None))
                site_settings = SiteSettings.objects.get(host__pk=request.POST.get('host'))
                score_tuple = tuple(
                    map(lambda x: int(x), re.match(r"(\d+):(\d+)", m.score).groups()))
                print("Score tuple:", score_tuple)
                def make_next_match_text(team):
                    text = ""
                    next_match = m.get_next_match_for_team(team)
                    if next_match is not None:
                        next_match_url = next_match.get_relative_url()

                        print("Next match url: ", next_match_url)
                        next_secondary_team = next_match.command_first if team == next_match.command_second else next_match.command_second
                        next_match_texts = {
                            "home": [f"Свою следующую игру <a href='{next_match_url}'>{team.name} проведут дома {dateformat.format(next_match.datetime, 'd E в H:i')} против {next_secondary_team.name}</a>."],
                            "guest": [f"На следующую игру <a href='{next_match_url}'>{team.name} поедут в {next_match.stadium.city.name} к местному {next_secondary_team.name}</a>. Игра состоится {dateformat.format(next_match.datetime, 'd E')} на стадионе {next_match.stadium.name}."],
                            "standard": [f"<a href='{next_match_url}'>{next_secondary_team.name} станет соперником {team.name} в следующем матче</a>. Встреча продёт на {next_match.stadium.name}, {dateformat.format(next_match.datetime, 'd E')}."]
                        }
                        text += "<p>" + random.choice(next_match_texts[{
                            team.stadium: "home",
                            next_secondary_team.stadium: "guest"
                        }.get(next_match.stadium, "standard")]) + "</p>"
                        return text
                next_match_text = ""




                #TODO Refactor these ifs into something approriate
                if m.command_first == site_settings.main_team and site_settings.main_champ.count() == 0:
                    main_team = m.command_first
                    secondary_team = m.command_second
                    (main_team_score, secondary_team_score) = score_tuple
                    next_match_text = make_next_match_text(main_team)

                if m.command_second == site_settings.main_team and site_settings.main_champ.count() == 0:
                    main_team = m.command_second
                    secondary_team = m.command_first
                    (secondary_team_score, main_team_score) = score_tuple
                    next_match_text = make_next_match_text(main_team)
                else:
                    main_team = m.command_first
                    secondary_team = m.command_second
                    (main_team_score, secondary_team_score) = score_tuple
                print("Main_team:", main_team, main_team_score, " Secondary team:", secondary_team, secondary_team_score)
                print('Goals:', m.goals)
                def get_result():
                    goal_difference = main_team_score - secondary_team_score
                    if goal_difference == 0:
                        return "draw"
                    if goal_difference >= 3:
                        return "rape"
                    if 0 < goal_difference < 3:
                        return "win"
                    return "lose"
                result = get_result()
                # result = {
                #     0: "draw",
                #     1: "win",
                #     2: "win",
                # }.get(abs(main_team_score - secondary_team_score), "rape")
                location = {
                    main_team.stadium: 'home',
                    secondary_team.stadium: 'guest'
                }.get(m.stadium, 'standard')
                news_content = ""
                ##Later these hard coded samples can be refactored into something better, like samples on remote server or more randomised output from methods
                default_titles = {
                    'win': [f"{main_team.name} победила {secondary_team.name} со счётом {m.score}"],
                    'lose': [f"{main_team.get_club_name()} {main_team.name} уступил {secondary_team.name}", f"{main_team.get_club_name()} {main_team.name} проиграл {secondary_team.get_club_name()} {secondary_team.name}"],
                    'rape': [f"{main_team.name} разгромила {secondary_team.name}"],
                    'draw': [f"Матч {main_team} - {secondary_team.name} не выявил победителя"],
                    'peace': [f"{main_team.name} и {secondary_team.name} разошлись миром", f"Матч {main_team.name} - {secondary_team.name} завершился без забитых голов"]
                }
                texts = {
                    'home': {
                        'win': {
                                'titles': default_titles['win'],
                                'contents': [ f"{main_team.name} победил {secondary_team.name} {dateformat.format(m.datetime, 'd E')} на домашнем стадионе {m.stadium.name} со счётом {m.score}."],
                            },

                        'lose': {
                            'titles': default_titles['lose'],
                            'contents': [f"Футбольный клуб {main_team.name} уступил {secondary_team.name} в матче, проходившем на стадионе {m.stadium.name}, со счётом {m.score}."]
                        },
                        'rape': {
                            'titles': default_titles['rape'],
                            'contents': [
                            f"{main_team.name} разгромил футбольный клуб {secondary_team.name} со счётом {m.score}. Игра проходила дома на стадионе {m.stadium.name}."]
                        },
                        'draw': {
                            'titles': default_titles['draw'],
                            'contents': [
                            f"Футболисты {main_team.name} и {secondary_team.name} не смогли выявить победителя в матче, проходившем {dateformat.format(m.datetime, 'd E')} на стадионе {m.stadium.name}. Игра завершилась со счётом {m.score}."
                        ]},
                    },
                    'guest': {
                        'win': {
                            'titles': [f'{main_team.name} в гостях обыграла {secondary_team.name}'],
                            'contents': [f"Футбольный клуб {main_team.name} одержал победу над {secondary_team.name} в матче, проходившем в {m.stadium.city.name} со счётом {m.score}."]
                        },
                        'lose': {
                            'titles': default_titles['lose'],
                            'contents': [f"{main_team.name} програли в гостевом матче {secondary_team.name}, {dateformat.format(m.datetime, 'd E')} в городе {m.stadium.city.name}. Счёт матча {m.score} в пользу хозяев поля."],
                        },
                        'rape': {
                            'titles': default_titles['rape'],
                            'contents': [f"{main_team.name} одержали убедительную гостевую победу над {secondary_team.name} со счётом {m.score}."]
                        },
                        'draw': {
                            'titles': default_titles['draw'],
                            'contents': [f"Игроки {main_team.name} увезли ничью в матче с {secondary_team.name}, проходившем в гостях на стадионе {m.stadium.name}, {dateformat.format(m.datetime, 'd E')}. Игра завершилась со счётом {m.score}."]
                        },
                    },
                    'standard': {
                        'win': {
                            'titles': default_titles['win'],
                            'contents': [f"В матче между клубами {main_team.name} и {secondary_team.name}, состоявшимся {dateformat.format(m.datetime, 'd E')} в {m.stadium.city.name}, победу одержал футбольный (хоккейный) клуб {main_team.name} со счётом {m.score}."],
                        },

                        'lose': {
                            'titles': default_titles['lose'],
                            'contents': [f"{main_team.name} уступил {secondary_team.name} в домашнем матче со счётом {m.score}, проходившем на стадионе {m.stadium.name}.", f"{main_team.name} проиграл в {m.stadium.city.name} местному клубу {secondary_team.name}, со счётом {m.score}."],
                        },
                        'rape': {
                            'titles': default_titles['rape'],
                            'contents': [f"{main_team.name} разгромил {secondary_team.name} со счётом {m.score}, проходившем на стадионе {m.stadium.name}."]
                        },
                        'draw': {
                            'titles': default_titles['draw'],
                            'contents': [f"{main_team.name} и {secondary_team.name} сыграли вничью на стадионе {m.stadium.name}. Итоговый счёт в матче {m.score}."]
                        }
                    }
                }
                # A little workaround, currently there are no special contents for situations without goals, also existing titles do not depend on the location, so no need to create another dictionaries in already big nested dictionary
                if main_team_score == 0 and secondary_team_score == 0:
                    news_name = random.choice(default_titles['peace'])
                else:
                    news_name = random.choice(texts[location][result]['titles'])
                news_content = "<p>" + random.choice(texts[location][result]['contents']) + "</p>\r\n"
                def make_goals_text(goals):
                    team_goals = len(list(filter((lambda x: len(x) > 0), goals)))
                    print("Number of teams who made goals", team_goals)
                    if team_goals == 0:
                        return f"Игроки {main_team.name} и {secondary_team.name} провели ряд опасных атак, но {m.stadium.name} так и не увидел забитых голов."
                    winner_number = goals.index(max(goals, key=lambda goal: len(goal)))
                    print("Winner_number", winner_number)
                    def get_team_by_index(index):
                        return {
                            0: m.command_first,
                            1: m.command_second
                        }.get(index)
                    print("Winner", get_team_by_index(winner_number))
                    def parse_goal(goal):
                        return goal["player"] + ", " + goal["minute"]
                    if team_goals == 1:
                        if len(goals[winner_number]) == 1:
                            return f"Единственный гол в матче забил {parse_goal(goals[winner_number][0])}, принёсший победу {get_team_by_index(winner_number).name}"
                        else:
                            return f"У {get_team_by_index(winner_number).name} безответными голами в ворота {get_team_by_index((winner_number + 1) % 2).name} отличились: " + ", ".join(list((map(parse_goal, goals[winner_number]))))
                    def parse_team_goals(goals):
                        print("Inside parse team goals", goals)
                        return "".join([f"<br/><strong>{get_team_by_index(i).name}</strong><br /> {'<br/>'.join(map(parse_goal, x))}" for i, x in enumerate(goals)]) + "."
                    return f"Голы на свой счёт в составе {get_team_by_index(winner_number).name} и {get_team_by_index((winner_number + 1) % 2).name} записали следующие футболисты: {parse_team_goals(goals)}"

                print(make_goals_text(m.goals))
                print("Next match text", next_match_text)
                news_content += "<h2>Забитые голы</h2>\r\n<p>" + make_goals_text(m.goals) + "</p>\r\n" + next_match_text
                print("News title", news_name),
                print("New text:", news_content)
                print("Next match:", m.get_next_match())
                news_h1 = f"{m.command_first.name} {m.score} {m.command_second.name}"
                return HttpResponse(json.dumps({'name': Template(news_name).render(Context({'m': m})),
                                                'h1': Template(news_h1).render(Context({'m': m})),
                                                'content': Template(news_content).render(Context({'m': m}))}))\
            # if request.POST.get('type', None) == 'get-template':
            #     site_for_team = False
            #     m = Match.objects.get(pk=request.POST.get('pk', None))
            #
            #     score_1 = int(m.score.split(':')[0])
            #     score_2 = int(re.sub('\D', '', m.score.split(':')[1]))
            #
            #     # if SiteSettings.objects.get(host__pk=request.POST['host']).main_team:
            #     #     site_for_team = True
            #     #
            #         # if site_for_team:
            #         #     if SiteSettings.objects.get(host__pk=request.POST['host']).main_team == m.command_first:
            #         #
            #         #     else:
            #     #
            #     # else:
            #     # team 1 win
            #     if score_1 > score_2:
            #         # team 1 big win
            #         if score_1 - score_2 >= 3:
            #             news_name = "{{ m.command_first.name }} дома разгромили {{ m.command_second.name }} - {{ m.score }}"
            #         # team 1 simple win
            #         else:
            #             news_name = "Домашняя победа {{ m.command_first.name }} {{ m.score }} {{ m.command_second.name }}"
            #
            #         news_content_part1 = "<p>В матче между клубами {{ m.command_first.name }} и {{ m.command_second.name }}, состоявшимся {{ m.datetime }} в {{ m.stadium.city.name }}, победу одержал футбольный клуб {{ m.command_first.name }} со счётом {{ m.score }}.</p>"
            #
            #     # team 2 win
            #     elif score_1 < score_1:
            #         # team 2 big win
            #         if score_2 - score_1 >= 3:
            #             news_name = "{{ m.command_second.name }} в гостях разгромили {{ m.command_first.name }} - {{ m.score }}"
            #         # team 2 simple win
            #         else:
            #             news_name = "{{ m.command_first.name }} {{ m.score }} {{ m.command_second.name }}"
            #         news_content_part1 = "<p>В матче между клубами {{ m.command_first.name }} и {{ m.command_second.name }}, состоявшимся {{ m.datetime|date:'d E Y' }} в {{ m.datetime|date:'H:i' }} в городе {{ m.stadium.city.name }}, победу одержал футбольный клуб {{ m.command_second.name }} со счётом {{ m.score }}.</p>"
            #
            #     # draw
            #     else:
            #         news_name = "Ничья в матче {{ m.command_first.name }} - {{ m.command_second.name }} - {{ m.score }}"
            #         news_content_part1 = "<p>{{ m.command_first.name }} и {{ m.command_second.name }} сыграли вничью в матче {{ m.champ.name }} на стадионе {{ m.stadium.name }}. Итоговый счёт в матче {{ m.score }}."
            #
            #     # h1
            #     news_h1 = "{{ m.command_first.name }} {{ m.score }} {{ m.command_second.name }}"
            #
            #
            #     # забитые голы
            #
            #     # если гол всего 1
            #     if score_1 + score_2 == 1:
            #         # победа 1
            #         if score_1 > score_2:
            #             news_content_part2 = '<p>Единственный гол в матче забил {% if m.goals.0 %}{{ m.goals.0.0.player }} ({{ m.goals.0.0.minute }}){% else %}{{ m.goals.1.0.player }} ({{ m.goals.1.0.minute }}){% endif %}, принёсший победу клубу {{ m.command_first.name }}.</p>'
            #         # победа 2
            #         else:
            #             news_content_part2 = '<p>Единственный гол в матче забил {% if m.goals.0 %}{{ m.goals.0.0.player }} ({{ m.goals.0.0.minute }}){% else %}{{ m.goals.1.0.player }} ({{ m.goals.1.0.minute }}){% endif %}, принёсший победу клубу {{ m.command_second.name }}.</p>'
            #     # если голов небыло
            #     elif score_1 + score_2 == 0:
            #         news_content_part2 = '<p>Игроки комманд {{ m.command_first.name }} и {{ m.command_second.name }} провели ряд опасных атак, но на стадионе {{ m.stadium.name }} так и не увидели забитых голов.</p>'
            #     else:
            #         # если победа в сухую
            #         if score_1 == 0 or score_2 == 0:
            #             # победа 1
            #             if score_1 > score_2:
            #                 news_content_part2 = '<p>У команды {{ m.command_first.name }} безответными голами в ворота клуба {{ m.command_second.name }} отличились: {% for i in m.goals.0 %}{% if forloop.last %}{{ i.player }} {{ i.minute }}{% else %}{{ i.player }} {{ i.minute }}, {% endif %}{% endfor %}</p>'
            #             # победа 2
            #             else:
            #                 news_content_part2 = '<p>У команды {{ m.command_second.name }} безответными голами в ворота клуба {{ m.command_first.name }} отличились: {% for i in m.goals.1 %}{% if forloop.last %}{{ i.player }} {{ i.minute }}{% else %}{{ i.player }} {{ i.minute }}, {% endif %}{% endfor %}</p>'
            #         else:
            #             # если ничья
            #             if score_1 == score_2:
            #                 news_content_part2 = '<p>В матче {{ m.command_first.name }} - {{ m.command_second.name }} не было выявлено победителей, игра завершилась со счётом {{ m.score }}. Голы на свой счёт записали: <br /><strong>{{ m.command_first.name }}</strong><br />{% for i in m.goals.0 %}{{ i.player }} {% if i.player2 %}({{ i.player2 }}){% endif %} {{ i.minute }}<br />{% endfor %}<strong>{{ m.command_second.name }}</strong><br />{% for i in m.goals.1 %}{{ i.player }} {% if i.player2 %}({{ i.player2 }}){% endif %} {{ i.minute }}<br />{% endfor %}</p>'
            #             else:
            #                 news_content_part2 = '<p>Голы на свой счёт в составе клубов {{ m.command_first.name }} и {{ m.command_second.name }} записали следующие футболисты: <br /><strong>{{ m.command_first.name }}</strong><br />{% for i in m.goals.0 %}{{ i.player }} {% if i.player2 %}({{ i.player2 }}){% endif %} {{ i.minute }}<br />{% endfor %}<strong>{{ m.command_second.name }}</strong><br />{% for i in m.goals.1 %}{{ i.player }} {% if i.player2 %}({{ i.player2 }}){% endif %} {{ i.minute }}<br />{% endfor %}</p>'
            #
            #     if m.get_next_match():
            #         news_content_part3 = '<p>Свою следующую игру <a href="{% url \'engine:match\' m.get_next_match.static_match.alias m.get_next_match.alias %}">{{ m.get_next_match.command_first.name }} проведет против {{ m.get_next_match.command_second.name }} {{ m.get_next_match.datetime|date:"d E Y" }}</a> в {{ m.get_next_match.datetime|date:"H:i" }} на стадионе {{ m.get_next_match.stadium.name }}.</p>'
            #     else:
            #         news_content_part3 = None
            #     # content
            #     news_content = news_content_part1 + '\r\n<h2>Забитые голы</h2>\r\n' + news_content_part2 + ("\r\n<h2>Следующий матч</h2>\r\n" + news_content_part3 if news_content_part3 else '')
            #
            #     return HttpResponse(json.dumps({'name': Template(news_name).render(Context({'m': m})), 'h1': Template(news_h1).render(Context({'m': m})), 'content': Template(news_content).render(Context({'m': m}))}))

            elif request.POST.get('type', None) == 'create-news':
                dt = Match.objects.get(pk=request.POST['match_id']).datetime + datetime.timedelta(hours=2, minutes=30)
                val = News.objects.create(match=Match.objects.get(pk=request.POST['match_id']), host=Site.objects.get(pk=request.POST['host']), content=request.POST['content'], name=request.POST['name'], seo_h1=request.POST['h_one'], alias=request.POST['url'], datetime=dt)
                return HttpResponse('/news/' + val.alias)

    elif request.GET.get('champ', None):
        matches = Match.objects.filter(~Q(score=None), champ=request.GET.get('champ', None)).order_by('datetime')
        item = 2

        c = {}
        c.update(csrf(request))

        context_data = {
            'matches': matches,
            'c': c
        }
    else:
        champs = Champ.objects.filter(active=True)
        item = 1

        context_data = {
            'champs': champs,
        }

    context_data.update({
        'item': item
    })

    return HttpResponse(template.render(context_data))

def news_item(request, alias):
    template = loader.get_template('news-item.html')

    context_data = default_context(request, alias, News)

    return HttpResponse(template.render(context_data))


def parse(request):
    template = loader.get_template('parse.html')

    if request.GET.get('update_team_images') == 'yes':
        context_data = default_context(request, 'index', TextPage)

        tot_created = 0
        for i in Team.objects.filter(Q(logo2=None) | Q(logo2__isnull=True) | Q(logo2="")):
            if i.logo:
                i.logo2 = 'teams/' + i.logo
                i.save()
                tot_created += 1

        context_data.update({
            'tot_created': tot_created
        })


    if request.GET.get('changer') == '1':
        template = loader.get_template('c_panel.html')
        context_data = default_context(request, 'index', TextPage)

        return HttpResponse(template.render(context_data))

    if request.GET.get('create_custom_match') == '1':
        template = loader.get_template('create_custom_match.html')
        context_data = default_context(request, 'index', TextPage)

        sports = TEAM_TYPE

        context_data.update({
            'sports': sports,
        })

        return HttpResponse(template.render(context_data))

    if request.GET.get('del_shit') == 'yes':

        a = SiteOrder.objects.filter(utm=[])[:100000].values_list('pk')
        a = SiteOrder.objects.filter(pk__in=a).delete()

        context_data = default_context(request, 'index', TextPage)

        context_data.update({
            'tot_created': a[0]
        })

        return HttpResponse(template.render(context_data))

    if request.GET.get('update_info') == 'yes':

        champ = request.GET.get('champ')

        matches = Match.objects.filter(Q(datetime__hour=0), ~Q(parse_url=None),
                                       datetime__gte=msc.localize(datetime.datetime.now()),
                                       champ=Champ.objects.get(pk=champ))

        w = update_dates(matches)

        context_data = default_context(request, 'index', TextPage)

        context_data.update({
            'tot_created': w
        })

        return HttpResponse(template.render(context_data))

    if request.is_ajax():
        if request.POST['type'] == 'parse':
            custom_urls = request.POST.get('custom_urls', None)
            None if custom_urls == "" else custom_urls
            urll = request.POST.get('urll', None)
            None if urll == "" else urll
            sport_type = int(request.POST['sport'])
            res = crazy_mega_parser(request, custom_urls.split('@') if custom_urls else None, urll, sport_type)
        elif request.POST['type'] == 'update_paste':
            res = update_paste_matches()
        elif request.POST['type'] == 'create_match':
            sport = request.POST.get('sport', None)
            com_1 = request.POST.get('com_1', None)
            com_2 = request.POST.get('com_2', None)
            champ = request.POST.get('champ', None)
            stad = request.POST.get('stad', None)

            if sport and com_1 and com_2 and champ:
                command1 = Team.objects.get(pk=com_1)
                command2 = Team.objects.get(pk=com_2)
                static, stat_created = Staticmatch.objects.get_or_create(command_first=command1,
                                                                         command_second=command2)
                chmp = Champ.objects.get(pk=champ)
                if stad:
                    stadium = Stadium.objects.get(pk=stad)
                else:
                    stadium = None

                match, m_created = Match.objects.get_or_create(name='{} - {}'.format(command1.name, command2.name),
                                    static_match=static, stadium=stadium, command_first=command1,
                                    command_second=command2, champ=chmp, datetime=datetime.datetime.now())

                chmp.teams.add(command2)
                chmp.teams.add(command1)

                update_urls(None)

                return HttpResponse('//{}/admin/engine/team/{}/change/'.format(request.get_host(), match.pk))

        elif request.POST['type'] == 'change':
            try:
                Match.objects.filter(pk=request.POST.get('match', 0)).update(static_match=request.POST.get('static', 0))
                res = 'gotovo'
            except:
                res = 'shlyapa'


        return HttpResponse(res)

    context_data = default_context(request, 'index', TextPage)

    sports = TEAM_TYPE

    context_data.update({
        'sports': sports,
    })

    return HttpResponse(template.render(context_data))


def checkout(request):
    CARRYALL_HEADERS.update({'referer': get_current_hostname(request).name})
    template = loader.get_template('checkout.html')

    context_data = default_context(request, 'checkout', TextPage)

    _utm_source = request.seo.analytics.get(UTM_SOURCE, False)
    is_mobile = request.user_agent.is_mobile
    request.seo_host = request.get_host()

    referer = request.seo.get_seo_info()['source_id']

    device = u'Mobile' if is_mobile else 'Desktop'
    transition = u'Переход из ' + _utm_source if _utm_source else u'СЕО'
    advert = request.seo.analytics.get(UTM_MEDIUM, '')
    advertising_company = request.seo.analytics.get(UTM_CAMPAIGN, '')
    transition_string = request.seo.analytics.get(UTM_TERM, '')
    advertising_campaign_id = request.seo.analytics.get(UTM_CAMPAIGN_ID, '')
    advertising_group_id = request.seo.analytics.get(UTM_GROUP_ID, '')
    term_id = request.seo.analytics.get(UTM_TERM_ID, '')

    context_data['cart'].utm = [
        {'device': device, 'transition': transition, 'advert': advert, 'advertising_company': advertising_company,
         'transition_string': transition_string, 'source_id': referer, 'advertising_campaign_id': advertising_campaign_id,
         'advertising_group_id': advertising_group_id, 'term_id': term_id}]
    context_data['cart'].save()

    if request.method == 'POST':
        form = ContactForm(request.POST)

        if form.is_valid():
            template = loader.get_template('success.html')
            item = BintranetSendForm(request, 'order_form')
            item = item.issue()

            if not isinstance(item, str):
                return item

            context_data.update({
                'item': item
            })

    else:
        form = ContactForm()

        context = RequestContext(request)

        context_data.update({
            'form': form,
            'context': context,
        })

    return HttpResponse(template.render(context_data))


def certificates(request):
    template = loader.get_template('certificate.html')

    context_data = default_context(request, 'index', TextPage)

    _utm_source = request.seo.analytics.get(UTM_SOURCE, False)
    is_mobile = request.user_agent.is_mobile
    request.seo_host = request.get_host()

    referer = request.seo.get_seo_info()['source_id']

    device = u'Mobile' if is_mobile else 'Desktop'
    transition = u'Переход из ' + _utm_source if _utm_source else u'СЕО'
    advert = request.seo.analytics.get(UTM_MEDIUM, '')
    advertising_company = request.seo.analytics.get(UTM_CAMPAIGN, '')
    transition_string = request.seo.analytics.get(UTM_TERM, '')
    context_data['cart'].utm = [
        {'device': device, 'transition': transition, 'advert': advert, 'advertising_company': advertising_company,
         'transition_string': transition_string, 'source_id': referer}]
    context_data['cart'].save()

    form = CertForm()

    certificates = Certificate.objects.filter(active=True).order_by('sort')

    if request.method == 'POST':
        template = loader.get_template('success.html')
        # отправка в бинтратент

        bin_id = BintranetSendForm(request, 'certificate_form')
        bin_id = bin_id.issue()

        context_data.update({
            'item': bin_id
        })

    context_data.update({
        "form": form,
        "certificates": certificates
    })

    return HttpResponse(template.render(context_data))


def get_missing_orders(request):
    template = loader.get_template('get_missing_orders.html')

    missing_orders = SiteOrder.objects.filter(~Q(name=None), ~Q(phone=None), bin_id=-1)

    data = {
        "missing_orders": missing_orders
    }

    return HttpResponse(template.render(data))

def get_content_plan(request):
    template = loader.get_template('get_content_plan.html')

    context_data = default_context(request, 'index', TextPage)

    select_related = ('command_first', 'command_second', 'stadium', 'champ', 'static_match')
    prefetch_realted = ('static',)

    if context_data['sitesettings'].main_champ.filter().count():
        gte_matches = []
        lte_matches = []
        for c in context_data['sitesettings'].main_champ.filter(active=True):
            gte_matches += c.champ_matches.all().filter(
                datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
            lte_matches += c.champ_matches.all().filter(
                datetime__lte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
        gte_matches = Match.objects.filter(pk__in=gte_matches, fake_match=False).select_related(*select_related).prefetch_related(*prefetch_realted).order_by('datetime')
        lte_matches = Match.objects.filter(pk__in=lte_matches, fake_match=False).select_related(*select_related).prefetch_related(*prefetch_realted).order_by('-datetime')
    else:
        all_champs = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team], active=True)
        gte_matches = []
        lte_matches = []
        for c in all_champs:
            gte_matches += c.champ_matches.all().filter(
                datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
            lte_matches += c.champ_matches.all().filter(
                datetime__lte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
        gte_matches = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
            command_second=context_data['sitesettings'].main_team),
                                           pk__in=gte_matches, fake_match=False).select_related(*select_related).prefetch_related(*prefetch_realted).order_by('datetime')
        lte_matches = Match.objects.filter(Q(command_first=context_data['sitesettings'].main_team) | Q(
            command_second=context_data['sitesettings'].main_team),
                                           pk__in=lte_matches, fake_match=False).select_related(*select_related).prefetch_related(*prefetch_realted).order_by('datetime')

    # Будущие матчи
    paginator = Paginator(gte_matches, 20)
    page = request.GET.get('next')

    try:
        next = paginator.page(page)
    except PageNotAnInteger:
        next = paginator.page(1)
    except EmptyPage:
        next = paginator.page(paginator.num_pages)

    # Старые матчи
    paginator = Paginator(lte_matches, 20)
    page = request.GET.get('prev')

    try:
        prev = paginator.page(page)
    except PageNotAnInteger:
        prev = paginator.page(1)
    except EmptyPage:
        prev = paginator.page(paginator.num_pages)

    context_data.update({
        'prev': prev,
        'next': next,
    })

    return HttpResponse(template.render(context_data))

@csrf_exempt
def check_reserve_orders(request):
    if request.method == 'POST':
        data = dict(request.POST).get('order_ids', [])
        if data:
            pairs_list = [(data[i], data[i + 1]) for i in range(0, len(data), 2)]
            for i in pairs_list:
                SiteOrder.objects.filter(custom_bin_id__pk=UNCUSTOM_ID_FUNC(i[0])).update(bin_id=i[1])
        return HttpResponse('success bratan')
    else:
        return HttpResponse('')


def order_success(request):
    template = loader.get_template('order_success.html')

    context_data = default_context(request, 'index', TextPage)

    return HttpResponse(template.render(context_data))


def error_404(request):
    return render(request, 'error_404.html', {})


def robots(request):
    template = loader.get_template('robots.txt')

    data = SiteSettings.objects.get(host=get_current_hostname(request))

    context = {
        'data': data
    }

    return HttpResponse(template.render(context), content_type="text/plain")


def control_panel(request):
    template = loader.get_template('c_panel.html')

    if not request.user:
        raise Http404

    c = {}
    c.update(csrf(request))
    context = {'c': c}
    context = {'types_s': TEAM_TYPE}
    if request.GET.get('type', ''):
        context.update({'type': request.GET.get('type', '')})

    if request.is_ajax():
        if request.POST['type'] == 'updatestads':
            count = 0
            ch = Champ.objects.get(pk=request.POST['champ'])
            for i in ch.champ_matches.filter(stadium=None):
                if i.command_first.stadium:
                    i.stadium = i.command_first.stadium
                    i.save()
                    count += 1
            Val = count

        elif request.POST['type'] == 'create_stads':
            count = 0
            url = request.POST['url']
            from bs4 import BeautifulSoup
            all_stads_page = requests.get(url, headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'})
            soup = BeautifulSoup(''.join(all_stads_page.text))
            hrefs = soup.findAll('a', {'class': 'table-item'})
            for i in hrefs:
                stad_url = 'https://championat.com{}'.format(i['href'])
                stad_query = requests.get(stad_url, headers={'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'})
                stad_soup = BeautifulSoup(''.join(stad_query.text))
                country = stad_soup.find('ul', {'class': 'tournament-header__facts'}).findAll('li')[0].text.strip('\n').split('\n')[1].strip()
                city = stad_soup.find('ul', {'class': 'tournament-header__facts'}).findAll('li')[1].text.strip('\n').split('\n')[1].strip()
                stadium = stad_soup.find('div', {'class': 'tournament-header__title-name'}).text.strip()
                print(country)
                if country:
                    country, country_created = Country.objects.get_or_create(name=country)

                if city:
                    city, city_created = City.objects.get_or_create(name=city, defaults={'country': country})

                if stadium:
                    stadium, stadium_created = Stadium.objects.get_or_create(name=stadium, city=city)

                count = count + stadium_created + country_created + city_created

            update_urls(None)
            
            Val = count
        return HttpResponse(Val)

    return HttpResponse(template.render(context))

def orders_check(request):
    if not request.user.is_authenticated():
        return HttpResponse('404')

    if request.is_ajax():
        from django.core import serializers
        val = SiteOrder.objects.get(bin_id=request.POST['id'])
        ticks = list(val.c_tickets.all())
        serialized_obj = serializers.serialize('json', [val, ])
        serialized_obj2 = serializers.serialize('json', ticks)
        return HttpResponse(json.dumps([serialized_obj, serialized_obj2]))

    template = loader.get_template('check_order.html')
    context_data = default_context(request, 'index', TextPage)

    return HttpResponse(template.render(context_data))

@cache_page(60 * 30)
def feed_xml(request):
    CARRYALL_HEADERS.update({'referer': get_current_hostname(request).name})
    template = loader.get_template('feed.html')
    context_data = default_context(request, 'index', TextPage)

    if context_data['sitesettings'].main_champ.all().count():
        matches = Match.objects.filter(~Q(carryall_id=''),
                                       datetime__gte=msc.localize(datetime.datetime.now()),
                                       champ__in=context_data['sitesettings'].main_champ.all().values_list('pk',
                                                                                                           flat=True),
                                       carryall_id__isnull=False, fake_match=False).order_by('datetime')
        matches_paste = Match.objects.filter(~Q(carryall_id=''),
                                             champ__in=context_data['sitesettings'].main_champ.all().values_list(
                                                 'pk', flat=True),
                                             datetime__gte=msc.localize(
                                                 datetime.datetime.now() - datetime.timedelta(days=2)),
                                             datetime__lte=msc.localize(datetime.datetime.now()),
                                             carryall_id__isnull=False, fake_match=False).order_by('datetime')
    else:
        all_champs = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team])
        gte_matches = []
        matches_paste_pk = []
        for c in all_champs:
            gte_matches += c.champ_matches.all().filter(~Q(carryall_id=''),
                                                        Q(command_first=context_data[
                                                            'sitesettings'].main_team) | Q(
                                                            command_second=context_data['sitesettings'].main_team),
                                                        datetime__gte=msc.localize(datetime.datetime.now()),
                                                        carryall_id__isnull=False).values_list('pk', flat=True)
            matches_paste_pk += c.champ_matches.all().filter(~Q(carryall_id=''),
                                                             Q(command_first=context_data[
                                                                 'sitesettings'].main_team) | Q(
                                                                 command_second=context_data[
                                                                     'sitesettings'].main_team),
                                                             datetime__gte=msc.localize(
                                                                 datetime.datetime.now() - datetime.timedelta(
                                                                     days=2)),
                                                             datetime__lte=msc.localize(datetime.datetime.now()),
                                                             carryall_id__isnull=False).values_list('pk', flat=True)
        matches = Match.objects.filter(pk__in=gte_matches, fake_match=False).order_by('datetime')
        matches_paste = Match.objects.filter(pk__in=matches_paste_pk, fake_match=False).order_by('datetime')

    if matches:
        get_real_info = requests.get('http://carryall2.tix-system.com/api_crud/seances/?id={}&currency_code=rur'.format(
            '&id='.join([i.carryall_id for i in matches])), headers=CARRYALL_HEADERS).json()
        currency = context_data['sitesettings'].default_currency
        # валюты с их курсом в json
        all_currencys = {}
        for cur in context_data['currencys']:
            if cur.lower_sign == 'rur':
                all_currencys.update({
                    'rub': {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
                })
            all_currencys.update({
                cur.lower_sign: {'original_rate': cur.original_rate, 'our_rate': cur.our_rate}
            })

        for i in get_real_info['results']:
            for b in i['prices']:
                if b['currency'] != currency.lower_sign:
                    if currency.lower_sign == 'rur':
                        # переводим цены секторов из uds/eur в рубли
                        b['max_price'] = to_rubles(b['max_price'], all_currencys[b['currency']]['our_rate'])
                        b['min_price'] = to_rubles(b['min_price'],
                                                   all_currencys[b['currency']][
                                                       'our_rate'])
                    else:
                        if currency.lower_sign == 'rur':
                            # переводим цены секторов из рублей в uds/eur
                            b['max_price'] = to_other(b['max_price'],
                                                      currency.our_rate)
                            b['min_price'] = to_other(b['min_price'],
                                                      currency.our_rate)
                        else:
                            # переводим цены секторов из uds/eur в uds/eur
                            b['max_price'] = from_other_to_other(b['max_price'],
                                                                 all_currencys[
                                                                     b['currency']][
                                                                     'our_rate'],
                                                                 currency.our_rate)
                            b['min_price'] = from_other_to_other(b['min_price'],
                                                                 all_currencys[
                                                                     b['currency']][
                                                                     'our_rate'],
                                                                 currency.our_rate)
    else:
        get_real_info = {'results': []}
    result = []

    min_price = 9999999

    for m, i in zip(matches, get_real_info['results']):
        #            if int(m.carryall_id) == i['id']:
        result.append({"id": m.id,
                       "name": '{} - {}'.format(m.command_first.default_name(), m.command_second.default_name()),
                       "min_price": i['prices'][0]['min_price'] if i['prices'] else 0,
                       "count": i['prices'][0]['total_count'] if i['prices'] else 0,
                       "stadium": m.stadium.name if m.stadium else "",
                       "city": m.stadium.city.name if m.stadium else "",
                       "url": 'https://%s%s' % (request.get_host(), reverse('main:match', kwargs={
                           'static_url': m.static_match.alias, 'match_url': m.alias})),
                       "datetime": m.datetime,
                       "logo1": m.command_first.logo2,
                       "logo2": m.command_second.logo2,
                       "champ": m.champ.image,
                       "currency": context_data['sitesettings'].default_currency.slug,
                       "home": 'true' if context_data['sitesettings'].main_team == m.command_first else 'false',
                       "emergency_shutdown": True if (m.datetime - datetime.timedelta(hours=1)) < msc.localize(datetime.datetime.now()) else False
                       })

        if min_price > i['prices'][0]['min_price'] if i['prices'] else 0:
            min_price = i['prices'][0]['min_price']

    for m in matches_paste:
        result.append({"id": m.id,
                       "name": '{} - {}'.format(m.command_first.default_name(), m.command_second.default_name()),
                       "min_price": 0,
                       "count": 0,
                       "stadium": m.stadium.name if m.stadium else "",
                       "url": 'https://%s%s' % (request.get_host(), reverse('main:match', kwargs={
                           'static_url': m.static_match.alias, 'match_url': m.alias})),
                       "datetime": m.datetime,
                       "currency": context_data['sitesettings'].default_currency.slug
                       })


    context_data.update({
        'now': msc.localize(datetime.datetime.now()),
        'result': result,
        'matches_with_tickets': matches.count(),
        'site': 'https://{}'.format(request.get_host()),
        'next_match': matches.first(),
        'min_price': min_price,
    })
    if matches.first():
        context_data.update({
            'home': 'true' if context_data['sitesettings'].main_team == matches.first().command_first else 'false'
        })
    else:
        context_data.update({
            'home': 'false'
        })

    return HttpResponse(template.render(context_data), content_type='application/xhtml+xml')


def sitemap_xml(request):
    template = loader.get_template('sitemap_xml.html')

    host = 'https://{}/'.format(request.get_host())

    urls = ['{}sitemap/{}.xml'.format(host, i) for i in sitemap_sections]

    context = {
        'urls': urls,
    }

    return HttpResponse(template.render(context), content_type='application/xhtml+xml')


def sitemap_lvl2_xml(request, alias):
    template = loader.get_template('sitemap_lvl2_xml.html')

    context_data = default_context(request, 'index', TextPage)

    host = 'https://{}/'.format(request.get_host())

    to_lvl_3 = False

    if alias == sitemap_sections[0]:
        urls = ['{}{}/'.format(host, i) for i in
                TextPage.objects.filter(~Q(alias='index'), host=get_current_hostname(request)).values_list('alias',
                                                                                                      flat=True)]
    elif alias == sitemap_sections[1]:
        urls = ['{}news/{}/'.format(host, i) for i in
                News.objects.filter(host__name=request.get_host()).values_list('alias', flat=True)]
    elif alias == sitemap_sections[2]:
        pks = []
        if context_data['sitesettings'].main_champ.all().count():
            pks += context_data['sitesettings'].main_champ.all().values_list('pk', flat=True)
        else:
            pks += Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]).values_list('pk', flat=True)
        urls = ['{}champ/{}/'.format(host, i) for i in Champ.objects.filter(pk__in=pks).values_list('alias', flat=True)]
    elif alias == sitemap_sections[3]:
        if context_data['sitesettings'].site_stadiums.all().count():
            pks = context_data['sitesettings'].site_stadiums.all()
        else:
            if context_data['sitesettings'].main_champ.all().count():
                pks = Stadium.objects.filter(
                    pk__in=context_data['sitesettings'].main_champ.all().values_list('teams__stadium__pk', flat=True))
            else:
                pks = Stadium.objects.filter(pk__in=[context_data['sitesettings'].main_team.stadium.pk])
        urls = ['{}arenas/{}/'.format(host, i) for i in
                Stadium.objects.filter(pk__in=pks).values_list('alias', flat=True)]

    elif alias == sitemap_sections[4]:
        if context_data['sitesettings'].main_champ.all().count():
            pks = context_data['sitesettings'].main_champ.all().values_list('teams__pk', flat=True)
        else:
            pks = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]).values_list('teams__pk',
                                                                                                       flat=True)
        urls = ['{}teams/{}/'.format(host, i) for i in Team.objects.filter(pk__in=pks).values_list('alias', flat=True)]

    elif alias == sitemap_sections[5]:
        pks = []
        if context_data['sitesettings'].main_champ.all().count():
            for i in context_data['sitesettings'].main_champ.all():
                pks += i.champ_matches.all().values_list('static_match__pk', flat=True)
        else:
            for i in Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]):
                pks += i.champ_matches.all().values_list('static_match__pk', flat=True)
        urls = ['{}matches/{}/'.format(host, i) for i in
                Staticmatch.objects.filter(pk__in=pks).values_list('alias', flat=True)]

    elif alias == sitemap_sections[6]:
        to_lvl_3 = True
        if context_data['sitesettings'].main_champ.all().count():
            pks = context_data['sitesettings'].main_champ.all().values_list('pk', flat=True)
        else:
            pks = Champ.objects.filter(teams__in=[context_data['sitesettings'].main_team]).values_list('pk', flat=True)
        urls = ['{}sitemap/matches/{}.xml'.format(host, i) for i in
                Champ.objects.filter(pk__in=pks).values_list('pk', flat=True)]
    else:
        raise Http404

    context = {
        'to_lvl_3': to_lvl_3,
        'urls': urls,
    }

    return HttpResponse(template.render(context), content_type='application/xhtml+xml')


def sitemap_lvl3_xml(request, alias, alias2):
    template = loader.get_template('sitemap_lvl3_xml.html')

    context_data = default_context(request, 'index', TextPage)

    host = 'https://{}/'.format(request.get_host())

    if alias == sitemap_sections[6]:
        try:
            pks = Champ.objects.get(id=alias2).champ_matches.all().values_list('pk', flat=True)
            urls = ['{}matches/{}/{}/'.format(host, i[1], i[0]) for i in
                    Match.objects.filter(pk__in=pks).values_list('alias', 'static_match__alias')]
        except:
            raise Http404
    else:
        return Http404

    context = {
        'urls': urls,
    }

    return HttpResponse(template.render(context), content_type='application/xhtml+xml')


def ajax(request):
    cart = SiteOrder.get_session(request=request)

    if request.is_ajax():
        # add carryall ticket
        if request.POST['type'] == 'put_carryall_ticket':
            if request.POST.get('count', None):
                item = json.loads(request.POST.get('json', '{"none": 0}'))
                tick, created = TicketCarryall.objects.update_or_create(match=Match.objects.get(id=request.POST['match_id']),
                                                                     hash=item['id'], sector=item['sn'], row=item['r'],
                                                                     seat=item['s'], source=base64.decodebytes(item['custom_id'].encode('utf-8')),
                                                                     ruble_price=item['rp'], price=item['p'], order=cart, defaults={'count': request.POST['count']})
            else:
                item = json.loads(request.POST.get('json', '{"none": 0}'))
                tick, created = TicketCarryall.objects.get_or_create(match=Match.objects.get(id=request.POST['match_id']),
                                                                     hash=item['id'], sector=item['sn'], row=item['r'],
                                                                     seat=item['s'], source=base64.decodebytes(item['custom_id'].encode('utf-8')),
                                                                     ruble_price=item['rp'], price=item['p'], order=cart)
            return JsonResponse(SiteOrderSerializer(cart).data)
            
        elif request.POST['type'] == 'corporate-form':
            # корпоративная заявка
            item = BintranetSendForm(request, 'corporate_form')
            MyValue = item.issue()

        elif request.POST['type'] == 'callback-form':
            # обратный звонок
            item = BintranetSendForm(request, 'callback_form')
            MyValue = item.issue()


        elif request.POST['type'] == 'remove_carryall_ticket':
            item = json.loads(request.POST.get('json', '{"none": 0}'))
            if request.POST.get('count', None):
                tick = TicketCarryall.objects.filter(hash=item['id'], order=cart,
                                                  match=Match.objects.get(id=request.POST['match_id']))
                if int(request.POST.get('count', 0)):
                    tick.update(count=request.POST['count'])
                else:
                    tick.delete()


            else:
                tick = TicketCarryall.objects.filter(hash=item['id'], order=cart,
                                                  match=Match.objects.get(id=request.POST['match_id']))
                tick.delete()
            return JsonResponse(SiteOrderSerializer(cart).data)
        elif request.POST['type'] == 'remove_carryall_ticket_fromcart':
            tick = TicketCarryall.objects.filter(hash=request.POST['hash'], order=cart)
            tick.delete()
            return JsonResponse(SiteOrderSerializer(cart).data)
        elif request.POST['type'] == 'add_static_ticket':
            tick, created = SiteOrderItem.objects.get_or_create(price=Price.objects.get(id=request.POST['id']),
                                                                order=cart)
            tick.count = 1 if created else tick.count + 1
            tick.save()
            return JsonResponse(SiteOrderSerializer(cart).data)
        elif request.POST['type'] == 'del_static_ticket':
            tick = SiteOrderItem.objects.get(id=request.POST['id'], order=cart)
            if tick.count == 1:
                tick.delete()

            else:
                tick.count -= 1
                tick.save()
            return JsonResponse(SiteOrderSerializer(cart).data)
        elif request.POST['type'] == 'mass_del_static_ticket':
            item = SiteOrderItem.objects.get(id=request.POST['id'], order=cart)
            item.delete()
            return JsonResponse(SiteOrderSerializer(cart).data)
        elif request.POST['type'] == 'get_cart':
            return JsonResponse(SiteOrderSerializer(cart).data)
        elif request.POST['type'] == 'change_user_currency':
            new_currency = Currency.objects.get(pk=request.POST['pk'])
            cart.user_currency = new_currency
            cart.save()

            if new_currency.lower_sign == 'rur':
                for i in cart.c_tickets.all():
                    i.price = i.ruble_price
                    i.save()
            else:
                for i in cart.c_tickets.all():
                    i.price = to_other(int(i.ruble_price), new_currency.our_rate)
                    i.save()
            MyValue = 'success'
        elif request.POST['type'] == 'get_champ_matches':
            site = SiteSettings.objects.get(host=get_current_hostname(request))
            if not site.main_champ.all().count():
                queryset = Champ.objects.get(pk=request.POST['pk']).champ_matches.filter(
                    Q(command_first=site.main_team) | Q(command_second=site.main_team),
                    datetime__gte=msc.localize(datetime.datetime.now())).order_by('datetime', 'carryall_id')
            else:
                queryset = Champ.objects.get(pk=request.POST['pk']).champ_matches.filter(
                    datetime__gte=msc.localize(datetime.datetime.now())).order_by('datetime', 'carryall_id')

            MyValue = render_matches(queryset[:20])

        elif request.POST['type'] == 'get_team_matches':
            select_related = ('command_first', 'command_second', 'stadium', 'champ')
            site = SiteSettings.objects.get(host=get_current_hostname(request))
            # если сайт для чемпионата
            if site.main_champ.all().count():
                gte_matches = []
                all_champs = []
                for c in site.main_champ.filter(active=True):
                    # достаем pks матчей которые скоро будут
                    gte_matches += c.champ_matches.all().filter(
                        datetime__gte=msc.localize(datetime.datetime.now())).values_list('pk', flat=True)
                    # если у текущего чемпоната есть будущие матчи отображаем его в фильтре
                    if c.champ_matches.all().filter(datetime__gte=msc.localize(datetime.datetime.now())).count():
                        all_champs.append(c)

                # матчи на главную сначала 20 с билетами и 10 без (если есть)
                self_matches_all = list(
                    Match.objects.filter(carryall_id__isnull=False, hide_match=False, pk__in=gte_matches,
                                         fake_match=False).select_related(*select_related).order_by('datetime')[
                    :20]) + list(Match.objects.filter(
                    carryall_id__isnull=True,
                    hide_match=False, pk__in=gte_matches, fake_match=False).select_related(
                    *select_related).order_by('datetime')[:10])
            # если сайт для команды
            else:
                # матчи на главную сначала 20 с билетами и 10 без (если есть)
                self_matches_all = list(
                    Match.objects.filter(Q(command_first=site.main_team) | Q(
                        command_second=site.main_team), carryall_id__isnull=False,
                                         hide_match=False,
                                         datetime__gte=msc.localize(datetime.datetime.now()),
                                         fake_match=False).select_related(*select_related).order_by('datetime')[
                    :20]) + list(Match.objects.filter(Q(command_first=site.main_team) | Q(
                    command_second=site.main_team),
                                                      carryall_id__isnull=True,
                                                      hide_match=False,
                                                      datetime__gte=msc.localize(datetime.datetime.now()),
                                                      fake_match=False).select_related(
                    *select_related).order_by('datetime')[:10])
                all_champs = []

                for c in Champ.objects.filter(teams__in=[site.main_team], active=True):
                    # ищем будущие матчи (их кол-во)
                    if c.champ_matches.all().filter(Q(command_first=site.main_team) | Q(
                            command_second=site.main_team),
                                                    datetime__gte=msc.localize(datetime.datetime.now())).count():
                        # если у текущего чемпоната есть будущие матчи отображаем его в фильтре
                        all_champs.append(c)

            MyValue = render_matches(queryset[:20])

    return HttpResponse(MyValue)

def Custom404(request):
    return HttpResponse('404')

def sendsay_jsfile(request):
    return HttpResponse(
        "if (typeof importScripts === 'function') { importScripts(\"https://image.sendsay.ru/js/push/sendsay_sw.js\");}",
        content_type="text/javascript")


@user_passes_test(lambda u: u.is_superuser)
def clear_all_cache_view(request):
    clear_all_cache()
    LogEntry.objects.create(object_repr='Clear cache', action_flag=True, user=request.user)
    messages.info(request, message='Кэш успешно очищен.')
    return HttpResponseRedirect(reverse_lazy('admin:index'))

@user_passes_test(lambda u: u.is_superuser)
def create_double(request, alias):
    m = Match.objects.get(pk=alias)
    m.pk = None
    m.alias = m.alias + '-{}'.format(randint(10000, 99999))
    m.fake_match = True
    m.carryall_id = None
    m.save()
    LogEntry.objects.create(object_repr='Дубль матча: {}-{}'.format(m.pk, m.command_first.name, m.command_second.name), action_flag=True, user=request.user)
    messages.info(request, message='Создан дубль матча: {}-{}'.format(m.command_first.name, m.command_second.name))
    return HttpResponseRedirect(reverse("admin:%s_%s_change" %('engine', 'match'), args=(m.id,)))