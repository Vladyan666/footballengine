from modeltranslation.translator import register, TranslationOptions

from engine.models import *

@register(Match_Uniq)
class MatchUniqTranslationOptions(TranslationOptions):
    fields = ('seo_h1', 'seo_description', 'seo_title', 'content',)

@register(TextPage)
class TextPageTranslationOptions(TranslationOptions):
    fields = ('name', 'seo_h1', 'seo_description', 'seo_title', 'content',)

@register(Match)
class MatchTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Team_Uniq)
class TeamUniqTranslationOptions(TranslationOptions):
    fields = ('seo_h1', 'seo_description', 'seo_title', 'content',)

@register(Staticmatch)
@register(Staticmatch_Uniq)
class MatchUniqTranslationOptions(TranslationOptions):
    fields = ('seo_h1', 'seo_description', 'seo_title', 'content',)

@register(Team)
class TeamTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(Stadium_Uniq)
class StadiumTranslationOptions(TranslationOptions):
    fields = ('seo_h1', 'seo_description', 'seo_title', 'content',)

@register(Stadium)
class StadiumTranslationOptions(TranslationOptions):
    fields = ('name', )

@register(City)
class CityTranslationOptions(TranslationOptions):
    fields = ('name', )

@register(Country)
class CountryTranslationOptions(TranslationOptions):
    fields = ('name', )

@register(Champ)
class ChampTranslationOptions(TranslationOptions):
    fields = ('name', )

@register(Group)
class GroupTranslationOptions(TranslationOptions):
    fields = ('name', )

@register(SiteSeoHeaders)
class SiteSeoHeadersTranslationOptions(TranslationOptions):
    fields = ('match', 'static', 'stadium', 'team',)

@register(StaticText)
class StaticTextTranslationOptions(TranslationOptions):
    fields = ('match_text', 'match_preview_text', )

@register(SiteSettings)
class SiteSettingsTranslationOptions(TranslationOptions):
    fields = ('default_currency', 'site_index_hml', 'site_word', 'banner_html',
              'support_your_team_text', 'main_banner_text', 'main_banner_text_two',
              'phone', 'robots', )

