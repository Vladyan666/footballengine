from django import forms
from django.utils.translation import ugettext_lazy as _

class ContactForm(forms.Form):    
    name = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'placeholder': _('Имя'), 'id': 'name', 'required': ''}), required=True)
    phone = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'id': 'phone', 'autocomplete': 'off', 'required': ''}), required=True)
    phone_code = forms.CharField(max_length=10, widget=forms.TextInput(attrs={'id': 'phone_code', 'autocomplete': 'off'}), required=False)
    email = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'placeholder': 'E-mail', 'id': 'email'}), required=False)
    addr = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'placeholder': _('Адрес доставки'), 'id': 'addr'}), required=False)
    textarea = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Доп. информация'), 'id': 'text'}),required=False)

class CertForm(forms.Form):
    name = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'placeholder': _('Имя'), 'id': 'name', 'required': ''}), required=True)
    phone = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'placeholder': _('Телефон'), 'id': 'phone', 'autocomplete': 'off', 'required': ''}), required=True)
    recipient = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'placeholder': _('Для кого'), 'id': 'for', 'required' : ''}), required=True)
    cost = forms.CharField(max_length=80, widget=forms.TextInput(attrs={'type': 'number', 'placeholder': _('Введите нужную сумму'), 'id': 'summa', 'min': 1}), required=False)