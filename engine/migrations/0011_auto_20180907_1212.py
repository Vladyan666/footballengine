# Generated by Django 2.0.7 on 2018-09-07 12:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0010_match_match_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='match_status',
            field=models.CharField(blank=True, choices=[('1', 'Матч не начался'), ('2', 'Первый тайм'), ('3', 'Второй тайм'), ('4', 'Матч окончен')], default='1', max_length=100, null=True, verbose_name='Статус матча'),
        ),
    ]
