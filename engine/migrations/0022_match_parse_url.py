# Generated by Django 2.0.7 on 2018-10-10 17:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0021_ticket_carryall_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='parse_url',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Url для парса'),
        ),
    ]
