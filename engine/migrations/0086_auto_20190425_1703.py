# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-04-25 17:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0085_auto_20190425_1645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match_uniq',
            name='static',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='static', to='engine.Match', verbose_name='Статическая модель'),
        ),
    ]
