# Generated by Django 2.0.7 on 2018-10-25 16:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0032_auto_20181024_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='robots',
            field=models.TextField(blank=True, default='User-agent: *\nDisallow: /', null=True, verbose_name='Robots.txt'),
        ),
    ]
