# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-10-29 10:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0129_auto_20191007_0926'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticketcarryall',
            name='count',
            field=models.IntegerField(default=1, verbose_name='Кол-во'),
        ),
    ]
