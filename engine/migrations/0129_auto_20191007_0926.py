# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-10-07 09:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0128_auto_20190927_1056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitesettings',
            name='google_optimize',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Счетчик Google optimize'),
        ),
    ]
