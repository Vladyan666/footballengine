# Generated by Django 2.0.7 on 2018-10-21 11:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0022_match_parse_url'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='team',
            options={'verbose_name': 'Команда', 'verbose_name_plural': 'Команды'},
        ),
        migrations.AlterModelOptions(
            name='team_uniq',
            options={'verbose_name': 'Уник команда', 'verbose_name_plural': 'Уник команды'},
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='address',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='code',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='country_fed',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='current_status',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='email',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='eng_name',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='fansite',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='fax',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='fed',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='flag',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='forma',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='founded',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='logo',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='name',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='offsite',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='phone',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='photo',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='short_description',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='stadium',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='techteam',
        ),
        migrations.RemoveField(
            model_name='team_uniq',
            name='widget',
        ),
    ]
