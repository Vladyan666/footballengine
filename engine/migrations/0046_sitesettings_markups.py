# Generated by Django 2.0.7 on 2018-11-20 11:36

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0045_auto_20181119_1122'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='markups',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=[[0.0, 1000.0], [3000.0, 1500.0], [4500.0, 2500.0], [6000.0, 3000.0], [10000.0, 4500.0], [16000.0, 5500.0], [18000.0, 7000.0], [30000.0, 9000.0], [35000.0, 10000.0], [40000.0, 13000.0], [50000.0, 15000.0], [60000.0, 18000.0], [70000.0, 22000.0], [90000.0, 25000.0], [100000.0, 30000.0], [130000.0, 35000.0], [300000.0, 39000.0]], null=True, verbose_name='Наценки'),
        ),
    ]
