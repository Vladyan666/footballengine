# Generated by Django 2.0.7 on 2018-09-28 16:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0015_ticket_carryall'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket_carryall',
            name='reserved',
            field=models.BooleanField(default=True, verbose_name='Забронирован'),
        ),
    ]
