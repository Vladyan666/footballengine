# Generated by Django 2.0.7 on 2018-11-21 11:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0047_merge_20181120_1250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitesettings',
            name='video_main',
            field=models.CharField(blank=True, help_text='Только название видео, добавленное в /static/viedo/', max_length=100, null=True, verbose_name='Видео для главной'),
        ),
    ]
