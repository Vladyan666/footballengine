# Generated by Django 2.0.7 on 2018-11-19 10:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0043_sitesettings_success'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField(blank=True, null=True, verbose_name='Количество')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлен')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='s_item', to='engine.Order', verbose_name='Заказ')),
                ('price', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_i', to='engine.Price', verbose_name='Тип цены')),
            ],
            options={
                'verbose_name': 'Билет',
                'verbose_name_plural': 'Билеты',
            },
        ),
    ]
