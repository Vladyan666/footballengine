# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-07 12:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0089_group_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='sport_type',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Футбол'), (2, 'Хоккей'), (3, 'Баскетбол')], default=1, verbose_name='Тип спорта'),
        ),
        migrations.AlterField(
            model_name='sitesettings',
            name='index_type',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Чемпионат'), (2, 'Чемпионат + плей-офф (система KHL)'), (3, 'Турнирная таблица (Группы)')], default=1, verbose_name='Тип главной'),
        ),
    ]
