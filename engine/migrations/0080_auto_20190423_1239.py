# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-04-23 12:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0079_currency'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='currency',
            options={'verbose_name': 'Валюта', 'verbose_name_plural': 'Валюты'},
        ),
        migrations.AddField(
            model_name='currency',
            name='lower_sign',
            field=models.CharField(blank=True, editable=False, max_length=20, null=True, verbose_name='Код валюты (lower)'),
        ),
    ]
