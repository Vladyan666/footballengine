from django.conf.urls import url

from . import views

urls = [
    url(r'^sitemap.xml$', views.sitemap_xml, name='sitemap_xml'),
    url(r'^sitemap/(?P<alias>[0-9A-Za-z\-_]+).xml$', views.sitemap_lvl2_xml, name='sitemap_lvl2_xml'),
    url(r'^sitemap/(?P<alias>[0-9A-Za-z\-_]+)/(?P<alias2>[0-9A-Za-z\-_]+).xml$', views.sitemap_lvl3_xml, name='sitemap_lvl3_xml'),
]