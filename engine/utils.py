import re, os
import threading
import pytz
from datetime import datetime
from django.core.cache import caches
from django.contrib.sites.models import Site
from multisite.models import Alias
from engine.models import *
from django.db.models import Q

sitemap_sections = ['textpages', 'news', 'champs', 'arenas', 'teams', 'staticmatches', 'matches']
# moscow timezone
msc = pytz.timezone("Etc/GMT+3")


def calculate_price(initial_price, margins_list=None):

    if margins_list is None or initial_price is None:
        return initial_price
    valid_increase = 0
    if type(initial_price) is str:
        initial_price = float(initial_price)

    for entry in margins_list:
        try:
            if type(entry) is list:
                limit = entry[0]
                increase = entry[1]
                if initial_price >= limit:
                    valid_increase = increase
                else:
                    break
        except (IndexError, ValueError):
            pass
    return initial_price + valid_increase

def get_filename(filename):
    return filename.upper()

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def custom_bin_id(id):
    id += 770014
    return 'S{}'.format(id)

def uncustom_bin_id(custom):
    return int(re.sub('S', '', custom)) - 770014

def clear_all_cache():
    """
    Сбрасываем ВЕСЬ кеш
    :return: None
    """

    def _clear_doorway_cache():
        cache = caches['default']
        cache.clear()

    t = threading.Thread(target=_clear_doorway_cache)
    t.daemon = True
    t.start()



# первеод цен из рублей в нерубли
def to_other(price, currency):
    return round(price / currency)
# первеод цен из нерублей в нерубли
def from_other_to_other(price, from_, to_):
    return round((int(price) * from_) / to_)
# первевод цен из нерублей в рули
def to_rubles(price, currency):
    return round(price * float(currency))
# меняем цены в выбранный курс
def update_sector_prices(prices, currencys, main_curr):
    if main_curr.lower_sign == 'rur':
        # переводим цены секторов из uds/eur в рубли
        prices['max_price'] = to_rubles(prices['max_price'], currencys[prices['currency']]['our_rate'])
        prices['min_price'] = to_rubles(prices['min_price'], currencys[prices['currency']]['our_rate'])
    else:
        if main_curr.lower_sign == 'rur':
            # переводим цены секторов из рублей в uds/eur
            prices['max_price'] = to_other(prices['max_price'], main_curr.our_rate)
            prices['min_price'] = to_other(prices['min_price'], main_curr.our_rate)
        else:
            # переводим цены секторов из uds/eur в uds/eur
            prices['max_price'] = from_other_to_other(prices['max_price'], currencys[prices['currency']]['our_rate'], main_curr.our_rate)
            prices['min_price'] = from_other_to_other(prices['min_price'], currencys[prices['currency']]['our_rate'], main_curr.our_rate)

    return prices

# переводим цены билетов (с кериола) в выбранную валюту
def update_ticket_price(i, all_currencys, currency):
    if i['c'] != currency.lower_sign:
        if currency.lower_sign == 'rur':
            i['rp'] = to_rubles(i['p'], all_currencys[i['c']]['our_rate'])
            i['p'] = to_rubles(i['p'], all_currencys[i['c']]['our_rate'])
        else:
            if currency.lower_sign == 'rur':
                i['rp'] = i['p']
                i['p'] = to_other(i['p'], currency.our_rate)
            else:
                i['rp'] = to_rubles(i['p'], all_currencys[i['c']]['our_rate'])
                i['p'] = from_other_to_other(i['p'], all_currencys[i['c']]['our_rate'], currency.our_rate)
    else:
        if currency.lower_sign == 'rur':
            i['rp'] = i['p']
        else:
            i['rp'] = to_rubles(i['p'], all_currencys[i['c']]['our_rate'])

    return i

# переводим статичиские цены в
def update_static_ticket_price(i, all_currencys, currency):
    if i.currency.lower_sign != currency.lower_sign:
        if currency.lower_sign == 'rur':
            i.price = to_rubles(int(i.price), all_currencys[i.currency.lower_sign]['our_rate'])
        else:
            i.price = from_other_to_other(int(i.price), all_currencys[i.currency.lower_sign]['our_rate'], currency.our_rate)
    return i

def get_current_alias(request):
    return Alias.objects.resolve(*request.get_host().split(':'))

def get_current_hostname(request):
    alias = get_current_alias(request)
    if hasattr(alias, 'site'):
        return alias.site
    return Site.objects.get_current()


def update_urls(request):
    from engine.models import Country, Match, Stadium, Staticmatch, City, Group, Champ, Team
    import datetime
    import pytils

    items = 0

    for i in Match.objects.filter(Q(alias='') | Q(alias__contains="'")):
        i.alias = pytils.translit.translify(
            i.command_first.default_name() + '-' + i.command_second.default_name() + '-' + datetime.datetime.strftime(i.datetime,
                                                                                                  '%d-%m-%Y')).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        Match.objects.filter(pk=i.pk).update(alias=re.sub("[\.|\#|\(|\)|\']", '', i.alias))
        items += 1

    for i in Staticmatch.objects.filter(Q(alias='') | Q(alias__contains="'")):
        i.alias = pytils.translit.translify(i.command_first.default_name() + '-' + i.command_second.default_name()).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        i.save()

        items += 1

    for i in Stadium.objects.filter(alias=''):
        i.alias = pytils.translit.translify(i.name).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        i.save()

        items += 1

    for i in Champ.objects.filter(alias=''):
        i.alias = pytils.translit.translify(i.name).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        i.save()

        items += 1

    for i in City.objects.filter(alias=''):
        i.alias = pytils.translit.translify(i.name).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        i.save()

        items += 1

    for i in Country.objects.filter(alias=''):
        i.alias = pytils.translit.translify(i.name).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        i.save()

        items += 1

    for i in Team.objects.filter(alias=''):
        i.alias = pytils.translit.translify(i.name).lower()
        i.alias = re.sub(' ', '-', i.alias)
        i.alias = re.sub("[\.|\#|\(|\)|\']", '', i.alias)
        i.save()

        items += 1


# TEAM_TYPE = (
#     (1, 'Футбол'),
#     (2, 'Хоккей'),
#     (3, 'Баскетбол'),
#     (4, 'Регби'),
#     (5, 'UFC'),
#     (6, 'Теннис'),
#     (7, 'Волейбол'),
#     (8, 'Бокс')
# )

def parse_calendar(url, s_type):
    crazy_mega_parser(None, False, url, s_type)

def crazy_mega_parser(request, custom_urls, url, sport_type):
    from engine.models import Country, Match, Stadium, Staticmatch, City, Group, Champ, Team
    from datetime import datetime
    import json, re, requests
    import pytils
    from bs4 import BeautifulSoup

    if not custom_urls:
        all_matches_page = requests.get(url, headers={
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'})

        all_matches_page = re.sub('\n', '', all_matches_page.text)

        urls = re.findall('<td class="stat-results__link">.*?href="(.*?)">.*?</td>', all_matches_page)

        counter = 0

        for g in urls:
            g = 'https://championat.com' + g
            urls[counter] = g
            counter += 1
        print('{} Матчей в списке'.format(counter))
    else:
        urls = custom_urls

    dates = {
        'января': 'jan',
        'февраля': 'feb',
        'марта': 'mar',
        'апреля': 'apr',
        'мая': 'may',
        'июня': 'jun',
        'июля': 'jul',
        'августа': 'aug',
        'сентября': 'sep',
        'октября': 'oct',
        'ноября': 'nov',
        'декабря': 'dec',
    }

    country_created = 0
    city_created = 0
    stadium_created = 0
    teams_craeted = 0
    champs_created = 0
    matches_created = 0

    for i in urls:
        query = requests.get(i, headers={
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'})
        soup = BeautifulSoup(''.join(query.text))

        try:
            result1 = soup.find('div', {'class': 'match-info__date'}).text.strip()
        except Exception:
            result1 = soup.find('div', {'class': 'match-info__title'}).text.strip()

        country = None
        city = None
        stadium = None
        # добываем дату
        try:
            dt = result1
            curr_month = re.sub('\d+', '', dt.split(',')[0]).strip()
            dt = re.sub(curr_month, dates[curr_month], dt)
            try:
                h_m = re.search('\d{2,2}:\d{2,2}', dt).group(0)
            except Exception:
                h_m = '00:00'
            dt = dt.split(',')[0] + ', ' + h_m
            dt = datetime.strptime(dt, '%d %b %Y, %H:%M')
        except Exception:
            dt = datetime.now()

        # добываем стадион, город, страну
        try:
            match_point = soup.find('div', {'class': 'match-info__extra-row'})
            stadium = match_point.find('a').text
            city = re.search('\((.*?)\)', re.sub('[\s\n]', '', match_point.text.strip())).group(1).split(',')[0]
            country = re.search('\((.*?)\)', re.sub('[\s\n]', '', match_point.text.strip())).group(1).split(',')[1]
            place_info = '{}, {}, {}.'.format(country, city, stadium)
        except Exception:
            place_info = 'error'

        # добываем названия команд и картинки
        try:
            teams_list = [i.text for i in soup.findAll('div', {'class': 'match-info__team-name'})]
            if (country == 'Россия' and 'Арсенал' in teams_list) or 'russia' in i:
                temp = []
                for it in teams_list:
                    if it == 'Арсенал':
                        temp.append('Арсенал Тула')
                    else:
                        temp.append(it)
                teams_list = temp
            print(teams_list)
            # teams = '{}, {}.'.format(teams_list[0], teams_list[1])
            ss = soup.findAll('div', {'class': 'match-info__team-logo'})
            teams_images = []
            for img_item in ss:
                img_url = img_item.find('img')['src']
                img = requests.get(img_url)
                path = os.path.join(django_settings.MEDIA_ROOT)
                if sport_type == 1:
                    image = open(path + '/teams/' + teams_list[ss.index(img_item)] + '.' + img_url.split('.')[3], 'wb')
                elif sport_type == 2:
                    image = open(
                        path + '/teams/' + teams_list[ss.index(img_item)] + 'хоккей' + '.' + img_url.split('.')[3],
                        'wb')
                else:
                    image = open(
                        path + '/teams/' + teams_list[ss.index(img_item)] + 'баскетбол' + '.' + img_url.split('.')[3],
                        'wb')

                print(image)
                image.write(img.content)
                image.close()
                if sport_type == 1:
                    teams_images.append(teams_list[ss.index(img_item)] + '.' + img_url.split('.')[3])
                elif sport_type == 2:
                    teams_images.append(teams_list[ss.index(img_item)] + 'хоккей' + '.' + img_url.split('.')[3])
                else:
                    teams_images.append(teams_list[ss.index(img_item)] + 'баскетбол' + '.' + img_url.split('.')[3])


        except Exception as e:
            print(e)  # teams = 'error'

        # добываем чемпионат
        try:
            champ_id = i.split('/')[6]
            champ_name = soup.find('option', {'value': champ_id}).text
            champ_year = soup.find('option', {'value': champ_id}).parent['data-year'].split('/')[0]
            champ = '{}, {}.'.format(champ_name, champ_year)
        except Exception:
            champ = 'error'

        # добываем группу
        try:
            group_name = soup.find('div', {'class', 'match-info__stage'}).text.split('.')[0]
            tour = 1
        except Exception:
            group_name = 'Основная'
            tour = 1

        # создаем страну, город, стадион
        if country:
            country, created = Country.objects.get_or_create(name=country)
            country_created = country_created + created

        if city:
            city, created = City.objects.get_or_create(name=city, defaults={'country': country})
            city_created = city_created + created
        if stadium:
            stadium, created = Stadium.objects.get_or_create(name=stadium, defaults={'city': city})
            stadium_created = stadium_created + created

        # создаем команды
        first_team, created = Team.objects.get_or_create(name=teams_list[0], team_type=sport_type,
                                                         defaults={'logo': teams_images[0]})
        teams_craeted = teams_craeted + created

        second_team, created = Team.objects.get_or_create(name=teams_list[1], team_type=sport_type,
                                                          defaults={'logo': teams_images[1]})
        teams_craeted = teams_craeted + created

        # создаем чемпионат
        if first_team.team_type == 3:
            champ_name = 'Баскетбол {}'.format(champ_name)

        champ, created = Champ.objects.get_or_create(name=champ_name, year=int(champ_year))
        champ.teams.add(first_team, second_team)
        champ.save()
        champs_created = champs_created + created

        # создаем группу
        group_name, created = Group.objects.get_or_create(name=group_name, champ=champ)
        group_name.teams.add(first_team, second_team)

        # создаем матч превью
        static_match, created = Staticmatch.objects.get_or_create(command_first=first_team, command_second=second_team)

        # создаем матч
        if not Match.objects.filter(parse_url=i).exists():
            match, created = Match.objects.update_or_create(name='{} - {}'.format(first_team.name, second_team.name),
                                                            static_match=static_match, command_first=first_team,
                                                            command_second=second_team, champ=champ, tour=int(tour),
                                                            datetime=dt, defaults={'parse_url': i, 'stadium': stadium,
                                                                                   'group': group_name})
            matches_created = matches_created + created
        else:
            m = Match.objects.filter(parse_url=i)
            m.update(name='{} - {}'.format(first_team.name, second_team.name),
                                                            static_match=static_match, command_first=first_team,
                                                            command_second=second_team, champ=champ, tour=int(tour),
                                                            datetime=dt, parse_url=i, stadium=stadium,
                                                                                   group=group_name)
            m.first().save()

    # пробегаемся по всем объектам, если у них пустой url то объявляем его
    update_urls(None)

    # статистика
    return 'Матчей созданно: ' + str(matches_created) + ', Команд создано: ' + str(teams_craeted)


def get_nearest_matches(team):

    from engine.models import Match
    quantity = 3  # quantity of nearest matches
    nearest_matches = Match.objects.filter(Q(command_first=team) | Q(command_second=team),
                                           datetime__gte=msc.localize(datetime.now())).order_by('datetime')[:quantity]

    return nearest_matches


def get_passed_matches(match):
    """
    Возвращает завершённые матчи с участием команд
    переданного в аргументе матча
    """

    from engine.models import Match
    quantity = 4  # quantity of passed matches
    teams_matches_cond = Q(command_first=match.command_first, command_second=match.command_second) | \
                         Q(command_first=match.command_second, command_second=match.command_first)
    matches_before = Match.objects.filter(teams_matches_cond, datetime__lt=match.datetime)
    passed_matches = matches_before.filter(datetime__lt=msc.localize(datetime.now())).order_by('-datetime')[:quantity]

    return passed_matches

# парсер для регби
# def rugby(request):
#     if not request.user.is_authenticated():
#         return HttpResponse('404')
#     else:
#         from bs4 import BeautifulSoup
#
#         country_created = 0
#         city_created = 0
#         stadium_created = 0
#         teams_craeted = 0
#         champs_created = 0
#         matches_created = 0
#
#         req = requests.get('https://www.rugbyworldcup.com/matches')
#         soup = BeautifulSoup(''.join(req.text))
#         # все матчи
#         matches = soup.findAll('span', {'class': 'fixtures__match-content'})
#         for i in matches:
#             # dates
#             day = i.find('span', {'class': 'fixtures-date__day-number'}).text.strip()
#             month = i.find('span', {'class': 'fixtures-date__month'}).text.lower().strip()
#             h_m = i.find('div', {'class': 'fixtures__time--local-time'}).find('span').text
#             dt = datetime.datetime.strptime('{} {} {}, {}'.format(day, month, '2019', h_m), '%d %B %Y, %H:%M')
#
#             # добываем стадион, город, страну
#             stad_and_city = i.find('span', {'class': 'fixtures__venue'}).text.split(',')
#             if len(stad_and_city) == 3:
#                 stadium = stad_and_city[0]
#                 city = stad_and_city[2]
#             else:
#                 stadium = stad_and_city[0]
#                 city = stad_and_city[1]
#
#             country = 'Япония'
#
#             # команды
#             teams_list = [team.strip() for team in i.find('div', {'class': 'fixtures__teams'}).text.strip().split(' v\n')]
#
#             # чемпионат
#             champ_name = 'Чемпионат Мира по Регби 2019'
#             champ_year = 2019
#
#             # group
#             if len(i.find('span', {'class', 'fixtures__event-phase'}).text.split('Pool ')) == 1:
#                 group_name = i.find('span', {'class', 'fixtures__event-phase'}).text.split('Pool ')[0]
#             else:
#                 group_name = i.find('span', {'class', 'fixtures__event-phase'}).text.split('Pool ')[1]
#
#             # создаем страну, город, стадион
#             if country:
#                 country, created = Country.objects.get_or_create(name=country)
#                 country_created = country_created + created
#
#             if city:
#                 city, created = City.objects.get_or_create(name=city, defaults={'country': country})
#                 city_created = city_created + created
#
#             if stadium:
#                 stadium, created = Stadium.objects.get_or_create(name=stadium, defaults={'city': city})
#                 stadium_created = stadium_created + created
#
#             # teams
#             first_team, created = Team.objects.get_or_create(name=teams_list[0], team_type=4)
#             teams_craeted = teams_craeted + created
#
#             second_team, created = Team.objects.get_or_create(name=teams_list[1], team_type=4)
#             teams_craeted = teams_craeted + created
#
#             # champ
#             champ, created = Champ.objects.get_or_create(name=champ_name, year=int(champ_year))
#             champ.teams.add(first_team, second_team)
#             champ.save()
#             champs_created = champs_created + created
#
#             # создаем группу
#             group_name, created = Group.objects.get_or_create(name=group_name, champ=champ)
#             group_name.teams.add(first_team, second_team)
#
#             # создаем матч превью
#             static_match, created = Staticmatch.objects.get_or_create(command_first=first_team, command_second=second_team)
#
#             # создаем матч
#             match, created = Match.objects.update_or_create(name='{} - {}'.format(first_team.name, second_team.name),
#                                                             static_match=static_match, command_first=first_team,
#                                                             command_second=second_team, champ=champ, tour=int(1),
#                                                             datetime=dt, defaults={'stadium': stadium,
#                                                                                    'group': group_name})
#             matches_created = matches_created + created
#             update_urls(None)
#
#     return HttpResponse('Матчей созданно: ' + str(matches_created) + ', Команд создано: ' + str(teams_craeted))