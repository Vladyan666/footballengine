"""_project_ URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
#from django.urls import include, path
from django.conf.urls import url, include
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls import handler404, handler500

from django.conf import settings as django_settings
from django.conf.urls.static import static
from .sitemap import urls as sitemap_urls

from . import views
app_name = 'engine'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^teams/$', views.teams, name='teams'),
    url(r'^matches/$', views.matches, name='matches'),
    url(r'^arenas/$', views.arenas, name='arenas'),
    url(r'^champs/$', views.champs, name='champs'),
    url(r'^news/$', views.news, name='news'),
    url(r'^ajax/$', views.ajax, name='ajax'),
    url(r'^parse/$', views.parse, name='parse'),
    url(r'^auto-news/$', views.auto_news, name='auto-news'),
    url(r'^feed.xml/$', views.feed_xml, name='feed_xml'),
    url(r'^sendsay_push_sw.js$', views.sendsay_jsfile, name='feed_xml'),
    url(r'^checkout/$', views.checkout, name='checkout'),
    url(r'^clear_all_cache/$', views.clear_all_cache_view, name='clear_all_cache_view'),
    url(r'^order_success/$', views.order_success, name='order_success'),
    url(r'^get_missing_orders/$', views.get_missing_orders, name='get_missing_orders'),
    url(r'^check_reserve_orders/$', views.check_reserve_orders, name='check_reserve_orders'),
    url(r'^certificates/$', views.certificates, name='certificates'),
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^get_content_plan/$', views.get_content_plan, name='get_content_plan'),
    url(r'^control_panel/$', views.control_panel, name='control_panel'),
    url(r'^orders_check/$', views.orders_check, name='orders_check'),
    url(r'^teams/(?P<alias>[0-9A-Za-z\-]+)/$', views.team, name='team'),
    url(r'^news/(?P<alias>[0-9A-Za-z\-]+)/$', views.news_item, name='news_item'),
    url(r'^champs/(?P<alias>[0-9A-Za-z\-_]+)/$', views.champ, name='champ'),
    url(r'^arenas/(?P<alias>[0-9A-Za-z\-_]+)/$', views.arena, name='arena'),
    url(r'^matches/(?P<alias>[0-9A-Za-z\-_]+)/$', views.match_preview, name='match_preview'),
    url(r'^matches/(?P<static_url>[0-9A-Za-z\-_]+)/(?P<match_url>[0-9A-Za-z\-_]+)/$', views.match, name='match'),
    url(r'^create_double/(?P<alias>[0-9]+)/$', views.create_double, name='create_double'),
    url(r'^(?P<alias>[0-9A-Za-z\-]+)/$', views.textpage, name='textpage'),
] + sitemap_urls + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)