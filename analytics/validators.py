import json

from django.core.exceptions import ValidationError


def validate_json(value):
    try:
        json.loads(value)
    except (TypeError, ValueError):
        raise ValidationError('Неподходящий формат данных')