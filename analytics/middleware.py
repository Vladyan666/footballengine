from collections import OrderedDict, namedtuple
from distutils.version import LooseVersion
from urllib.parse import urlparse

from django.conf import settings
from django.http.request import split_domain_port
# from django.utils.encoding import force_unicode
from django.utils.six import iteritems, text_type, string_types

# from .constants import ANALITYCS_KEY, UTM_SOURCE, UTM_MEDIUM, UTM_CAMPAIGN, UTM_TERM, UTM_CONTENT
# from .models import AdsKey

from django.utils.deprecation import MiddlewareMixin

class SEOAnalytics(namedtuple('SeoAnalitycsBase', 'request marks')):
    default_status = 999

    utm_sources = OrderedDict()
    utm_sources['AdWords'] = (4, '(Эдвордс)')
    utm_sources['adwords'] = (4, '(Эдвордс)')
    utm_sources['google_SearchNetwork'] = (4, '(Эдвордс)')
    utm_sources['google'] = (4, '(Эдвордс)')
    utm_sources['remarketing-g'] = (4, '(Ремаркетинг Эдвордс)')
    utm_sources['yandex-direct'] = (3, '(Директ)')
    utm_sources['direct'] = (3, '(Директ)')
    utm_sources['yandex'] = (3, '(Директ)')
    utm_sources['direct-obsh'] = (3, '(Директ)')
    utm_sources['direct-mp'] = (3, '(Директ)')
    utm_sources['remarketing-y'] = (3, '(Ремаркетинг Директ)')
    utm_sources['instagram'] = (1000, '(Instagram)')
    utm_sources['facebook'] = (1001, '(Facebook)')
    utm_sources['email'] = (5, 'email рассылка')
    utm_sources['Sendsay'] = utm_sources['email']
    utm_sources['google-k50'] = (8, 'Google Adwords K50')
    utm_sources['yandex-k50'] = (7, 'Yandex.Direct K50')


    sources = OrderedDict()
    sources['google'] = (2, '(SEO google)')
    sources['google.ru'] = (2, '(SEO google)')
    sources['google.com'] = (2, '(SEO google)')
    sources['google.pl'] = (2, '(SEO google)')
    sources['goo.gl'] = (2, '(SEO google)')
    sources['googleadservices.com'] = (4, '(Эдвордс)')
    sources['yandex'] = (1, '(SEO yandex)')
    sources['ya.ru'] = (1, '(SEO yandex)')
    sources['yandex.ru'] = (1, '(SEO yandex)')
    sources['yandex.net'] = (1, '(SEO yandex)')
    sources['go.mail.ru'] = (3, '(Директ)')
    sources['sendsay.ru'] = (5, '(Sendsay)')

    known_medium = {
        'cpc': 'oплата за клик',
        'display': 'баннерная реклама, с оплатой за показы',
        'price': 'реклама на прайс площадках с оплатой за клик',
        'retargeting': 'реклама на ретаргетинговых проектах',
        'affiliate': 'ссылки для партнерских программ',
        'social_cpc': 'реклама в соц.сетях с оплатой за клик',
        'special': 'ссылки для спец.проектов, таких как закладки в опере и прочих'
    }

    @property
    def analytics(self):
        return self.marks

    last = lambda self, lst: lst[-1] if lst else ''

    def analytics_message(self, request=None):
        if request is None:
            request = self.request

        if not self.analytics:
            return self.default_status, ''

        host, message = '', []
        _utm_source = self.analytics.get(settings.UTM_SOURCE) or ''
        status, status_text = self.utm_sources.get(_utm_source, (None, '(СЕО)'))
        is_mobile = getattr(request, 'flavour', 'full') == 'mobile'
        self_host = request.get_host()

        refferers = self.analytics.get('referer', []) or list(filter(
            lambda x: x and self_host not in urlparse(x).netloc,
            self.analytics.get('history', [])
        ))
        referer = self.last(refferers)
        ref_netloc = urlparse(referer).netloc
        if ref_netloc:
            host, _ = split_domain_port(ref_netloc)

        if host:
            if u'sendsay.ru' in host:
                host = u'sendsay.ru'
                referer = u'sendsay.ru'

        if status is None:
            for k in self.sources.keys():
                if k in host:
                    status, status_text = self.sources.get(k)
                    break

        message = [dict(
            header_color='black',
            header='Сайт',
            text='мобильный' if is_mobile else 'ПК',
            text_color='gray'
        ), dict(
            header_color='black',
            header='Переход из' if host else 'Нет перехода, но',
            text=' '.join([host, status_text]),
            text_color='gray',
            text_title=referer
        )]

        _medium = self.analytics.get(settings.UTM_MEDIUM)
        if _medium:
            message.append(dict(
                header_color='black',
                header='Тип рекламы',
                text=_medium,
                text_color='gray',
                text_title=self.known_medium.get(_medium, 'Неизвестный тип')
            ))

        _campaign = self.analytics.get(settings.UTM_CAMPAIGN)
        if _campaign:
            message.append(dict(
                header_color='black',
                header='Рекламная кампания',
                text=_campaign,
                text_color='gray'
            ))

        _term = self.analytics.get(settings.UTM_TERM, None)
        if _term:
            message.append(dict(
                header_color='black',
                header='Фраза перехода',
                text=', '.join(_term) if isinstance(_term, (list, tuple)) else _term,
                text_color='gray'
            ))

        msg_payload = ''
        for msg in filter(lambda m: isinstance(m, (dict, OrderedDict)), message):
            base_dict = dict(header_color='black', text_color='gray', header='', text='', text_title='')
            dc = base_dict.copy()
            dc.update(**msg)
            if dc.get('text_title'):
                msg_payload += '<p style="color: {header_color}">{header}:</br>' \
                               '<a style="color: {text_color}" href="#" title="{text_title}">{text}</a>' \
                               '</p>'.format(**dc)
            else:
                msg_payload += '<p style="color: {header_color}">{header}:</br>' \
                               '<font style="color: {text_color}">{text}</font></p>'.format(**dc)

        return status or self.default_status, msg_payload

    def check_status(self, ref, status=None):
        if status is None:
            status = self.default_status

        if status != self.default_status:
            return status

        for _source, _status in iteritems(self.sources):
            if _source in ref.lower():
                return _status
        return status

    # @property
    # def ads_model(self):
    #     # ads_keys = filter(None, map(lambda (k, v): k if v else None, iteritems(self.marks)))
    #     ads_keys = filter(None, map(lambda p: p[0] if p[1] else None, iteritems(self.marks)))
    #     ads_values = filter(None, self.marks.values())
    #
    #     try:
    #         return AdsKey.objects.filter(source__in=ads_keys, key__in=ads_values).first()
    #     except (AttributeError, Exception):
    #         return AdsKey.objects.none()

    # @property
    # def margins(self):
    #     return getattr(self.ads_model, 'margins', None)

    def get_seo_info(self, request=None):
        if request is None:
            request = self.request

        if not self.analytics:
            return self.default_status, ''

        host, message = '', []
        _campaign = self.analytics.get(settings.UTM_CAMPAIGN)
        _utm_source = self.analytics.get(settings.UTM_SOURCE) or ''
        try:
            if 'k50' in _campaign.lower() or 'к50' in _campaign.lower():
                _utm_source = '%s-k50' % _utm_source
        except Exception:
            pass
        _campaign_id = self.analytics.get(settings.UTM_CAMPAIGN_ID)
        _group_id = self.analytics.get(settings.UTM_GROUP_ID)
        _term_id = self.analytics.get(settings.UTM_TERM_ID)

        status, status_text = self.utm_sources.get(_utm_source, (None, '(СЕО)'))
        is_mobile = getattr(request, 'flavour', 'full') == 'mobile'
        self_host = request.get_host()

        refferers = self.analytics.get('referer', []) or list(filter(
            lambda x: x and self_host not in urlparse(x).netloc,
            self.analytics.get('history', [])
        ))
        referer = self.last(refferers)
        ref_netloc = urlparse(referer).netloc
        if ref_netloc:
            host, _ = split_domain_port(ref_netloc)

        if status is None:
            for k in self.sources.keys():
                if k in host:
                    status, status_text = self.sources.get(k)
                    break

        data = dict()

        data['source_id'] = status or self.default_status
        data['device'] = 'Mobile' if is_mobile else 'Desktop'
        data['transition'] = 'Переход из ' if host else 'Нет перехода, но ' + \
            '%s (%s)' % (' '.join([host, status_text]), referer)

        _medium = self.analytics.get(settings.UTM_MEDIUM)
        if _medium:
            data['advert'] = '%s (%s)' % (
                _medium, self.known_medium.get(_medium, 'Неизвестный тип'))
        if _campaign:
            data['advertising_company'] = _campaign

        _term = self.analytics.get(settings.UTM_TERM, None)
        if _term:
            data['transition_string'] = ', '.join(
                _term) if isinstance(_term, (list, tuple)) else _term,

        if _campaign_id:
            data['campaign_id'] = self.analytics.get(settings.UTM_CAMPAIGN_ID, '')

        if _group_id:
            data['group_id'] = self.analytics.get(settings.UTM_GROUP_ID, '')

        if _term_id:
            data['term_id'] = self.analytics.get(settings.UTM_TERM_ID, '')

        return data


class AnalitycsMiddleware(MiddlewareMixin):
    version = LooseVersion(settings.ANALYTICS_VERSION)

    def process_request(self, request):
        fullpath = request.get_full_path()
        fullpath = fullpath if 'http' in fullpath else None
        self_host = request.get_host()
        referer = request.META.get('HTTP_REFERER', fullpath)
        data = dict(
            utm_source=request.POST.get(settings.UTM_SOURCE, request.GET.get(settings.UTM_SOURCE, None)),
            utm_medium=request.POST.get(settings.UTM_MEDIUM, request.GET.get(settings.UTM_MEDIUM, None)),
            utm_term=request.POST.get(settings.UTM_TERM, request.GET.get(
                settings.UTM_TERM, request.POST.get('keyword', request.GET.get('keyword', None))
            )),
            utm_content=request.POST.get(settings.UTM_CONTENT, request.GET.get(settings.UTM_CONTENT, None)),
            utm_campaign=request.POST.get(settings.UTM_CAMPAIGN, request.GET.get(settings.UTM_CAMPAIGN, None)),
            utm_campaign_id=request.POST.get(settings.UTM_CAMPAIGN_ID, request.GET.get(settings.UTM_CAMPAIGN_ID, None)),
            utm_group_id=request.POST.get(settings.UTM_GROUP_ID, request.GET.get(settings.UTM_GROUP_ID, None)),
            utm_term_id=request.POST.get(settings.UTM_TERM_ID, request.GET.get(settings.UTM_TERM_ID, None)),
            referer=referer if referer and self_host not in referer else None,
            ip=self.get_client_ip(request),
            ua_string=request.META.get('HTTP_USER_AGENT', None),
            utm_version=text_type(self.version)
        )
        if getattr(settings, 'COLLECT_HISTORY', False):
            data['history'] = referer
        marks = self.update_session(request.session.get(settings.ANALITYCS_KEY, None), data)
        request.session[settings.ANALITYCS_KEY] = marks
        seo_klass = SEOAnalytics(request=request, marks=marks)
        setattr(request, 'seo', seo_klass)
        setattr(request, 'get_seo_info', getattr(seo_klass, 'get_seo_info', lambda r: ''))

    @classmethod
    def get_client_ip(cls, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        return x_forwarded_for.split(',')[0] if x_forwarded_for else \
            request.META.get('HTTP_CF_CONNECTING_IP', request.META.get('REMOTE_ADDR'))

    def update_session(self, info, data):
        if info is None:
            info = {}

        if LooseVersion(info.get('utm_version', '0.0.1')) < self.version:
            info = {}

        for k, v in data.items():
            v = v or ''
            if not v.strip():
                continue

            dt = info.get(k, v)
            if isinstance(dt, (list, tuple)):
                if v not in dt:
                    dt.append(v)
            elif isinstance(dt, (text_type,) + string_types):
                dt = v if k.startswith('utm_') or k.startswith('__') else [v]
            info.update({k: dt})
        return info


class MultipleProxyMiddleware(MiddlewareMixin):
    FORWARDED_FOR_FIELDS = [
        'HTTP_CF_CONNECTING_IP',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED_HOST',
        'HTTP_X_FORWARDED_SERVER'
    ]

    def process_request(self, request):
        """
        Rewrites the proxy headers so that only the most
        recent proxy is used.
        """
        for field in self.FORWARDED_FOR_FIELDS:
            if field in request.META:
                if ',' in request.META[field]:
                    parts = request.META[field].split(',')
                    request.META[field] = parts[-1].strip()
