FROM        python:3.6
MAINTAINER  Inzem <inzem77@gmail.com>

ENV         LANG C.UTF-8

RUN         set -x \
            && apt-get -qq update \
            && apt-get install -yq libpq-dev git \
            && apt-get purge -y --auto-remove \
            && rm -rf /var/lib/apt/lists/*

RUN         mkdir /root/.ssh/
ADD         id_rsa /root/.ssh/id_rsa
RUN         chmod 400 /root/.ssh/id_rsa
RUN         touch /root/.ssh/known_hosts
RUN         ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN         mkdir -p /opt/footballengine/app
RUN         mkdir -p /opt/footballengine/static
RUN         mkdir -p /opt/footballengine/media

WORKDIR     /opt/footballengine/app

ADD         requirements.txt /opt/footballengine/app/
RUN         pip install --no-cache-dir -r /opt/footballengine/app/requirements.txt
#RUN         pip install -e git+git@bitbucket.org:wcckd/payments.git@d6621b60a29d5f409ea348bc3e09582d69321cd1#egg=payments

RUN         rm -r /root/.ssh

ADD         . /opt/footballengine/app
